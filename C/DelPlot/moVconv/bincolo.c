#include <stdio.h>
#include <stdlib.h>
        
main(void)
{ 
	FILE *infile;
        FILE *outfile;
	int nV;
	int time;
        int id;
        int lnum;  /*layer number*/
	int numlayers; /*total layers*/
        int k;
        int i;
	int j;
        int ex;  /*number of extra (dummy) vortices in layer */
        float x;
        float y;
	int bigid;
	float bigx=99.00;
	float bigy=99.00;
	int totvor;

/* open input file*/
	if ((infile=fopen("smtest","r"))==NULL) {
	printf("File missing\n");
	exit(1);
			}

/* open output file */
	if ((outfile=fopen("colout","w"))==NULL) {
        printf("Can't open output\n");
        exit(1);
                        }

        fscanf(infile,"%d",&nV);
        fscanf(infile,"%d",&time);
	j=0;

while(!(feof(infile))){
	lnum=1;
	numlayers=1;
	if(j){
	fwrite(&totvor,sizeof(int),1,outfile);
	fwrite(&time,sizeof(int),1,outfile);
	fwrite(&numlayers,sizeof(int),1,outfile);
	}
	i=0;
	k=0;
        fscanf(infile,"%d",&id);
        fscanf(infile,"%f",&x);
        fscanf(infile,"%f",&y);
	while(!(id>0)){
	if(j){
	fwrite(&lnum,sizeof(int),1,outfile);
	fwrite(&id,sizeof(int),1,outfile);
	fwrite(&x,sizeof(float),1,outfile);
	fwrite(&y,sizeof(float),1,outfile);}
	k++;
        fscanf(infile,"%d",&id);
        fscanf(infile,"%f",&x);
        fscanf(infile,"%f",&y);
			}
	if(k>(nV-k)){
	ex=2*k-nV;
	lnum=2;
	if(j){
        fwrite(&lnum,sizeof(int),1,outfile);
        fwrite(&id,sizeof(int),1,outfile);
        fwrite(&x,sizeof(float),1,outfile);
        fwrite(&y,sizeof(float),1,outfile);}
	k++;
	while(k<(nV)){
	fscanf(infile,"%d",&id);
	fscanf(infile,"%f",&x);
	fscanf(infile,"%f",&y);
	if(j){
	fwrite(&lnum,sizeof(int),1,outfile);
	fwrite(&id,sizeof(int),1,outfile);
	fwrite(&x,sizeof(float),1,outfile);
	fwrite(&y,sizeof(float),1,outfile);}
	k++;
                        }
	bigid=1000;
	for(i=0;i<ex;i++){
	if(j){
	fwrite(&lnum,sizeof(int),1,outfile);                  
	fwrite(&bigid,sizeof(int),1,outfile);                    
	fwrite(&bigx,sizeof(float),1,outfile);                   
	fwrite(&bigy,sizeof(float),1,outfile);}
	bigid++;
			}
		}
 	else if(k==(nV-k)){
	ex=0;
	lnum=2;
	if(j){
        fwrite(&lnum,sizeof(int),1,outfile);                                                       
        fwrite(&id,sizeof(int),1,outfile);                                                         
        fwrite(&x,sizeof(float),1,outfile);                                                        
        fwrite(&y,sizeof(float),1,outfile);}                                            
        k++;
        while(k<(nV)){
	fscanf(infile,"%d",&id);
	fscanf(infile,"%f",&x);
	fscanf(infile,"%f",&y);
	if(j){
	fwrite(&lnum,sizeof(int),1,outfile);                  
	fwrite(&id,sizeof(int),1,outfile);                    
	fwrite(&x,sizeof(float),1,outfile);                   
	fwrite(&y,sizeof(float),1,outfile);}
	k++;
                        }
		}
	else if(k<(nV-k)){
	ex=nV-2*k;
	lnum=1;
	bigid=1000;
	for(i=0;i<ex;i++){
	if(j){
	fwrite(&lnum,sizeof(int),1,outfile);
	fwrite(&bigid,sizeof(int),1,outfile);
	fwrite(&bigx,sizeof(float),1,outfile);                   
	fwrite(&bigy,sizeof(float),1,outfile);}                  
	bigid++;                                   
			}
	lnum=2;
	if(j){
        fwrite(&lnum,sizeof(int),1,outfile);
        fwrite(&id,sizeof(int),1,outfile);
        fwrite(&x,sizeof(float),1,outfile);
        fwrite(&y,sizeof(float),1,outfile);}
        k++;
	while(k<(nV)){
	fscanf(infile,"%d",&id);
	fscanf(infile,"%f",&x);
	fscanf(infile,"%f",&y);
	if(j){
	fwrite(&lnum,sizeof(int),1,outfile);
	fwrite(&id,sizeof(int),1,outfile);
	fwrite(&x,sizeof(float),1,outfile);                   
	fwrite(&y,sizeof(float),1,outfile);}
	k++;                                                  
                        }
		}
        fscanf(infile,"%d",&nV);
        fscanf(infile,"%d",&time);
	totvor=nV+ex;
	j++;
		}

fclose(infile);
fclose(outfile);

	}
