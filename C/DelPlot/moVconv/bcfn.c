#include <stdio.h>
#include <stdlib.h>

#define MAXPARTS 25000
 
typedef struct {
	int id;
	float x;
	float y;
	} particle;
        
main(void)
{ 
	FILE *infile;
        FILE *outfile;
	int nV;
	int time;
	particle p[MAXPARTS];
        int lnum;  /*layer number*/
	int numlayers; /*total layers*/
	int num1;
	int num2;
        int k;
        int i;
	int j;
	int flag;
        int ex;  /*number of extra (dummy) vortices in layer */
	int bigid=MAXPARTS;
	float bigx=990.00;
	float bigy=990.00;
	int totvor;

/* open input file*/
	if ((infile=fopen("smtest","r"))==NULL) {
	printf("File missing\n");
	exit(1);
			}

/* open output file */
	if ((outfile=fopen("colout","w"))==NULL) {
        printf("Can't open output\n");
        exit(1);
                        }

	flag=0;
	numlayers=2;

while(!(feof(infile))){
	if(flag){
	fwrite(&totvor,sizeof(int),1,outfile);
	fwrite(&time,sizeof(int),1,outfile);
	fwrite(&numlayers,sizeof(int),1,outfile);
	j=0;
	lnum=0;
	for(i=0;i<nV;i++){
		if(p[i].id<=0){
		fwrite(&lnum,sizeof(int),1,outfile);
		fwrite(&i,sizeof(int),1,outfile);
		fwrite(&p[i].x,sizeof(float),1,outfile);
		fwrite(&p[i].y,sizeof(float),1,outfile); 
		j++;       
			}
		}
		if(j<(nV-j)){
		ex=nV-2*j;
		for(i=0;i<ex;i++){
		fwrite(&lnum,sizeof(int),1,outfile);
		fwrite(&i,sizeof(int),1,outfile);
                fwrite(&bigx,sizeof(float),1,outfile);
                fwrite(&bigy,sizeof(float),1,outfile);
		bigid++;
			}
		}
	j=0;
	lnum=1;
	for(i=0;i<nV;i++){
                if(p[i].id>0){
		fwrite(&lnum,sizeof(int),1,outfile);
                fwrite(&p[i].id,sizeof(int),1,outfile);
                fwrite(&p[i].x,sizeof(float),1,outfile);
                fwrite(&p[i].y,sizeof(float),1,outfile);
                j++;
                        }
		}
                if(j<(nV-j)){
                ex=nV-2*j;
                for(i=0;i<ex;i++){
		fwrite(&lnum,sizeof(int),1,outfile);
                fwrite(&i,sizeof(int),1,outfile);
                fwrite(&bigx,sizeof(float),1,outfile);
                fwrite(&bigy,sizeof(float),1,outfile);
                bigid++;
                        }
                }
	}
	
        fscanf(infile,"%d",&nV);
        fscanf(infile,"%d",&time);
	num1=0;
	num2=0;

	for(i=0;i<nV;i++){
        fscanf(infile,"%d",&p[i].id);
        fscanf(infile,"%f",&p[i].x);
        fscanf(infile,"%f",&p[i].y);
	if(p[i].id<=0)
	num1++;
	if(p[i].id>0)
	num2++;
		}
	
	totvor=((num1>num2) ? num1 : num2 );
	flag++;
  }

fclose(infile);
fclose(outfile);

	}
