#include <stdio.h>
#include <stdlib.h>
        
#define MAXTYPE 5
main(void)
{ 
  FILE *in;
  FILE *outfile;
  int nV;
  int time;
  int id;
  int lnum;  /*layer number*/
  int numlayers; /*total layers*/
  int i,j,k,ll;
  int ex;  /*number of extra (dummy) vortices in layer */
  float x;
  float y;
  int bigid;
  float bigx=99.00;
  float bigy=99.00;
  int totvor;
  int num[MAXTYPE];

  /* open input file*/
  if ((in=fopen("smtest","r"))==NULL) {
    printf("File missing\n");
    exit(1);
  }

  /* open output file */
  if ((outfile=fopen("colout","w"))==NULL) {
    printf("Can't open output\n");
    exit(1);
  }
  /* Get max number of particles of one species */
  for(i=0;i<MAXTYPE;i++)
    num[i]=0;
  fscanf(in,"%d",&nV);
  fscanf(in,"%d",&time);
  for(i=0;i<nV;i++){
    fscanf(in,"%d",&id);
    fscanf(in,"%f",&x);
    fscanf(in,"%f",&y);
    if(id<=0) num[0]++;
    else num[1]++;
  }

  /* Total number of particles on *each* layer is the */
  /* max of the number of each type. */
  if(num[0]>num[1])
    totvor=num[0];
  else
    totvor=num[1];

  printf("num0 %d, num1 %d totvor %d\n",num[0],num[1],totvor);

  fclose(in);
  in=fopen("smtest","r");

  numlayers=2;
  while(!(feof(in))){
    fscanf(in,"%d",&nV);
    fscanf(in,"%d",&time);
    fwrite(&totvor,sizeof(int),1,outfile);
    fwrite(&time,sizeof(int),1,outfile);
    fwrite(&numlayers,sizeof(int),1,outfile);
    /* Read in particles of first type. */
    for(ll=0;ll<numlayers;ll++){
      for(i=0;i<totvor;i++){
	if(i<num[ll]){
	  fscanf(in,"%d",&id);
	  fscanf(in,"%f",&x);
	  fscanf(in,"%f",&y);
	}
	else{
	  x=1000.0;
	  y=1000.0;
	}
	fwrite(&lnum,sizeof(int),1,outfile);
	/* CIJOL Throwing away old ids since some were negative. */
	fwrite(&i,sizeof(int),1,outfile);
	fwrite(&x,sizeof(float),1,outfile);
	fwrite(&y,sizeof(float),1,outfile);
      }
    }
  }
  fclose(in);
  fclose(outfile);
}
