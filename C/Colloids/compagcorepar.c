/*Revisions log:
 *2.26.01 Adding sinusoidal pinning.  Note that the colloid density should
          always be set to 1 due to the way we chose our units.  Vary
          Av in order to adjust the parameter Gamma.  Reducing the range
          of the cells since our function falls off much more rapidly
          than the Bessel function.
 *2.25.01 Adapted from vratchet3.c.  New project -- colloids, using
          screened Yukawa potential.  The screening is strong so we
          can just replace the Bessel function table with a new lookup
          table, and maintain most of the rest of the code.
          Interaction based on I.V. Schweigert, PRL 84, 4381 (2000).
 =============================== COLLOIDS ===============================
 *6.22.00 Making everything ascii.  Switching to ascii smovie format.
 *6.22.00 Vortex ratchet project.  Starting with transverse force
          code: noiseIIaiT.c.  Housecleaning and checking for any known
          bugs.  Adjusting configuration.
 ================================== RATCHETS ============================
 *7.7.99 Transverse force setting.  Making fx and fy ascii.
 *6.14.99 Removing annealing.
 *6.12.99 Adapted from noiseVor8.c  Fixing some known bugs, even though 
          this might slightly alter the phase diagram.  These are
	  the NPOT and range_2 bugs.  Thus N_POT has been increased
	  from 1000 to 1500, and the value used for range_2 has been
	  altered.  Setting the code to collect S(q) data.  thus, it
	  will simply apply a requested current to the system, and
	  sit for a requested period of time, writing movie file.
	  Adjusting: now it sweeps the current quickly up to the
	  desired value.
 *10.29.98 Adding simulated annealing when the system is started from scratch.
           Disabling TODOSIG, which has been *set* up until now.
 *10.29.98 Removing all MPI routines.  Setting up a small amount of
           run-time user input.
 *10.26.98 Setting to write only a single set of files for one continuous
           sweep of the current.
 *4.14.98 High resolution current setting: data every 0.004 f0.
 *3.29.98 Disabling river, stress files.
 *10.26.97 Correction to river structure: adding periodic BC in x.
 *10.23.97 Slight alterations to current interval
 *10.22.97 Version for taking data at multiple currents.  Checking
           entire code.  All nodes use same seed.
 *9.22.97 Correcting river writes.
 *9.21.97 Version for taking data at a SINGLE current.  No ramping of
          the current whatsoever.  Expects a start file with vortices
	  in an equilibrium state at the desired current.  All 
	  remaining data files enabled.  River file written periodically.
 *9.21.97 Defect files permanently disabled.  pstr files also
          permanently disabled.  See documentation in places
	  like get_defect_speed3 and descendents.  Increasing the
	  averaging of the velocity to every 200 steps.  Also increasing
	  decimation factor to 200.  Increasing interval between
	  Pn calculations to 1000.
 *9.19.97 This version of code intended just for current sweep and
          preliminary data taking.  Thus, river and defect files
	  disabled.  Decimation factor increased to 100.
 *9.17.97 Adding second river dump at the beginning of each data run.
 *9.15.97 I need to do MORE AVERAGING.  Especially, the instantaneous
          vortex velocities are not useful for doing correlations.  I
	  should have the time-averaged velocities instead.  Working
	  to implement this.  Note that it will still be possible to
	  get instantaneous information from the two stress files.
 *9.15.97 Fixed restart bug - problem with strcpy, strcat.
 *9.15.97 Slight change to P files, enabling program to read from
          file whether or not to restart, and if so, what current
	  number to begin with.  Changing use_con and X_cur information.
 *9.14.97 Increasing decimation factor and increasing time spent at
          each current.  Changing a lot of other constants also.
 *9.14.97 Problem fixed.  Three data structures in the voronoi routine
          that were "myalloc"-ed need to be freed at the end of each
	  call, or else memory fills.  Now I have a somewhat different
	  problem, which is that the highly defective lattices 
	  produce dfct files that fill the file system too rapidly.
	  I significantly reduce the frequency of writes to these files;
	  this lowers resolution... will have to come up with some
	  way around it.
 *9.11.97 DEBUG VERSION
 *9.11.97 Setting all file writes, including meanexit, to binary.
          Shortening file name prefixes.  Removing
	  OLDaval_statistics completely.
 *9.8.97  DEBUG TEST Writing defect information in ascii
 *9.8.97  Voronoi code's ability to write postscript removed.
          Three files containing data on psub5, psub6, and psub7
          now written according to force file conventions.
	  Defect site number, number of sides, and xy locations
	  written to defectfile according to its own conventions.
 *9.8.97  Tacking att voronoi construction code onto the end of
          this file.  Goal:  write that code into the simulation
	  such that at certain intervals, the simulation sends
	  position data to the routines and gets back information
	  on defect location and number.  Eventual goal: track
	  speed of defects to test Vinokur prediction.
 *9.5.97  Non-restart version.  Taking time labels off kmovies, etc.
 *9.4.97  Back to non-restart.  Changing run_ramp_rate to no
          longer open a series of kmovie files.  There will now be
	  just one file per current - this should make life easier...
 *9.3.97  Special configuration to allow restart.
 *9.3.97  Slight adjustment to file writes.  Still seems to be an
          odd problem: program runs fine, then suddenly can't open
	  a file.  Still don't see where my file pointers are 
	  leaking, if that's the problem.
 *9.3.97  Checking for file closures.  Looks okay.  Making some naming
          convention changes.  Currents less than 10 are written as 0#.
	  Taking out the fincr force file as it is not useful.
	  Time marks on files written with 6 digits.
 *8.31.97 Repairs to pstressfile, which has a few glitches.
 *8.29.97 Fixing an assortment of things on my new configuration.
          First, the logic for opening new kmovie files does not
	  work properly unless the time between these openings is
	  equal to the time spent taking data.
	  The calculation of how long to spend increasing the
	  current didn't come out right.  Putting in some checking
	  diagnostics as nothing obvious appears.
	  Stressfile seems to have worked.  Adding a second such file
	  that will write only the forces exerted by the pins;
	  this file is called pstressfile.
 *8.28.97 Setting up logic that will run the code in the following way:
          Start from zero current.  Slowly increment current by small
	  intervals.  At certain larger intervals, hold at a given
	  current for a specified amount of time and take data, giving
	  all files meaningful names.
	  Set to ALWAYS FIELD COOL; must change for restarts.
 *8.28.97 Getting rid of some deadwood.  Linted, found bug in
          declaration of variable mN used in aval_statistics.
 *8.28.97 Attempting to put together code containing all possible
          requests for data.  These include:
          -- Average tortuosity.  Written periodically to meanexit
              file, which is closed occasionally to preserve data.
              One should only need the last line from the last
              meanexit file.  Appears to work fine.
          -- Fractal dimension.  Compile-time trigger installed
	     that will enable the dumpriver code segment.  Be sure
	     to check setting of frequency of file writes.
	  -- Overall x-direction voltage noise signal.  This and all
	     other noise signals employ the DECIFACTOR setting to
	     determine the decimation of the file.
	  -- Overall y-direction voltage noise signal.
          -- Velocity information for velocity-velocity correlation
             calculation: velfile.
          -- (New) Lattice stress information, written to a
	     "stressfile" in a format similar to velfile.  For this
	     calculation, we want only the vortex-vortex forces, so
	     the write-out is put between calc_vortex_force and
	     calc_pin_force.  Done at same interval as velfiles.
 *7.22.97 Large reduction in frequency of velocity file writes.
           Change to tortuosity calculation and fake "exit" region.
           Present sum_file decimation is 20.
 *7.20.97 Repairs.  First, checking meanexit file, which did not work.
            Now, implementing velocityfile output very infrequently.
            This will occur inside aval_statistics.
 *7.19.97 Modifications:
            Frequency of kmovie writes reduced to every 5000 steps
            Supressing all river file writes (too large)
	    Removing rbexit file writes (too large).  Instead,
	      creating meanexit file that writes cumulative mean
	      and variance every WRITEMOVIE steps.
 *7.5.97  Making exit file binary due to large size.  This version
          of code is for sitting at separate currents.
 *6.23.97 Thinking about it, I realize that in a periodic sample
          it is redundant to have an enter and an exit file.  I
          disable the enter file.
          Note that the sinuosity calculation occurs in moveVortices.
 *6.23.97 New project: Vortex voltage noise.  With the river project
          completed, all tools for an in-depth noise analysis are
          prepared.  I am taking out the fake vortices and preparing
          a completely periodic system.  Vortices will be driven with
          a constant applied force read in from a file.  I will keep
          the river files since they are quite small when compressed; I
          may slightly reduce the frequency with which they are written
          to every 100000 steps instead of every 60000.  Also reducing
          sample size to 24 x 24.  Maintaining decimation of sumforce
          files since high frequencies still aren't interesting.  Note,
          however, that I intend to use a time step four times smaller,
          changing from .04 to .01.  This may lead to adjustments of
          several of the write times.
 =====================================================================
 *3.7.97   Noise code.  Enabling writes of several voltage files.
           Using the new SUMFORCE file format: NO TIME.
           Re-enabled: sumf_file, sumfy_file.
           NEW: Automatically decimates sum force files by a factor
           of 10, averaging over these 10 values.
 *2.1.97   Version 4.1a  Further adjustment to river structure; trying
           to eliminate odd "river echoes".  BUG FIX: problem with very
           last BC check in updateRivers, used SY rather than R_POT.
 *1.29.97  Version 4.1  Doing checks on river structure; trying to
           eliminate glitch appearing at col. 36.  INCREASING river
           resolution to 300x300.  Think glitch is found: periodic B.C.
	   in updateRiver should apply *only* to y, not to x.
 *12.13.96 Version 4a.  River written every 60000 steps.  Separate
           kmovies opened again.
 *12.12.96 Version 4 of River code.  River structure write-out
           made binary.  Sinuosity calculation incorporated into code
	   so that high-resolution smovies are not required.  For this,
	   variables cum_disp and cum_x added to vortex structure.
	   tmpi not updated.  NEW FORMAT OF MOVIE CODE INTRODUCED: KMOVIE.
	   Similar to smovie format, but also contains cumulative displacement
	   of each vortex.  Name of new format: kmovie.
	   As each vortex leaves the sample, its total displacement is
	   added to the information written in exitfile.  This new exitfile
	   format is named REXIT.  Multiple kmovies are not opened.
	   Restart reads in cumulative vortex motion.
 *12.11.96 Version 3 of River code.  System size increased such that
           the sample region is 36 x 36.  River structure written only
	   at fixed intervals.  Smovies closed at same intervals, with
	   new smovies opened (to reduce file size).  Enter and exit
	   files receive the same treatment. Force files disabled.
	   Movie set to high resolution to improve sinuosity statistics.
	   TODOSIG disabled.
 *11.21.96 BUG FIX ADDED: installed periodic B.C. in updateRiver.
 *11.19.96 Debugged.  Apparent problem with fflush of data files...
           removing the fflushes for now.
 *11.19.96 Full closing flush of smovie reinstated.
 *11.18.96 Repairs made for proper parallel use.
 *11.18.96 Following modifications made:  Reduction of movie writes.
           All force files except sum_forces_file disabled.  Particle 
	   files disabled.  River files renamed to avoid overwrite.
	   Modified enter and exit files enabled.  River table
	   resolution increased, and river writes decreased.
 *11.15.96 Still adjusting details of updateRiver criteria.
 *11.14.96 Rigging code to work ONLY with fixed system size.  See
           initialize().  
 *11.14.96 Successful test.  Next, altering river structure so that
           it includes only the region filled by the sample.
 *11.13.96 Basic river structure working.  Now installing incrementation
           by vortex movement rather than by vortex position.
 *11.13.96 Beginning river structure installation.  River structure
           to be updated after each call to moveVortices.
 *11.13.96 Code readapted for gradient driving.  River structure
           to be installed.  Fake vortices re-enabled.
 *11.13.96 ######### NEW PROJECT: RIVERS ####################
 *11.9.96 Version places four extra vortices.
 *11.8.96 Slight glitch with square restart mechanism fixed.
 *11.7.96 Pin radius fixed.  Code adds a single additional vortex in
          the middle of the sample.
 *11.7.96 Square pins appear to have radius zero.  Checking.
 *11.7.96 For square pinning, set p_pl to 3 in P file.  Default myid 0.
 *11.7.96 Modification.  Periodic pinning installed.  Same routine in use
          by Charles placed into this code.  This routine puts down pinning
	  sites and then places vortices inside the pinning sites.  
	  Additional vortices are placed in the system.  Charles' code
	  has this done randomly, but here I install a controlled method
	  of addition.  Pulling routines from melt.addvortex.tranII.c.
	  Installing add_vortex_in_pins.  Installing add_pinning_square.
 *11.6.96 Particle files re-enabled
 *11.5.96 Serial version
 *11.5.96 Force file f2 now reads wider region of sample, 5 lambda
          in width.
 *10.18.96 On-the-fly decimation of sumf files results in spectrum aliasing
          problems during analysis.  Therefore, sumf files written
          at every step until such time as I find a way to band-pass filter
	  while taking data.  Disabling particle files.
 *10.4.96 Configured to trigger either type of run based on idstring.
          Specially sets system size properly by placing vortices
	  in triangle lattice, then discarding, when reading from
	  file.
 *10.3.96 Found SERIOUS BUG:  Pinning is not placed in compliance
          with revised system size (flexible SX).  The result is
	  some "half" pinning sites, clipped at the edges, that serve
	  to unevenly transport flux across the periodic B.C.  
	  Expect that fix would be to put down vortices before pinning.
	  Checking.
 *9.15.96 Fixed smovie file name.  NEW SUMFORCE FILE FORMAT: NO TIME.
          Also adding sum_forces for y-direction.
 *9.14.96 Setting for new runs.  New file name convention in use.
 *9.5.96  Fixing placement of initial triangular vortex lattice
          by allowing SX to vary.  SX made a global variable.
 *9.1.96  Increasing smovie resolution.  Reenabling part files
 *8.31.96 Added to parameter file: whether or not to use existing
          ascii contour file.
 *8.31.96 smovie now flushed rather than closed.  all files have
          identifying string appended.
 *8.31.96 Adapted from a version of current.  Rigged for two modes.
          First is a rapid scan of IV curve, done with fast increase
	  of current from zero.  Second is a slower inspection of 
	  depinning region of IV curve, using parameters input by user.
	  Special:  Parameters read from file to decrease recompilation.
	  To run this, disabled several of the improvements listed
	  below.  TODOSIG disabled.  velfile disabled.  particlefiles
	  disabled.
 ***************************************************************
 * 8.30.96:
 * Summary of old comments: Current slowly increased in calc_vortex_force.
    add_pinningMPI places random pinning according to desired pin
    density, calculating the density based on system size and requested
    number of pin sites.  Correction added to make_interaction_list: case
    for dummy_cell being greater than NCELLS added.  get_parameters pass
    of current fixed.  Unique ID numbers maintained for field cooling.
    Rigged for restart.  Tried passing vortex positions only every
    other time.  Problem with processor "babysitting" of particles 
    that move off processor remedied by having all processors calculate
    entire pinning array.  Then additional improvement: processors
    pass pin locations to neighbors during initialization so that each
    must calculate only its own pins.
 *8.29.96:
 * Summary of old comments:  Important controlling variables include
    time_span, WRITEMOVIETIME, and VELFILETIME.  'oldx' now being
    used by aval_statistics to determine when a vortex has entered
    the sample.  todosigempezar, paratodos added for special start_file
    configuration.  New particle files and revised avalfiles installed.
    fake_row now automatically set.  Variable range pinning disabled,
    and all pinning is same strength.  add_pinning_channel installed,
    along with control of barrier pinning site (on/off) which is checked
    at certain intervals.  Av=2, used in read_array to multiply the
    bessel function.  tmpi installed, form of first[] altered.
    Diagnostic files useful for debugging.  Cells mapped and passed at every
    step; hope to return to less frequent cell mapping.  Use of trans[]
    in passCellsMPI corrected to transII[].  make_interaction_list
    must use NCELLS_Y both above and below max. allowed cells;  also must 
    use transII[] array to go from translated indices back to real. 
    remove_a_vortex must decrement i before using it to index vortex, 
    since vortex has entries only out to index nV-1.  Check to be sure 
    that all MPI_Recv get id as MPI_INT, not a float.  In map_vorticesMPI, 
    code is looping over i up to value nV.  BUT, during the loop, vortices 
    can be removed, REDUCING nV AND CHANGING THE ORDER OF THE VORTICES!!
    i must be DECREMENTED after removing a vortex in order to cover
    the vortex put in the slot of the removed vortex.  Otherwise,
    vortices will be skipped!!  Small change to link_vortex:  set 
    virt_vortex->next to NULL prior to putting it into first[][] 
    structure.  The "next" structure in the vortex structure is
    used in make_interaction_list.  Vortex id now preserved via 
    change to sendarray logic. Vortex id number is now also passed.  
    Only the node which contains location 25 will open files related to 
    that location.  Also, only node with 8 writes part8file,  and only node 
    with boundary writes exitfile.  File to record when barrier site is 
    opened and closed added.  start_hereMPI added. vortex_start controls
    whether vortices are field cooled, read from file, or put down from 
    scratch.  nrerror accomodates graceful MPI exit. Current driving 
    installed.  System size increased.  Fake vortices, vortex addition 
    disabled, driving current added.
 *7.1.96: 
 * Summary of old comments:  All pinning sites have same strength.
    Hexagonal, clustered pinning allowed.  Small bug with get_parameters
    and cluster pinning fixed.  Bug in increment of time in run_ramp_rate
    fixed.  drop_vortex no longer checks number of vortices.  Periodically
    flushes sum_forces.  Vortices can be "field-cooled" using routine
    add_vortex_hexagonal; use of this routine controlled by variable
    field_cool.  smovie now called smovieV.  Be aware that excessively
    high ramp rates may make it appear that the pinning is not working.
    Data structure "potentials" removed, along with all old contour
    file code and fuzz code.    Can read in binary contour file. Code
    added to maintain completely unique ID numbers for each particle.
    Contains numerical recipies Rand routine.
    Had variable size, same strength non-overlapping pinning sites, which
    were changed to variable strength, same size non-overlapping sites.
    smovie format includes number of particles in frame, as well as
    the time.  MPI routines are in place.  Fast pinning
    addition algorithm never worked, so it is currently disabled. The
    fake fuzz vortices code is in place and working; fake vortices
    are not drawn on the contour.  Minor bug in print_parameters fixed.
    AVAL_CUTOFF is a global *variable* -- abuse of notation needed
    to prevent program from hanging on a particle drop.  Automatic
    AVAL_CUTOFF incrementation in place, with the maximum wait time
    between drops being MAX_DROP_WAIT.  The run parameters are now
    hardwired, rather than being obtained interactively or from a file.
    Each node uses its own SEED.  sum_forces_file is written at every
    step, in binary.  The movie/smovie files are closed between writes
    to avoid loss of data when run terminates.  Code writes and reads
    a special contour file, binary or ascii; also, it uses a start_file
    for initial vortex configuration.
*/

/* ## CIJOL: These are the compile time settings. Please check!*/

/* For applying a sinusoidal current */
/*#define USINGRATCHET 1 */
/* For sinusoidal pinscape (analytic) */
/*#define ANALYTICPINS 1*/

/* ## For generation of fractal dimension data:*/
/*#define GETFRACDIM 1*/

/* ## Length of time to spend taking data: */
#define DATAINTERVAL 100000

/* ## File interval setting; esp. important when fractal dimension is on: */
/* ## CIJOL notes this should probably be equal to datainterval value.*/
#define FILEINTERVAL 100000

/* ## How often to write fractal dimension data: */
#define FRACINTERVAL 60000

/* ## How often to write out velocity-velocity files: */
#define VELFILETIME   8000

/* ## Over what time period to average instantaneous vortex velocities: */
#define VELAVERAGETIME 200

/* ## How often to calculate positions of dislocations */
#define CALCDISLOCTIME 8000

/* ## How often to stop and take data (in current units): */
/* CIJOL: eventually this should come from file instead */
#define CURRENTINTERVAL 0.004

/* ## What is the maximum current to reach? */
/* CIJOL: this should come from file */
#define MAXCURRENT 0.32

/* ## Decimation factors for all voltage signals. Both int and float: */
#define DECIFACTOR 10
#define FDECIFACTOR 10.0

/* Jeff adding for gftolineplas temperature fit routine */
#define TOTALTAU 250
#define TAUSTEPS 2
#define DECIFACXTIMESTEP 0.05
#define HALFNUMBINS 1500

/* Jeff adding to sweep over currents and rhos */
#define MINRHO 0.08
#define MAXRHO 0.8
#define MINCUR 0.04
#define MAXCUR 0.28
#define RHODENS 12
#define CURDENS 24

#define RHOCURTAG 137
#define RHOCURFIELDS 4
#define RESULTTAG 274
#define RESULTFIELDS 7
#define MASTER_RANK 0

#define OUTLIERFRAC 0.0001
#define ZEROSTOWAIT 5

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_fit.h>
#include <gsl/gsl_sort.h>
#include <mpi.h>

/*#define TODOSIG  1*/
/*#define TODOSIG_EMP 1*/
#define PARATODOS 0

#define NR_END 1
#define FREE_ARG char*

#ifndef PI
#define PI 3.14159265
#endif

#define TRUE 1
#define FALSE 0

#define LEFT 0
#define RIGHT 1

#define ON  1
#define OFF 0

#define FROM_SCRATCH 0
#define FIELD_COOL 1
#define FROM_FILE 2
#define IN_PINS 3

#define SQRT3_2 .866025404

#define MAX(x,y) (x>y) ? x : y
#define MIN(x,y) (x<y) ? x : y
#define ABS(x)   (x<0) ? -x: x

#define WRITEMOVIETIME 400
/*#define WRITEMOVIETIME 2000*/
#define WRITEEXITTIME 8000

/* IMPORTANT NOTE!:  SX/NCELLS_Y, SY/NCELLS_Y must be integers equal to  */
/*                   the cutoff for the system. For example, if SX=36.0  */
/*                   and NCELLS_X = 6.0, then the cutoff for the vortex  */
/*                   potential is 36/6 = 6 lambda */

/* ALSO NOTE: NCELLS_X,NCELLS_Y both must be greater than 3 */
/*NOTE: if system size is altered, fake_row in add_fake_vortices must*/
/*also be altered.  2.23.95 */

/* For voltage noise work using 24x24 periodic sample*/
/* SX made global variable 9.5.96 */
/* Increasing size to 36 x 36  on 10.22.97 */
float SX = 48.0;
#define	DSX	   48.0                     /* Size of system in x-direction */
#define	SY  	   48.0                     /* Size of system in y-direction */

#define LEFTSX     0.0                      /* Left most value   */
/*RIGHTSX also made global variable 9.5.96*/
/*#define RIGHTSX    SX                    */ /* Right most value */
float RIGHTSX;

#define LEFTSX2    47.0                     /* Left side of second sample */
#define RIGHTSX2   50.0                     /* Right side of second sample */

#define LEFTBAR    (RIGHTSX + 2.0) /* Left side of vortex removal region*/
#define RIGHTBAR   (LEFTBAR + 4.0) /* Right side of vortex removal region */

/* NOTE: cutoff is now at 4 rather than 6. */
#define	NCELLS_X   12           /* The number index cells in the x direction */
#define	NCELLS_Y   12           /* The number index cells in the y direction */
#define	N_CELLS	   (NCELLS_X*NCELLS_Y)  /* Total number of index cells for the system */
#define	NULL_CELL  N_CELLS     /* Empty cell representing outside the sample */

#define N_POT      1000            /* The size of the lookup,lookup_par tables */

#define R_POT      300  /*The size of the river lookup table.*/
#define R_POT2     150   /*Half of r_pot*/

#define RELAX_TIME 10      /* MD steps between cell adjustments */

#define SEED        118768626 /*Seed reintroduced 3.16.95*/
#define PIN_PARABOLIC 1

#define PIN_PLACE_RANDOM     1
#define PIN_PLACE_HEXAGONAL  2
#define PIN_PLACE_SQUARE     3
#define PIN_PLACE_CLUSTER    4
#define PIN_PLACE_RATCHET    5

#define N_PIN_POT     100000

#define PATHPREPEND "/tmp/jdrocco/"
#define INPUTPREPEND "./"
#define RESULTPREPEND "/scratch/jdrocco/parcore/"

struct vector {
  float x;
  float y;
};

/* This structure contains all the data necessary for a parabolic well.    */
/* It is used when parabolic wells are calculated exactly, as is lookup_par  */
/* For details on the algorithm, see add_pinning().                        */

struct pinning_site {
  float x;
  float y;
  float Ap;
  float depth;
  float radius;
  int id;
  struct pinning_site *next;
};

struct pinning_site *pin_sites; /* array containing exact parabolic wells. */

/* This structure contains all the data needed for a single vortex. */

struct vortex {
	float 	x;	       /* x position of this vortex */
	float 	y;	       /* y position */
	struct 	vortex *next;  /* Pointer to next vortex in same cell */
	float 	fx;	       /* x-component of force on this vortex */
	float 	fy;            /* y-component of force on this vortex */
	int	id;	       /* Identifying number */
	float 	oldx;	       /* oldx position of this vortex */
	float 	oldy;	       /* oldy position */
	int     color;         /* For debugging, but not used with smovie */
	float   rivx;          /* For river structure use */
	float   rivy;         
	float   cum_disp;      /* For sinuosity calculation */
 	float   cum_x;
	float   avgfx;         /* For writing time-averaged velocity info. */
	float   avgfy;      
};

/* CIJOL Not needed unless you plan to use fake vortices */
#ifdef NOTUSING
void *lookup; /* table of the pinning forces */
              /* set to void so that it can be either char or struct vector*/
              /* for use with fuzz or parabolic pinning*/
#endif

/* Table storing info for exact parabolic wells. See add_pinning(): */
int lookup_par[N_POT][N_POT]; 

#ifdef NOTUSING
float pin_forces[N_PIN_POT]; /*Used for parabolic pinning only*/
#endif

/* Table storing river passage information.  For now, has same */
/* resolution as lookup table. */
#ifdef NOTUSING
int river[R_POT][R_POT];
#endif


/* This structure holds pointers to pairs of interacting particles */
/* See make_interaction_list() for more details.                   */

struct interaction_list {
  struct vortex *p1;
  struct vortex *p2;
};

/*Structure holding vortex data to be sent to another node.*/
struct send_vector{
  float x;
  float y;
  int id;
  int cell;
};

/* Pointers to first and last vortices in each index cell. +1 for NULL_CELL */
/* first and last needed for index cells. */
#define MAXCELL  100  /*Max number of vortices allowed in a single cell.*/
struct vortex *first[N_CELLS+1][MAXCELL];  
struct vortex *last[N_CELLS+1];	

#define TWOPI 6.2831853

float forces[50000];  /* lookup table for vortex-vortex forces */

float Av;  /*the vortex pre-factor*/

int next_id_no;  /*id number for next vortex added*/

int master_list_size;

/*Variables needed for MPI*/
int myid;
int num_procs;
int ihave_8;     /*Determines if node will write location 8 files*/
int ihave_25;    /*Determines if node will write location 25 files*/
int ihave_exit;  /*Determines if node will write exit file*/
int ihave_barrier; /*Determines if node will write barrier file*/

/*Commonly needed values put in global so don't have to calculate a*/
/*zillion times.*/
/*All start with SYS to indicate SYSTEM, and should not be altered.*/

float SYS_size_x;
float SYS_size_y;
float SYS_size2_x;
float SYS_size2_y;
float SYS_left_edge;
float SYS_right_edge;
float range2;
float SYS_scale_x;

/* This variable should be altered ONLY once, when pinning forces look-up table is made*/

float SYS_pin_scale;

float potential_scale;
float potential_F_max;
int update_list;
FILE *kmovie;
FILE *sumf_file;
FILE *sumfy_file;
FILE *sum_forces_file;
/*FILE *sum_forces_file2;*/
FILE *velocityfile;
/*FILE *stressfile;*/
FILE *rexitfile;
/*FILE *enterfile;*/
/*FILE *part8file;*/
/*FILE *part25file;*/
FILE *diagnosefile;
FILE *psub5file;
FILE *psub6file;
FILE *psub7file;
FILE *instotfile;
FILE *rhocurfile;
FILE *resultfile;

/***********************************************************************/
/******************** BEGIN CODE **************************************/
/**********************************************************************/

/*=========================== get_parameters_file =========================*/
/*This code gets the parameters from the user.*/
/* 5.15.95 Altered to read parameters from a file rather than*/
/*         interactively.  It is assumed that no graphics are to*/
/*         be displayed.*/
/* Back to reading parameters from file.*/

void get_parameters_file(dt,Ap,r_pin,num_pin,Ap_surface,num,type,drop,
		    pin_placement,order,vortex_start,chan_width,
		    current,increment,tot_time,filename,
		    id_string,use_contour,start_curr_number,
			 min_rhop,max_rhop,rhop_width,ratchetperiods,
			 maxcurr,maxtemper,temperature,temperature_incr,
			 period,jeffrho,jeffcur)
float *dt;
float *Ap;
float *r_pin;
int   *num_pin;
float *Ap_surface;
int   *num;
int   *type;
int   *drop;
int   *pin_placement;
float *order;
int   *vortex_start;
float *chan_width;
float *current;
float *increment;
int *tot_time;
char filename[];
char id_string[];
int  *use_contour;
int  *start_curr_number;
float *min_rhop,*max_rhop;
float *rhop_width;
int *ratchetperiods;
float *maxcurr;
float *maxtemper;
float *temperature;
float *temperature_incr;
float *period;
float *jeffrho;
float *jeffcur;
{
  void nrerror();

  int choise = PIN_PLACE_SQUARE;
  FILE *parameters;
  float Apin,r_pinin,rho_v;
  int dropin;
  float currentin,incrementin;
  int tot_timein;
  float tot_timemod;
  char trash[20];
  char stringin[20];
  int use_restart;
  int start_curr_number_in;
  float min_rho_p_in,max_rho_p_in;
  float rhop_width_in;
  int ratchetperiods_in;
  float maxcurrin,maxtemperin;
  float temperin,temperincrin;
  float period_in;

  /*Open file containing parameters*/
  if((parameters=fopen(filename,"r"))==NULL)
    nrerror("Error opening parameter file\n");
  
  /*Identifying string*/
  fscanf(parameters,"%s %s",trash,stringin);
  strcpy(id_string,stringin);

  /*type of pinning: permanently parabolic*/
  /* 1=parabolic. 2=fuzz. 3=sin. 4=log. 5=linear*/
  *type = 1;

  /*Maximum pinning force*/
  fscanf(parameters,"%s %f",trash,&Apin);
  *Ap = Apin;

  /*Range of pinning*/
  fscanf(parameters,"%s %f",trash,&r_pinin);
  *r_pin = r_pinin;

  /*Density of pinning: Minimum and maximum values, for ratchets */
  fscanf(parameters,"%s %f",trash,&min_rho_p_in);
  fscanf(parameters,"%s %f",trash,&max_rho_p_in);
  /*Convert this to number*/
  *num_pin = (int)(max_rho_p_in*SX*SY);
  *min_rhop = min_rho_p_in;
  *max_rhop = max_rho_p_in;

  /* Width of region with constant rho_p */
  fscanf(parameters,"%s %f",trash,&rhop_width_in);
  *rhop_width = rhop_width_in;

  /* Number of ratchet periods in sample */
  fscanf(parameters,"%s %d",trash,&ratchetperiods_in);
  *ratchetperiods = ratchetperiods_in;

  /*Pin placement*/
  /* 1=random.  2=hexagonal.  5=ratchet*/
  fscanf(parameters,"%s %d",trash,&choise);
  *pin_placement = choise;

  /*No provision for changing order*/
  if ((*pin_placement == PIN_PLACE_HEXAGONAL)||
      (*pin_placement == PIN_PLACE_SQUARE)){
    /*Order parameter for hexagonal pinning*/
    *order = 0;
  }
  else
    *order = 0;

  /*Density of vortices*/
  fscanf(parameters,"%s %f",trash,&rho_v);
  /*Convert this to number*/
  *num = (int)(*jeffrho*SX*SY);
  
  /*vortex force pre-factor*/
  fscanf(parameters,"%s %f",trash,&Av);

  /*Number of md steps between drops*/
  fscanf(parameters,"%s %d",trash,&dropin);
  *drop = dropin;

  /*Starting current*/
  fscanf(parameters,"%s %f",trash,&currentin);
  *current = *jeffcur;

  /*Current increment*/
  fscanf(parameters,"%s %f",trash,&incrementin);
  *increment = incrementin;

  /* Maximum current */
  fscanf(parameters,"%s %f",trash,&maxcurrin);
  *maxcurr = maxcurrin;

  /* Temperature */
  fscanf(parameters,"%s %f",trash,&temperin);
  *temperature = temperin;

  /* Temperature increment */
  fscanf(parameters,"%s %f",trash,&temperincrin);
  *temperature_incr = temperincrin;

  /* Max temperature */
  fscanf(parameters,"%s %f",trash,&maxtemperin);
  *maxtemper = maxtemperin;

  /*Total md steps to run*/
  fscanf(parameters,"%s %d",trash,&tot_timein);
  tot_timemod=((float)tot_timein)/(*jeffrho); 
  *tot_time = (int)tot_timemod;

  /*Never use pre-existing contour*/
  *use_contour = 0;

  /*Use restart file?*/
  fscanf(parameters,"%s %d",trash,&use_restart);

  if(use_restart)
    *vortex_start = FROM_FILE;
  else
    *vortex_start = FIELD_COOL;

  /*Vortices field cooled when id string starts "iv".*/
/*  if((id_string[0] == 'i') && (id_string[1]=='v')){
    *vortex_start = FIELD_COOL;
    fprintf(diagnosefile,"Field cooling vortices\n");
    fflush(diagnosefile);
  }*/
  /*Vortices placed in pins when id string starts "siv".*/
/*  else if((id_string[0] == 's') && (id_string[1]=='i')){
    *vortex_start = IN_PINS;
    fprintf(diagnosefile,"Vortices placed in pins\n");
    fflush(diagnosefile);
  }*/
  /*Vortices started from scratch when id string starts "riv".*/
/*  else if((id_string[0] == 'r') && (id_string[1]=='i')){
    *vortex_start = FROM_SCRATCH;
    fprintf(diagnosefile,"Vortices started from scratch\n");
    fflush(diagnosefile);
  }*/
/*  else{
    *vortex_start = FROM_FILE;
    fprintf(diagnosefile,"Starting vortices from file\n");
    fflush(diagnosefile);
  }*/

  /*Get starting current number*/
  fscanf(parameters,"%s %d",trash,&start_curr_number_in);
  *start_curr_number = start_curr_number_in;

  /* For analytic pins: period. */
  fscanf(parameters,"%s %f",trash,&period_in);
  *period=period_in;

  /*Channels not currently used:*/
  /*Channel width*/
  /*Percent time barrier pin is on*/
  /*Frequency of checking barrier status*/
  *chan_width = 0.0;

  /*time step permanently set*/
  *dt = .005;

  /*Surface barrier strength permanently set*/
  *Ap_surface = 0;
  fclose(parameters);
}

/*========================= open_force_file ==================*/
/* This routine opens all the data files we'll send output to */
void open_force_file(sumfname,sumfyname,
		     filename,psub5name,psub6name,psub7name,instotname)
     char sumfname[];
     char sumfyname[];
     /* Unused: */
     char filename[];
     char psub5name[];
     char psub6name[];
     char psub7name[];
     char instotname[];
{
  void nrerror();

  if((sumf_file = fopen(sumfname,"w"))==NULL)
    nrerror("Error opening sum_f.");

  if((sumfy_file = fopen(sumfyname,"w"))==NULL)
    nrerror("Error opening sumfy");

  if((instotfile = fopen(instotname,"w"))==NULL)
    nrerror("Error opening instot");

  /*if((psub5file = fopen(psub5name,"w"))==NULL)
    nrerror("Error opening psub5");*/
  
  /*if((psub6file = fopen(psub6name,"w"))==NULL)
    nrerror("Error opening psub6");*/
  
  /*if((psub7file = fopen(psub7name,"w"))==NULL)
    nrerror("Error opening psub7");*/

  /*if(ihave_8){
    if((sum_forces_file = fopen(filename,"w"))==NULL)
      nrerror("Error opening sum_forces_file");
  }*/
}
  
void init_output_files(id_string,
		       num)
char id_string[];
/*Unused:*/
int num;
{
  void nrerror();

  char temp[20];
  char filename[120];
  char kmovie_name[80];
  /* Unused: */
  char temp2[20];

#ifdef NOTUSING
  sprintf(temp2,"%d",num);
  if(num<10)
    strcpy(temp,"00000");
  else if(num<100)
    strcpy(temp,"0000");
  else if(num<1000)
    strcpy(temp,"000");
  else if(num<10000)
    strcpy(temp,"00");
  else if(num<100000)
    strcpy(temp,"0");

  if(num>100000)
    strcpy(temp,temp2);
  else
    strcat(temp,temp2);
#endif

  /* CIJOL changing to smovie format */
  /*strcpy(kmovie_name,"km");*/
  strcpy(kmovie_name,PATHPREPEND);
  strcat(kmovie_name,"sm");
  strcat(kmovie_name,id_string);
  /*CIJOL removing time stamp */
  /*strcat(kmovie_name,".");*/
  /*strcat(kmovie_name,temp);*/
  if((kmovie=fopen(kmovie_name,"w"))==NULL){
    nrerror("Error opening kmovie");
  }

  strcpy(filename,"vel");
  strcat(filename,id_string);
  /*CIJOL removing time stamp*/
  /*strcat(filename,".");*/
  /*strcat(filename,temp);*/
  /*if((velocityfile=fopen(filename,"w"))==NULL){
    nrerror("Error opening velfile");
  }*/

  strcpy(filename,"str");
  strcat(filename,id_string);
  /*CIJOL removing time stamp*/
  /*strcat(filename,".");*/
  /*strcat(filename,temp);*/
  /*if((stressfile=fopen(filename,"w"))==NULL){
    nrerror("Error opening stressfile");
  }*/

  if(ihave_exit){
    strcpy(filename,"mex");
    strcat(filename,id_string);
    /*CIJOL removing time stamp*/
    /*strcat(filename,".");*/
    /*strcat(filename,temp);*/
    /*if((rexitfile=fopen(filename,"w"))==NULL){
      nrerror("Error opening meanexit");
      }*/
  }

  strcpy(filename,"enter");
  strcat(filename,id_string);
  strcat(filename,".");
  strcat(filename,temp);
/*Disabling*/
/*  if((enterfile=fopen(filename,"w"))==NULL){
    nrerror("Error opening enterfile");
  }*/
}
/*============================== Main Loop==================================*/
	
main(argc,argv) 
int argc;
char *argv[];
{
  void initialize();
  void run_ramp_rate();
  void nrerror();
  void init_output_files();
  void gftolineplas();
  float **fmatrix( int, int, int, int);
  void free_fmatrix(float **, int, int, int, int);

  struct vortex *vortex;              /* pointer to all the vortices */
  struct interaction_list *master_list; 
                          /* Pointer to list of interacting particle pairs */  
  int  nV;	                     /* Current number of vortices in system */
  int  drop_steps;         /* The number of steps before calling drop_vortex */
  float	dt;	                          /* Size of a MD time step */
  int max_vortex_num;          /* Maximum # of vortices that will be dropped */
  int pinning_type;                       /* Are we doing fuzz or parabolic  */
  int time;
  char filename[120],temp[30],filename2[120];
  char partfile[120],partfile2[120];
  char sumfname[120];
  char sumfyname[120];
  char psub5name[120],psub6name[120],psub7name[120];
  char instotname[120];
  int time_span;           /* Amount of time to spend at a given drop rate*/
  float current;
  float currenty;
  float increment;
  char id_string[20];
  char pagenumber[10];
  char myidstr[20];
  int use_contour;
  int currentcount;
  int start_curr_num;
  float Apin;
  float maxcurrent;
  float maxtemper;
  float temperature;
  float temperature_incr;
  /* Unused: */
  char kmovie_name[80];
  float currentinterval;
  float jeffrho,jeffcur,realrho;
  int rhoind,curind;
  float meannpr,varnpr,stdnpr;
  MPI_Status status;
  float rhocurout[RHOCURFIELDS];
  int idnow;
  int linenow;
  float **fxmat;
  float **fymat;

  /* CIJOL Adding user prompt */
  /*printf("Enter myid (node number): ");
  scanf("%d",&myid);
  printf("Using id %d\n",myid);
  printf("Enter maximum current [IN Y]: ");
  scanf("%f",&maxcurrent);*/

// Initialize MPI
         MPI_Init(&argc,&argv); 
// Get the number of processes
         MPI_Comm_size(MPI_COMM_WORLD,&num_procs); 
// Get my process number (Rank)
         MPI_Comm_rank(MPI_COMM_WORLD,&myid); 

  /*Initialize variables*/
  RIGHTSX=SX;

  if(MASTER_RANK==myid){
  	if((rhocurfile = fopen("mrrhocur","w"))==NULL)
    	printf("Error opening rhocur\n");

  	if((resultfile = fopen("mrresult","w"))==NULL)
    	printf("Error opening result\n");
  }

  for(rhoind=0;rhoind<RHODENS;rhoind++){
	jeffrho=MINRHO+((float)rhoind*(MAXRHO-MINRHO)/((float)RHODENS));
	for(curind=0;curind<CURDENS;curind++){
		jeffcur=MINCUR+((float)curind*(MAXCUR-MINCUR)/((float)CURDENS));
		if(!(((rhoind*CURDENS+curind)%num_procs)==myid)) continue;

  time = 0;
  master_list_size = 0;
  next_id_no = 0;
  update_list = 1;

  /* Some of these global variables are affected when the system */
  /* size is changed slightly */
  SYS_size_x = SX;
  SYS_size_y = SY;
  SYS_size2_x = SX/2.0;
  SYS_size2_y = SY/2.0;
  SYS_left_edge = LEFTSX;
  SYS_right_edge = RIGHTSX;
  SYS_scale_x = ((float)(N_POT)/((float)SX));
  /* range2 is NOT affected when the system size is varied slightly */
  range2 = (1.5*(float)(SX)/NCELLS_X)*(1.5*(float)(SX)/NCELLS_X);
  /*range2 = 1.2*((float)(SX)/NCELLS_X)*((float)(SX)/NCELLS_X);*/

  strcpy(filename,"diag");
  sprintf(temp,"%d",myid);
  strcat(filename,temp);
  if((diagnosefile=fopen(filename,"w"))==NULL)
    nrerror("Error opening diag file");


  initialize(&vortex,&master_list,&nV,&dt,&drop_steps,&max_vortex_num,
	     &pinning_type,&current,&currenty,&increment,&time_span,
	     id_string,&use_contour,&start_curr_num,&Apin,&maxcurrent,
	     &maxtemper,&temperature,&temperature_incr,&jeffrho,&jeffcur);

  fprintf(diagnosefile,"Initialization complete\n");
  fflush(diagnosefile);

  /*...............................................................*/
  /* At this point, the program has completed primary initialization. */
  /* Now installing new control logic that will alternately sweep up */
  /* the current and then pause at a given current and take data.*/
  /* The program will assemble appropriate id strings itself, taking */
  /* from the user only the desired page number for the run. */
  /* Two types of forcefiles will be written: one set during the */
  /* current increment phase, the other during each pause.*/
  /* CIJOL: ALTERING to run at a single input current.  Still done */
  /* in two phases: the first phase just lets things equilibrate */
  /* a touch. */

  /* First, name the page number more appropriately */
  strcpy(pagenumber,id_string);
  sprintf(myidstr,"%d",myid);

  /*Begin loop*/
  currentcount = start_curr_num;
  /* For now, get current interval at compiletime*/
  currentinterval = CURRENTINTERVAL;
  /*printf("Currentinterval: %f\n",currentinterval);*/

  /*do{*/
  {
    currentcount++;

    /* Assemble the id_string for this set of files */
#ifdef NOTUSING
    strcpy(id_string,"ivn");
    strcat(id_string,pagenumber);
#endif
    strcat(id_string,".n");
    strcat(id_string,myidstr);
#ifdef NOTUSING
    strcat(id_string,".c");
    if(currentcount<100)
      strcat(id_string,"0");
    if(currentcount<10)
      strcat(id_string,"0");
    sprintf(temp,"%d",currentcount);
    strcat(id_string,temp);
#endif
    
    /* Call run_ramp_rate with instructions to increment the current */
    /* and write nothing.  This is indicated by the */
    /* flag variable after id_string.  Send 0 to increment the */
    /* current, and 1 to take data.*/
    /* Also set time_span appropriately.*/
#ifdef NOTUSING
    fprintf(diagnosefile,"RRR currentinterval %f increment %f dropsteps %d\n",
	    currentinterval,increment,drop_steps);
    time_span = (int) ((currentinterval/increment)*drop_steps);
    printf("RRR mode 0 timespan %d begincurr %f\n",time_span,current);
    fprintf(diagnosefile,
	    "RRR mode 0 timespan %d begincurr %f\n",time_span,current);
    fflush(diagnosefile);

    /* CIJOL SPECIAL open special files during current ramp. */
    strcpy(kmovie_name,"kmup");
    strcat(kmovie_name,id_string);
    /* CIJOL skipping */
    /*if((kmovie=fopen(kmovie_name,"w"))==NULL){
      nrerror("Error opening kmovie");
      }*/
    strcpy(sumfname,"fxup");
    strcat(sumfname,id_string);
    /* CIJOL skipping */
    /*if((sumf_file = fopen(sumfname,"w"))==NULL)
      nrerror("Error opening sum_f.");*/
    strcpy(sumfyname,"fyup");
    strcat(sumfyname,id_string);
    /* CIJOL skipping */
    /*if((sumfy_file = fopen(sumfyname,"w"))==NULL)
      nrerror("Error opening sumfy");*/

    /* CIJOL skipping */
    run_ramp_rate(drop_steps,&nV,&time,vortex,master_list,dt,time_span,
		  &current,&currenty,increment,0,temperature,&meannpr,&varnpr,fxmat,fymat,&linenow);
    /* CIJOL skipping */
    /*fclose(kmovie);
    fclose(sumf_file);
    fclose(sumfy_file);*/
#endif
    
    /* The current has now been incremented to a level where we */
    /* want to take some data.  Prepare all data files.*/

    strcpy(sumfname,PATHPREPEND);
    strcat(sumfname,"fx");
    strcpy(sumfyname,PATHPREPEND);
    strcat(sumfyname,"fy");
    strcpy(filename,"f8");
    strcpy(filename2,"f2");
    strcpy(partfile,"p8");
    strcpy(partfile2,"p2");
    strcpy(psub5name,"psub5");
    strcpy(psub6name,"psub6");
    strcpy(psub7name,"psub7");
    strcpy(instotname,PATHPREPEND);
    strcat(instotname,"it");
    strcat(sumfname,id_string);
    strcat(sumfyname,id_string);
    strcat(filename,id_string);
    strcat(partfile,id_string);
    strcat(partfile2,id_string);
    strcat(psub5name,id_string);
    strcat(psub6name,id_string);
    strcat(psub7name,id_string);
    strcat(instotname,id_string);
    
    init_output_files(id_string,
		      time);
    open_force_file(sumfname,sumfyname,
		    filename,psub5name,psub6name,psub7name,instotname);

    fxmat=fmatrix(0,(((int)(time_span/FDECIFACTOR))+5),0,nV);  // use mem instead of files
    fymat=fmatrix(0,(((int)(time_span/FDECIFACTOR))+5),0,nV);
  
    /* Call run_ramp_rate again, this time operating in data-collection */
    /* mode 1.  Take the specified amount of data.*/
    /* CIJOL time_span set above */
    /*time_span = DATAINTERVAL;*/
    printf("RRR mode 1 timespan %d begincurr %f\n",time_span,current);
    run_ramp_rate(drop_steps,&nV,&time,vortex,master_list,dt,time_span,
		  &current,&currenty,increment,1,temperature,&meannpr,&varnpr,fxmat,fymat,&linenow);
    fclose(sumf_file);
    fclose(sumfy_file);
    fclose(instotfile);
    /*fclose(psub5file);*/
    /*fclose(psub6file);*/
    /*fclose(psub7file);*/
    /*if(ihave_8){*/
      /*fclose(sum_forces_file);*/
      /* leave commented   fclose(part8file);*/
    /*}*/
    /*  if(ihave_25){*/
    /*    fclose(sum_forces_file2);*/
    /*    fclose(part25file);*/
    /*  }*/
  
    fclose(kmovie);
    /*if(ihave_8 || ihave_25)*/
    /*fclose(velocityfile);*/
    /*fclose(stressfile);*/
    /*if(ihave_exit)
      fclose(rexitfile);*/
    /*Disabling*/
    /*  fclose(enterfile);*/
    /* CIJOL running only 1 current 6.12.99 */
    /*}while(current < MAXCURRENT);*/
  }

  realrho=(float)nV/(SX*SY);
  stdnpr=sqrt(varnpr);

  if(!(MASTER_RANK==myid)){
	rhocurout[0]=realrho;
	rhocurout[1]=jeffcur;
	rhocurout[2]=meannpr;
	rhocurout[3]=stdnpr;
	MPI_Send(&rhocurout, RHOCURFIELDS, MPI_FLOAT, MASTER_RANK, RHOCURTAG,MPI_COMM_WORLD);
  }

  if(MASTER_RANK==myid){
  	fprintf(rhocurfile,"%f %f %f %f \n",realrho,jeffcur,meannpr,stdnpr);
	for(idnow=1;idnow<num_procs;idnow++){
		MPI_Recv(&rhocurout,RHOCURFIELDS,MPI_FLOAT,MPI_ANY_SOURCE,RHOCURTAG,MPI_COMM_WORLD,&status);
		fprintf(rhocurfile,"%f %f %f %f \n",rhocurout[0],rhocurout[1],rhocurout[2],rhocurout[3]);
	}
	fflush(rhocurfile);
  }

  gftolineplas(nV,jeffcur,jeffrho,realrho,myid,num_procs,fxmat,fymat,linenow,pagenumber);

    free_fmatrix(fxmat,0,(((int)(time_span/FDECIFACTOR))+5),0,nV);  // use mem instead of files
    free_fmatrix(fymat,0,(((int)(time_span/FDECIFACTOR))+5),0,nV);
	
 }
}

  if(MASTER_RANK==myid){
  	fclose(rhocurfile);
  	fclose(resultfile);
  }

// Finalize
         MPI_Finalize(); 

  exit(0); /* End of program! */
}

/*============================ run_ramp_rate ===========================*/
/* Collects force data for a specified amount of drops at a specified*/
/* ramp rate.*/
/* Now set for two-stage operation.  If called with operating flag of 0,*/
/* increments current but does not take data.  If called with operating */
/* flag of 1, takes data without incrementing current.*/
void run_ramp_rate(drop_steps,nV,time,vortex,master_list,dt,time_span,current,
		   currenty,increment,operate_flag,temperature,mnprat,vnprat,fxmat,fymat,linenow)
int drop_steps;  /*how fast to drop vortices*/
int *nV;
int *time;
struct vortex *vortex;
struct interaction_list *master_list;
float dt;          /*time step to use*/
int time_span;  /*how long to run*/
float *current,*currenty;
float increment;
int operate_flag;  /* Controls program execution between two modes.*/
float temperature;
float *mnprat;
float *vnprat;
float **fxmat;
float **fymat;
int *linenow;
{
  void calc_vortex_force();
  void calc_pinning_force();
  void moveVortices();
  void drawIt();
  void aval_statistics();
  void dumpRiver();
  void nrerror();
  void calc_current_force();
  /* Unused: */
  void drop_vortex();
  void voronoi_construct();
  void drop_vortex_debug();
  void dumpRiver();
  void updateRiver();
  float *fvector(int,int);
  void free_fvector(float *,int,int);

  int local_time=0;
  float asumX=0;
  float *asumf;
  float *asumfy;
  float *npstore;
  float exvariance = 0;
  float exmean = 0;
  int  mN = 0;
  float meannp,varnp;
  float sumnp,sumsqnp;
  int meaning;
  int storetimes=0;
  int k;

  asumf = fvector(0,*nV);
  asumfy= fvector(0,*nV);
  npstore=fvector(0,(((int)(time_span/FDECIFACTOR))+5));
  *linenow=0;

  for(k=0;k<*nV;k++){
	asumf[k]=0.0;
	asumfy[k]=0.0;
	}

  fprintf(diagnosefile,
	  "Beginning run_ramp_rate with current %f, currenty %f\n",*current,
	  *currenty);
  fflush(diagnosefile);

  /* What follows is the main loop:  drop a particle if need be, then      */
  /* calculate the foces on the vortices, and then move them appropriately. */
  /* then calculate (and output) rmds data for this time step. Draw if it   */
  /* is necessary. When all the md steps are done, simply call exit.        */
  
  while(local_time<time_span){

    /*VORTEX DROPPING DISABLED 8.26.96*/
    /*Vortex dropping re-enabled 11.13.96*/
    /* Vortex dropping disabled again 6.23.97*/
    /*if(!((*time)%drop_steps))*/
      
      /*drop_vortex(vortex,nV);*/      /*Add a vortex to the system */
      /* When a vortex added, need to update master_list */
      /*update_list = 1;*/
    /*}*/

    /*This code increments current*/
    /* Current will only be incremented in mode 0 operation.*/
    /* CIJOL Always increment */
    /*if(!operate_flag){*/
    {
      if(!((*time)%drop_steps)){
	*current += increment; 
	/**currenty += increment;*/
	fprintf(diagnosefile,"Incrementing current t=%d, curr=%f\n",
		*time,*current);
	if(fabs(*current) > 100)
	  nrerror("Problem with current value");
      }
    }

    /* Movie and other files will only be written in mode 1 operation.*/
    /* CIJOL movie always written */
    /*Write to movie if desired*/
    if (!((*time) % WRITEMOVIETIME))
      drawIt(vortex,*nV,*time);

#ifdef NOTUSING
    if(operate_flag){

      /* Calculate displacement locations if desired*/
      if(!((*time)% CALCDISLOCTIME)){
	voronoi_construct(*nV,vortex,*time);
      }      

#ifdef GETFRACDIM
      /* Special dump of river at beginning of time interval */
      if(!local_time)
	dumpRiver(local_time,id_string);
      
      /* Dump river data structure at specified time */
      if((local_time)&&(!((local_time)%FRACINTERVAL)))
	dumpRiver(local_time,id_string);
#endif
    }/*end operate flag switch*/
#endif

    /* Find the vortex-vortex forces */
    calc_vortex_force(master_list,vortex,nV,time);

    /* Find the pinning forces */
#ifdef ANALYTICPINS
    calc_analytic_pinning_force(vortex,*nV);
#else
    /* For random pinning: */
    calc_pinning_force(vortex,*nV);
#endif

    /* Find the current and temperature forces, separately */
    calc_current_force(vortex,*nV,*current,*currenty,*time,temperature,
		       drop_steps);

    aval_statistics(vortex,nV,*time,&asumX,asumf,asumfy,&exvariance,
		    &exmean,&mN,operate_flag,npstore,&storetimes,fxmat,fymat,linenow);
    
    /*Periodically flush sum_forces_file*/
    if(operate_flag){
      if(!((*time)%8192)){
	fflush(sumf_file);
	/*fflush(sum_forces_file);*/
	fflush(sumfy_file);
	/*fflush(psub5file);*/
	/*fflush(psub6file);*/
	/*fflush(psub7file);*/
      }
    }

    moveVortices(vortex,nV, dt);
    
    /*Rivers updated in mode 1 */
    /* CIJOL removing to speed code */
    /*if(operate_flag)
      updateRiver(vortex,*nV);*/

    (*time)++;
    local_time++;
  }

	sumnp=0.0;
	sumsqnp=0.0;
	for(meaning=0;meaning<storetimes;meaning++)
		sumnp+=npstore[meaning];
	meannp=sumnp/(float)storetimes;
	*mnprat=meannp;
	for(meaning=0;meaning<storetimes;meaning++)
		sumsqnp+=(npstore[meaning]-meannp)*(npstore[meaning]-meannp);
	varnp=sumsqnp/(float)storetimes;
	*vnprat=varnp;

  /*One last dump of the river:*/
#ifdef GETFRACDIM
  if(operate_flag)
    dumpRiver(local_time,id_string);
#endif
    free_fvector(asumf,0,*nV);
    free_fvector(asumfy,0,*nV);
    free_fvector(npstore,0,(((int)(time_span/FDECIFACTOR))+5));
}
/*============================ anneal_vortex ===========================*/
/* For a system started in a field-cooled state, runs annealing before */
/* any of the normal functions of the program begin. */
void anneal_vortex(drop_steps,nV,vortex,master_list,dt,current,currenty,
		   increment)
int drop_steps;  /*how fast to drop vortices*/
int *nV;
struct vortex *vortex;
struct interaction_list *master_list;
float dt;          /*time step to use*/
float *current,*currenty;
float increment;
{
  void calc_vortex_force();
  void calc_pinning_force();
  void calc_analytic_pinning_force();
  void calc_current_force();
  void moveVortices();

  int local_time=0;
  float temperature;

  fprintf(diagnosefile,"Beginning anneal_vortex\n");
  fflush(diagnosefile);

  /* Calculate the forces on the vortices, and then move them appropriately. */
  
  temperature = 3.0;
  while(temperature>0.0){

    /*This code increments temperature*/
    if(!(local_time%drop_steps)){
      temperature -= increment; 
      printf("T=%2.2f  ",temperature);
      fflush(stdout);
    }

    /* Find the vortex-vortex forces */
    calc_vortex_force(master_list,vortex,nV,&local_time);
    
    /* Find the pinning forces */
#ifdef ANALYTICPINS
    calc_analytic_pinning_force(vortex,*nV);
#else
    calc_pinning_force(vortex,*nV);
#endif

    /* Find the current and temperature forces.  Set current to zero. */
    calc_current_force(vortex,*nV,0.0,0.0,0,temperature,drop_steps);

    moveVortices(vortex,nV, dt);

    local_time++;
  }
}

/*============================= Initialize =================================*/
/* This routine does all the necessary setup for the program.  First, we ask*/
/* the user to enter various parameters (get_parameters).  Next we setup the*/
/* graphics display, if the user has asked for it (in get_parameters).  Next*/
/* we allocate memory for vortices and lookup tables.  Now setup the pinning*/
/* substrate.  Lastly, we set nV = 0, and open the output files.            */
/* NEW March 96: Vortices can be placed in triangular array*/

void initialize(vortex,master_list,nV,dt,drop_steps,max_vortex_num,
		pinning_type,current,currenty,increment,tot_time,id_string,
		use_contour,start_curr_num,Apout,maxcurr,maxtemper,
		temperature,temperature_incr,jeffrho,jeffcur)
     struct vortex **vortex;         /*pointer to all the vortices*/
     struct interaction_list **master_list;
                              /*pointer to list of interacting particle pairs*/
     int *nV;                         /*Current number of vortices in system */
     float *dt;                       /* Size of an MD time step*/
     int *drop_steps;        /*The number of steps before calling drop_vortex*/
     int *max_vortex_num;        /*Maximum # of vortices that will be dropped*/
     int *pinning_type;                /*Are we doing fuzz or parabolic */
     float *current;
     float *currenty;
     float *increment;
     int *tot_time;
     char id_string[];
     int *use_contour;
     int *start_curr_num;
     float *Apout;
     float *maxcurr;
     float *maxtemper;
     float *temperature;
     float *temperature_incr;
     float *jeffrho;
     float *jeffcur;
{
  void nrerror();
  void make_lookup_tableII();
  void start_here();
  void add_vortex_hexagonal();
  void add_vortex_in_pins();
  /* Unused: */
  void init_pinning_tables();
  void print_parameters();

  float Ap;                       /* Max strength of pinning sites */
  float r_pin;                    /*range of a parabolic pinning trap*/
  float Ap_surface;               /*Strength of surface barrier*/
  int pin_placement;
  float order;
  int num_pin;
  int vortex_start;
  char starting_file[80];
  char filename[120];
  char temp[80];
  float chan_width;
  char local_id_string[20];
  int anneal_time;
  float anneal_increment;
  float min_rhop,max_rhop;
  float rhop_width;
  int ratchetperiods;
  float period;
  /* Unused: */
  int i,j;
  int temp2;

  *currenty=0;

  sprintf(temp,"%d",myid);
  strcpy(filename,INPUTPREPEND);
  strcat(filename,"Pco");
//  strcat(filename,temp);   Dont need this for mult nodes

  get_parameters_file(dt,&Ap,&r_pin,&num_pin,&Ap_surface,max_vortex_num,
		      pinning_type,drop_steps,&pin_placement,&order,
		      &vortex_start,&chan_width,current,increment,
		      tot_time,filename,id_string,use_contour,
		      start_curr_num,&min_rhop,&max_rhop,&rhop_width,
		      &ratchetperiods,maxcurr,maxtemper,temperature,
		      temperature_incr,&period,jeffrho,jeffcur);

  fprintf(diagnosefile,"Parameters obtained\n");
  fflush(diagnosefile);

  *Apout = Ap;

  /* Not needed */
  /*init_pinning_tables(r_pin);*/

  /* Get memory for vortices */

  *vortex = (struct vortex*) malloc((2*(*max_vortex_num))*sizeof(struct vortex));
  *master_list = (struct interaction_list*) malloc(((*max_vortex_num)*250)*sizeof(struct interaction_list));
  
  if (vortex == NULL) nrerror("Can't get memory for vortices.\n");
  if (master_list == NULL) nrerror("Can't get memory\n");

  pin_sites = (struct pinning_site *) malloc(sizeof(struct pinning_site)*(2000 + num_pin*2));
  if (pin_sites == NULL) nrerror("Can't get memory for pin_sites\n");

#ifdef NOTUSING
  /*Zero out river structure*/
  for(i=0;i<R_POT;i++){
    for(j=0;j<R_POT;j++){
      river[i][j]=0;
    }
  }
#endif

  ihave_8 = TRUE;
  ihave_25 = TRUE;
  ihave_exit = TRUE;

  ihave_barrier=TRUE;

  /*#############################################*/
  /*Put down vortices as desired.  Either:*/
  /* 1.  No vortices initially, added to system normally*/
  /* 2.  Vortices in some configuration read from file*/
  /* 3.  Vortices placed in triangular array */
  /* 4.  Vortices placed IN PINNING SITES */

  *nV = 0;  /* Initialize the number of vortices */

  if(vortex_start == FROM_SCRATCH);
    /*Do nothing here; vortices added as the program runs.*/

  else if(vortex_start == FIELD_COOL){
    /* Put the vortices down, and then **anneal** them (see below) */
    add_vortex_hexagonal(*vortex,nV,max_vortex_num);
  }

  else if(vortex_start == FROM_FILE){

    /* Special: This sets system size properly 10.4.96*/
    /* This operates only for systems without s or r prefix*/
    if(pin_placement == PIN_PLACE_SQUARE){
      fprintf(diagnosefile,"System size not altered\n");
      fflush(diagnosefile);
    }
    else{
      fprintf(diagnosefile,"Setting system size properly\n");
      fflush(diagnosefile);
      add_vortex_hexagonal(*vortex,nV,max_vortex_num);
    }

    *nV = 0;

    /*Open start_file*/
    strcpy(starting_file,"kst");
    strcat(starting_file,id_string);
    strcat(starting_file,"n");
    strcat(starting_file,temp);
    
#ifdef TODOSIG_EMP
    temp2 = PARATODOS;
    sprintf(temp,"%d",temp2);
#endif
    start_here(*vortex,nV,starting_file); 
  }

  /* Initialize the pinning.*/

  /* CIJOL: For specialized noise code, the id_string is not */
  /* sufficiently unique at this point.  We need to add the */
  /* node number.  8.29.97*/
  sprintf(temp,"%d",myid);
  strcpy(local_id_string,id_string);
  strcat(local_id_string,"n");
  strcat(local_id_string,temp);

  /* CIJOL Call this only if using random pinning */
#ifdef ANALYTICPINS
  /* Overloading this data structure... */
  pin_sites[0].Ap=Ap;
  pin_sites[0].x=period;
#else
  make_lookup_tableII(Ap,r_pin,&num_pin,pin_placement,order,chan_width,
		      *use_contour,local_id_string,min_rhop,max_rhop,
		      rhop_width,ratchetperiods);
#endif
  
  fprintf(diagnosefile,"Pinning initialized\n");
  fflush(diagnosefile);

  /*Important: THIS vortex configuration must go AFTER the pinning */
  /* has been initialized.*/
  if(vortex_start == IN_PINS)
    add_vortex_in_pins(*vortex,nV,num_pin);
  
  /* Also use local id string here: */
  /* CIJOL don't bother */
#ifdef NOTUSING
  print_parameters(*dt,Ap,r_pin,num_pin,*max_vortex_num,*drop_steps,
		   vortex_start,*current,local_id_string);
#endif
  
  /* Anneal vortices */
  /* CIJOL removing */
  /*printf("Not annealing vortices\n");*/
  if(vortex_start == FIELD_COOL){
    anneal_time = 800;
    /*anneal_increment = 0.1;*/
    anneal_increment=0.4;
    anneal_vortex(anneal_time,nV,*vortex,*master_list,*dt,current,currenty,
		  anneal_increment);
  }
}

/*============================= print_parameters ===========================*/
/* This routine prints out the run's parameters */

void print_parameters(dt,Ap,r_pin,num_pin,num,drop,vortex_start,current,
		      id_string)
float   dt;
float   Ap;
float   r_pin;
int     num_pin;
int     num;
int     drop;
int     vortex_start;
float   current;
char    id_string[];
{
  FILE *note_file;
  char filename[120];

  strcpy(filename,id_string);
  note_file = fopen(filename,"w");

  fprintf(note_file,"riv:\n");
  fprintf(note_file,"The system size is %.3f X %.3f\n",SX,SY);
  fprintf(note_file,"The time step is %f\n",dt);
  fprintf(note_file,"The maximum pinning force is %f\n",Ap);
  fprintf(note_file,"The radius of the pinning sites is %f\n",r_pin);
  fprintf(note_file,"The number of pinning sites is %d\n",num_pin);
  fprintf(note_file,"Requested number of vortices is %d\n",num);
  fprintf(note_file,"Particle drop / current increment at %d\n",drop);
  fprintf(note_file,"Initial current of %f\n",current);
  switch(vortex_start){
  case FIELD_COOL:
    fprintf(note_file,"Vortices field cooled\n");
    break;
  case FROM_FILE:
    fprintf(note_file,"Vortices started from file\n");
    break;
  case FROM_SCRATCH:
    fprintf(note_file,"Vortices started from scratch\n");
    break;
  }
  fprintf(note_file,"Run on %d processors\n",num_procs);
  fclose(note_file);
}

#ifdef NOTUSING
/*============================= init_pinning_tables =========================*/
/* This routine initializes the lookup table pin_forces.  Pin_forces contains*/
/* the magnitude of the force/distance at a given distance squared.  The */
/* advantages of this are two-fold.  One, we don't need to use a square root.*/
/* Two, we can avoid divisions and trigs. */

void init_pinning_tables(r_pin)
float r_pin;
{
  int i;
  float r,r2,Force,scale;

  scale = r_pin*r_pin/N_PIN_POT;

  SYS_pin_scale = 1.0/scale;

  pin_forces[0] = 0.0;
  
  for(i=1; i<=N_PIN_POT; i++){
    r2 = i*scale;
    r = (float)sqrt((double)(r2));

    if (r!=0.0){
      Force = -1.0/r_pin;
    }
    else
      Force = 0.0;

    pin_forces[i] = Force;
  }
}
#endif

/*============================= drop_vortex================================*/

/* This routine adds vortices to the system in the appropriate manner. */
/* Currently, we add vortices in a narrow strip toward the left of     */
/* the system.                                                         */

void drop_vortex(vortex,nV)
struct vortex *vortex;
int *nV;
{
  float Rand();

  int i;

  i = *nV;

  /* Set the x-coord for somewhere in unpinned region */
  vortex[i].x = Rand() * (LEFTSX);
  /* Set the y-coord for anywhere in the system. */
  vortex[i].y = Rand()* SY;
  /* Not currently used- for debugging */  
  vortex[i].color = 1;
  /* ptr to next vortex in cell; null=no more vortices in list. */
  vortex[i].next = NULL;
  /* Set force in the x direction on vortex = 0 at beginning. */
  vortex[i].fx = 0.0;
  /* Set force in the y direction on vortex = 0 at beginning. */
  vortex[i].fy = 0.0;
  /* Set old x pos to curr one - needed for Euler in moveVortices */
  vortex[i].oldx = vortex[i].x;
  /* Set old x pos to curr one - needed for Euler in moveVortices */
  vortex[i].oldy = vortex[i].y;
  /* Set vortex id - ie tenth vortex dropped has id 9 (0-9 = 10) */
  vortex[i].id = next_id_no;
  /* Zero vortex displacement */
  vortex[i].cum_disp = 0;
  vortex[i].cum_x = 0;
  vortex[i].avgfx = 0;
  vortex[i].avgfy = 0;

  /* increase vortex number - see below */
  i++;
  next_id_no++;

  /* update the vortex number. */
  *nV = i;
}
/*============================== drop_vortex===============================*/

/* This routine adds vortices to the system in the appropriate manner. */
/* Currently, we add vortices in a narrow strip toward the left of     */
/* the system.                                                         */
/* FOR DEBUG: puts down vertical line of vortices*/

void drop_vortex_debug(vortex,nV)
struct vortex *vortex;
int *nV;
{
  int i,j;

  i = *nV;

  for(j=0;j<12;j++){
    vortex[i].x = 2.0;   /* Set the x-coord for somewhere in unpinned region */
    vortex[i].y = j*2 + 0.5;          /* Set the y-coord for anywhere in the system.      */
    vortex[i].color = 1;               /* Initialize color to 1 - not currently used- for debugging */
    vortex[i].next = NULL;             /* ptr to next vortex in cell; null=no more vortices in list. */
    vortex[i].fx = 0.0;                /* Set force in the x direction on vortex = 0 at beginning. */
    vortex[i].fy = 0.0;                /* Set force in the y direction on vortex = 0 at beginning. */
    vortex[i].oldx = vortex[i].x;      /* Set old x pos to curr one - needed for Euler in moveVortices */
    vortex[i].oldy = vortex[i].y;      /* Set old x pos to curr one - needed for Euler in moveVortices */
    vortex[i].id = next_id_no;                  /* Set vortex id - ie tenth vortex dropped has id 9 (0-9 = 10) */
    /* Zero displacement */
    vortex[i].cum_disp = 0;
    vortex[i].x = 0;
    i++;                               /* increase vortex number - see below */
    next_id_no++;
  }
  *nV = i;                           /* update the vortex number. */
}

/*=========================== calc_vortex_force() ==========================*/

/* This routine first prepares for calculating the vortex forces by  */
/* re-making the master list if necessary. The actual force calculations */
/* occur in get_vortex_forces.*/

void calc_vortex_force(master_list,vortex,nV,time)
struct interaction_list *master_list;
struct vortex *vortex;
int *nV;
int *time;
{
  void get_vortex_forces();
  void adjustCells();
  int make_interaction_list();

  static long int master_list_size = 0;
  
  /* range2 is the cuttoff squared. It's a bit too large to ensure that */
  /* no new particles enter interaction range durring the RELAX_TIME md */
  /* steps before the list is updated.                                  */

  if ((update_list)||(!((*time)%RELAX_TIME))) {
    adjustCells(vortex, *nV);	/* Put all vortices in correct cells */    
    /* Re-make interaction list */
    master_list_size = make_interaction_list(master_list); 
    update_list = 0; 
  }

  get_vortex_forces(master_list,master_list_size);

}

/*============================ get_vortex_forces============================*/
	
/* This routine calculates the net vortex-vortex forces on all the particles */
/* It goes through the master_list, and calculates the force between all */
/* pairs of vortices */

void get_vortex_forces(master_list,list_size) 
struct interaction_list *master_list;
long int list_size;
{
  void force();
  
  struct vortex *cv, *p;
  float  fx, fy;
  int i;
	
  for(i=0; i<list_size; i++) {
    cv = master_list[i].p1;
    p  = master_list[i].p2;

    force(p,cv,&fx,&fy);   /* Get force on p due to cv */
    p->fx  += fx;
    cv->fx -= fx;	  /* Force on cv due to p = -(force on p due to cv). */
    p->fy  += fy;
    cv->fy -= fy;
  }
			
}

/*============================== make_interaction_list======================*/

/* This function updates the master list, which is essentially a neighbor */
/* The list is generated by using the cell index method as described */
/* in Allen & Tisdley(sp?). We look at each particle and determine its */
/* neighbors by cell index. BUT, cell index also gives us a number of */
/* particles which fall outside the interaction cutoff boundary. */
/* make_interaction_list checks to see if the cell-index particles are */
/* really in range. If they are, then the pair is added to the interaction */
/* list. The list is updated  every  RELAX_TIME md steps. This little trick */
/* increases run speed a small amount.  */
int make_interaction_list(master_list) 
struct interaction_list *master_list;
{
  int neighbor();
  int in_range();

  struct vortex *cv, *p;
  int 	cell, nn, n;

  int list_pos = 0;

  /* This loop is patterned after that in Computer Simulations of Liquids,
     by M. Allen, as described in Sec. 5.3.2.  We divide the sample up into 
     N_CELLS rectangular cells. */

  for (cell = 0; cell < N_CELLS; cell++) {

    /* Choose the current vortex to be the first one in the current cell.  */
    cv = first[cell][1];  

    while (cv != NULL) {	/* Loop over each vortex in this cell */
      p = cv -> next;	/* p points to the next vortex in the current cell */
      
      while (p != NULL) {
	/* Run thru all vortices in current cell after c_vortex */
	if (in_range(p,cv)) {
	  master_list[list_pos].p1 = cv;
	  master_list[list_pos].p2 = p;
	  list_pos++;
	}
	p = p->next;
      }
			
      for (nn = 0; nn < 4; nn++) { 	/* Loop over four neighboring cells */
	n = neighbor(cell,nn);
	p = first[n][1];	

	while (p != NULL) {	
	  if (in_range(p,cv)) {
	    master_list[list_pos].p1 = cv;
	    master_list[list_pos].p2 = p;
	    list_pos++;
	  }
	  p = p->next;
	}
      }
      
      cv = cv->next;
    }
  }
	
  return list_pos;

}

/*============================= in_range ====================================*/

/* Taking into account periodic b.c., this function checks to see */
/* if particles p and v are in the given interaction range.       */
/* It returns true (1) if they are in range, otherwise 0.         */

int in_range(p,v)
struct vortex *p;
struct vortex *v;
{
  float dx,dy,r2;
  static float size2_y = (float)(SY)/2.0;

  dx = p->x - v->x;
  dy = p->y - v->y;

  /* Apply minimum image condition */

  if (dx>SYS_size2_x)  dx -= SX;
  if (dx<-SYS_size2_x) dx += SX;
  
  if (dy>size2_y)  dy -= SY;
  if (dy<-size2_y) dy += SY;


  r2 = dx*dx + dy*dy;

  if (r2 > range2)
    return 0;
  else
    return 1;

}


/*========================= calc_pinning_force=============================*/

/* This routine calculates the fuzz pinning force and the pinning for */
/* the twinning plane, where the twinning plane is a parabolic well. For */
/* fuzz, the function checks if the force acting on the particle is    */
/* greater than the fuzz pinning - if the force isn't great enough, the net */
/* force on the vortex is set to zero - ensuring that the vortex won't move. */
/* Contains code to apply current.  Also contains pin force stress file */
/* writes.*/

void calc_pinning_force(vortex,num_par)
struct vortex *vortex;
int num_par;
{
  int i,x,y,index;
  float x1,y1,dx,dy;
  float Ap;
  float xscale;

  static float yscale = ((float)N_POT)/((float)SY);

  xscale = SYS_scale_x;

  for(i=0; i<num_par; i++) {
    x = (int)((vortex[i].x)*xscale);
    y = (int)(vortex[i].y * yscale);

    /*These error traps are occasionally needed*/
    if (x < 0) {
      fprintf(diagnosefile,"Error I. x=%d, v.x=%f\n",x,vortex[i].x);
      fflush(diagnosefile);
      x += N_POT;
    }
    
    if (x >= N_POT) {
      fprintf(diagnosefile,"Error II\n");
      fflush(diagnosefile);
      x -= N_POT;
    }

    if (y < 0) {
      fprintf(diagnosefile,"Error III\n");
      fflush(diagnosefile);
      y += N_POT;
    }
    
    if (y >= N_POT) {
      fprintf(diagnosefile,"Error IV\n");
      fflush(diagnosefile);
      y -= N_POT;
    }

    index = lookup_par[y][x];
    
    if (index) {
      x1 = pin_sites[index].x;
      y1 = pin_sites[index].y;
      Ap = pin_sites[index].Ap;
      
      dx = vortex[i].x - x1;
      dy = vortex[i].y - y1;
      
      if (dx>SYS_size2_x)  dx -= SX;
      if (dx<-SYS_size2_x) dx += SX;
      
      if (dy>SYS_size2_y)  dy -= SY;
      if (dy<-SYS_size2_y) dy += SY;
      
      vortex[i].fx += Ap*dx;
      vortex[i].fy += Ap*dy;

    }
    
    /* Needed only for fake vortices */
#ifdef NOTUSING
    vortex[i].fx += (((struct vector *)lookup)+y*N_POT + x)->x;
    vortex[i].fy += (((struct vector *)lookup)+y*N_POT + x)->y;
#endif
    
  }
}


/*================= calc_analytic_pinning_force=========================*/
/* This routine calculates the pinning force from an analytic function */
/* such as sinusoidal in x. */
void calc_analytic_pinning_force(vortex,num_par)
struct vortex *vortex;
int num_par;
{
  int i;
  float x1,y1,dx,dy;
  float Ap,period;
  float pinforce;

  Ap = pin_sites[0].Ap;
  period = pin_sites[0].x;

  for(i=0; i<num_par; i++) {
    pinforce = Ap*(float)sin((double)(TWOPI*vortex[i].x/period));
    vortex[i].fx += pinforce;
  }
}

/*======================== calc_current_force ====================*/
/* Separating out current and temperature forces from pinning routine */
void calc_current_force(vortex,nV,current,currenty,time,temperature,
			drop_steps)
struct vortex *vortex;
int nV;
float current,currenty;
int time;
float temperature;
int drop_steps;
{
  float gasdev();

  int i;
  float Xcurrent;

  /* Determine the present value of the current (sinusoidally varying)*/
#ifdef USINGRATCHET
  Xcurrent = 
    current*(float)sin((double)(TWOPI*(float)time/(float)drop_steps));
#else
  Xcurrent=current;
#endif

  for(i=0;i<nV;i++){
    
    vortex[i].fx += Xcurrent;
    vortex[i].fy += currenty;
    
    /* Adding temperature 10.29.98 */
    /* Mean = 0, std dev = temperature. */
    if(temperature>0.0){
      vortex[i].fx += temperature*gasdev();
      vortex[i].fy += temperature*gasdev();
    }  
  }
}

/*========================  aval_statistics ==================*/
/* For older code see next routine */
/* New calculation for sum_forces, 6.3.96*/
/* Calculation of sum_X removed, 5.10.95*/
/* Added write-out of all vortex velocities at certain times 3.10.96*/
/* New:  Two files added to record time of entry and exit of vortices*/
/* from system.  Entry time requires use of oldx.  Exit time taken as*/
/* time when vortex is removed.*/
/* NEW: Decimates file by averaging over every 10 steps.*/
void aval_statistics(vortex,nV,time,asumX,asumf,asumfy,exvariance,exmean,mN,
		     operate_flag,npstore,timefrstore,fxmat,fymat,linenow)
struct vortex *vortex;
int *nV;
int time;
float *asumX;
float asumf[];
float asumfy[];
float *exvariance;
float *exmean;
int *mN;
int operate_flag;
float npstore[];
int *timefrstore;
float **fxmat;
float **fymat;
int *linenow;
{
  /* Unused: */
  void remove_a_vortex();
  float *fvector(int,int);
  void free_fvector(float *,int,int);

  int i,j,k;
  float sum_X;
  float *sumf;
  float *sumfy;
  struct vortex *v;
  int writevelocity=0;
  float x;
  float variance,mean;
  int locmN;
  int numpart;
  float y,fx,fy;
  /* Unused: */
  float tortu;
  int postot,negtot;
  float npratio;

  sumf = fvector(0,*nV);
  sumfy= fvector(0,*nV);

  for(k=0;k<*nV;k++){
        sumf[k]=0.0;
        sumfy[k]=0.0;
        }

  sum_X = 0.0;
  //sumf=0;  Jeff removing
  //sumfy=0;  Jeff removing

  /* Disabling long-term means.*/
  /*mean = *exmean;*/
  /*variance = *exvariance;*/
  /*locmN = *mN;*/
  mean = 0;
  variance = 0;
  locmN=0;

  /*Is it time to write velocity file?*/
#ifdef NOTUSING
  if(operate_flag){
    if(!(time%VELFILETIME))
      writevelocity = 1;
  }
#endif

  /* This code segment accumulates averaged velocities */
#ifdef NOTUSING
  if(operate_flag){
    if((time%VELFILETIME)>(VELFILETIME-VELAVERAGETIME)){
      for(i=0;i<*nV;i++){
	vortex[i].avgfx += vortex[i].fx;
	vortex[i].avgfy += vortex[i].fy;
      }
    }
  }
#endif


#ifdef NOTUSING
  if(writevelocity){
    numpart = *nV;
    fwrite(&numpart,sizeof(int),1,velocityfile);
    fwrite(&time,sizeof(int),1,velocityfile);
  }
#endif

  for (i=0; i < *nV; i++) {

    v = &vortex[i];
    x = v->x;
    fx = v->fx;
    fy = v->fy;

    /*Write velfile data if requested */
    /* Now writing TIME AVERAGED velocity */
#ifdef NOTUSING
    if(writevelocity){
      fwrite(&x,sizeof(float),1,velocityfile);
      y = v->y;
      fwrite(&y,sizeof(float),1,velocityfile);
      fwrite(&(v->avgfx),sizeof(float),1,velocityfile);
      fwrite(&(v->avgfy),sizeof(float),1,velocityfile);
      v->avgfx = 0;
      v->avgfy = 0;
    }
#endif
    
    /*Sum of forces on all particles, x and y directions.*/
    //if(10==i) sumf += fx;  Jeff removing
    //if(10==i) sumfy += fy;  Jeff removing

    sumf[i]=fx; //Jeff adding
    sumfy[i]=fy; //Jeff adding

    /*Sum of forces on particles inside sample*/
    /*done ALONG CERTAIN LINE*/
#ifdef NOTUSING
    if(ihave_8 || ihave_25){
      /*Made wider!*/
      if((x>=8.0)&&(x<13.0)){
	sum_X += fx;
      }
    }
#endif

    /*Vortex entry check:*/
    /*Disabling*/
    /*if((x>=5.0)&&(x<=8.0))*/
      /*if(v->oldx < 5.0){*/
	/*fprintf(enterfile,"%d %d %f\n",time,v->id,v->y);*/
    /*}*/

    /* Remove vortices which enter barrier region */ 
    /* Record time of exit in exitfile.*/
    /* Also record cumulative displacement of vortex.*/
    /*CIJOL NOTE VORTEX REMOVAL DISABLED 8.26.96*/
    /* Vortex removal reinstated 11.13.96; exitfile not reinstated.*/
    /*if(ihave_exit){*/

#ifdef NOTUSING
    /*Altering this for fully periodic sample*/
    /*if ( (x > LEFTBAR)&&(x<RIGHTBAR) ) { */
    /* CIJOL altering slightly 7.22.97 */
    if((x>=(SX-3.0))&&(x<=SX))
      if(v->oldx < (SX-3.0)){
	/* 7.5.97 Making rexitfile binary*/
	/* 7.19.97 DISABLING RBEXIT FILE, and replacing */
	/*         with a cumulative calculation of mean */
	/*         and variance.*/
	/*fprintf(rexitfile,"%d %d %f %f %f\n",time,v->id,v->y,
		v->cum_disp,v->cum_x);*/
	/*fwrite(&time,sizeof(int),1,rexitfile);*/
	/*fwrite(&(v->id),sizeof(int),1,rexitfile);*/
	/*fwrite(&(v->y),sizeof(float),1,rexitfile);*/
	/*fwrite(&(v->cum_disp),sizeof(float),1,rexitfile);*/
	/*fwrite(&(v->cum_x),sizeof(float),1,rexitfile);*/

	if(v->cum_x>3.0){
	  tortu = (v->cum_x)/(v->cum_disp);
	  variance = (locmN/(locmN+1.0))*variance + 
	             (locmN/((locmN+1.0)*(locmN+1.0)))
	             *(tortu-mean)*(tortu-mean);
	  mean = mean + (1.0/(locmN+1.0))*(tortu-mean);
	  locmN++;
	  
	  /*Vortices not removed, but accumulation variables reset*/
	  /*remove_a_vortex(i,vortex,nV);*/
	  /*(*nV)--;  */
	  v->cum_disp = 0;
	  v->cum_x = 0;
	}
      }
#endif

  }
  *exvariance = variance;
  *exmean = mean;
  *mN = locmN;

  if(operate_flag){
    /* CIJOL suppressing */
#ifdef NOTUSING
    /*New write to meanexit file*/
    if(!(time%WRITEEXITTIME)){
      /*Changing to binary 9.11.97 so that all files are binary */
      /*fprintf(rexitfile,"%d %f %f %d\n",time,mean,variance,locmN);*/
      fwrite(&time,sizeof(int),1,rexitfile);
      fwrite(&mean,sizeof(float),1,rexitfile);
      fwrite(&variance,sizeof(float),1,rexitfile);
      fwrite(&locmN,sizeof(int),1,rexitfile);
    }
#endif
    
    sum_X += *asumX;
    for(j=0;j<*nV;j++){
    sumf[j] += asumf[j];
    sumfy[j] += asumfy[j];
	}
 
    /* Decimation now set at compiletime. */
    if(!(time%DECIFACTOR)){
      /*sum_X /= FDECIFACTOR;*/
	postot=0;
	negtot=0;

      for(j=0;j<*nV;j++){
      sumf[j] /= FDECIFACTOR;
      sumfy[j] /= FDECIFACTOR;

	if((sumf[j])>0.0) postot++;
	if((sumf[j])<0.0) negtot++;
      
      /* CIJOL SPECIAL making ascii */
      /*fwrite(&sumf,sizeof(float),1,sumf_file);
	fwrite(&sumfy,sizeof(float),1,sumfy_file);*/
	if(time){  /*Pesky problem with fxtest if you print at t=0 */
	 fprintf(sumf_file,"%f ",sumf[j]); 
	 fprintf(sumfy_file,"%f ",sumfy[j]); 

	 fxmat[*linenow][j]=sumf[j];
	 fymat[*linenow][j]=sumfy[j];
	}
      
      /*if(ihave_8)
	fwrite(&sum_X,sizeof(float),1,sum_forces_file);*/
      
      *asumX = 0;
      asumf[j] = 0.0;
      asumfy[j] = 0.0;
     }
	if(time){ /* Again just excepting the print part for the very first */
        fprintf(sumf_file,"\n");
        fprintf(sumfy_file,"\n");
	*linenow=*linenow+1;
	}
	fprintf(instotfile,"%d %d \n",postot,negtot);
	npratio=(float)negtot/(float)postot;
	npstore[*timefrstore]=npratio;
	*timefrstore+=1;
    }
    else{
      *asumX = sum_X;
      for(j=0;j<*nV;j++){
      asumf[j] = sumf[j];
      asumfy[j] = sumfy[j];	
	}
    }
  }
  /* CIJOL New, special: */
  else{

    for(j=0;j<*nV;j++){
    sumf[j] += asumf[j];
    sumfy[j] += asumfy[j];
        }

    /* Decimation now set at compiletime. */
    if(!(time%DECIFACTOR)){
	for(j=0;j<*nV;j++){
      sumf[j] /= FDECIFACTOR;
      sumfy[j] /= FDECIFACTOR;

      /* CIJOL SPECIAL making ascii */
      /*fwrite(&sumf,sizeof(float),1,sumf_file);
        fwrite(&sumfy,sizeof(float),1,sumfy_file);*/
         fprintf(sumf_file,"%f ",sumf[j]);
         fprintf(sumfy_file,"%f ",sumfy[j]);

      /*if(ihave_8)
        fwrite(&sum_X,sizeof(float),1,sum_forces_file);*/

      *asumX = 0;
      asumf[j] = 0.0;
      asumfy[j] = 0.0;
     }
	fprintf(sumf_file,"\n");
	fprintf(sumfy_file,"\n");
    }
    else{
      *asumX = sum_X;
      for(j=0;j<*nV;j++){
      asumf[j] = sumf[j];
      asumfy[j] = sumfy[j];
      }
    }
  }   
  free_fvector(sumf,0,*nV);
  free_fvector(sumfy,0,*nV);
}
/*===================== remove_a_vortex =========================*/

void remove_a_vortex(numout,vortex,nV)
int numout;
struct vortex *vortex;
int *nV;
{
  int i;

  i=*nV;
  /*CIJOL NOTE: moved decrement of i to here BEFORE using i since*/
  /*the array vortex does NOT have a vortex at nV.*/
  i--;

  vortex[numout].x     = vortex[i].x;
  vortex[numout].y     = vortex[i].y;
  vortex[numout].fx    = vortex[i].fx;
  vortex[numout].fy    = vortex[i].fy;
  vortex[numout].oldx  = vortex[i].oldx;
  vortex[numout].oldy  = vortex[i].oldy;
  vortex[numout].color = vortex[i].color;
  vortex[numout].next  = vortex[i].next; 
  vortex[numout].id    = vortex[i].id; 
  vortex[numout].cum_disp = vortex[i].cum_disp;
  vortex[numout].cum_x = vortex[i].cum_x;
  vortex[numout].avgfx = vortex[i].avgfx;
  vortex[numout].avgfy = vortex[i].avgfy;
    
  *nV = i;
  update_list = 1;

}
/*=============================== moveVortices===============================*/

/* This code does one step. However, there is at this point (6-3-94) some */
/* confusion on what algorithm to move a particle. Hence the type of      */
/* update method must be specified.                                       */
/* Added sinuosity calculation 12.12.96 */

void moveVortices(vortex, nV, dt)
struct vortex *vortex;
int *nV;
float dt;
{

  int i;
  struct vortex *v;
  float oldx,oldy;
  float dx,dy;

  for (i = 0; i < *nV; i++) {
    v = &vortex[i];

    oldx = v->x;
    oldy = v->y;

    dx = (v->fx)*dt;
    dy = (v->fy)*dt;
    
    v->x += dx;
    v->y += dy;
    
    v->oldx = oldx;
    v->oldy = oldy;
    
    if (v->x >= (SX))
      v->x -= SX;
    if (v->x < 0.0)
      v->x += SX;
    
    if (v->y >= (SY))
      v->y -= SY;
    if (v->y < 0.0)
      v->y += SY;
    
    v->fx = 0.0;
    v->fy = 0.0;
    
    /*Sinuosity calculation*/
    /* Altering this for fully periodic sample*/
    /*if(v->x > 5.0){*/
    {
      v->cum_x += dx;
      v->cum_disp += (float)sqrt((double)(dx*dx+dy*dy));
    }
  }
}

/*=========================== adjustCells==================================*/

/* This maps the vortices to the cells. */

void adjustCells(vortex, nV) 
struct vortex *vortex;
int nV;
{
  int getCell();

  int i, cell;

  /* Clear out old info  */
  for (i = 0; i <= N_CELLS; i++) first[i][1] = last[i] = NULL;
		
  for (i = 0; i < nV; i++) {	/* Go to each vortex...  */
    cell = getCell(&vortex[i]);	/* Find its cell... */

    if (first[cell][1] == NULL) {
      first[cell][1] = last[cell] = &vortex[i];
      vortex[i].next = NULL;
    }
    else {
      last[cell]->next = &vortex[i];   /* Set last one to point to this one */
      vortex[i].next = NULL;	   /* This one is now last, so points to NULL */
      last[cell] = &vortex[i];	   /* Set pointer to last one to this one */
    }
	
  }
  
}
			
/*=============================== getCell===================================*/

/* Finds the cell the vortex is located in. */
/* Requires ORIGINAL system size, now called DSX */

int getCell(v) 
struct vortex *v;
{
  void nrerror();

  int hor, vert, cell;
  float x;

  x = v->x;

  /*Note this apparent kluge, from jmg 8.14.96:*/
  if(x>=DSX)
    x = DSX - 0.01;
	
  hor = x / (DSX/NCELLS_X);
  vert = (v->y) / (SY/NCELLS_Y);
  cell = hor + NCELLS_X * vert;
  if (cell < 0) cell = 0;
  if (cell >= N_CELLS) cell = N_CELLS-1;
  
  if ((cell >= 0) && (cell < N_CELLS))
    return cell;
  else
    nrerror("Illegal cell number in getCell!\n");

}

/*=============================== neighbor===================================*/

/* this routine is used to find the neighbors to the given cell. */
/* Each cell has 4 neighboring cells to look at (see Allen & Tisdley */
/* for details). This routine finds the neighbor numbered nn. */

int neighbor(cell, nn) 
int cell;
int nn;
{
  int n;
	
  switch (nn) {
     case 0:	n = cell - NCELLS_X + 1; break;
     case 1:	n = cell + 1; break;
     case 2:	n = cell + NCELLS_X + 1; break;
     case 3:	n = cell + NCELLS_X; break;
  }
	
  if (!((cell+1) % NCELLS_X))				/* In right column */
    if (nn != 3) n -= NCELLS_X;
  
  if (cell < NCELLS_X)					/* In bottom row */
    if (nn == 0) n += NULL_CELL;
			
  if (cell >= NCELLS_X*(NCELLS_Y-1))		/* In top row */
    if (nn > 1) n -= NULL_CELL;
  
  return n;
}


/*=============================== force=====================================*/

/* Finds the vortex-vortex force between a given pair of vortices. */

void force(p,v,fx,fy)
struct vortex *p,*v;
float *fx,*fy;
{
  void read_array();

  float f, dx, dy, r2;
  int i;
  static float r_min,r_max,scale;
  static int N_max = 0;

  if (!N_max) {
    read_array(&N_max,&r_min,&r_max);

    printf("N_max = %d\nR_min = %f\nR_max = %f\n",N_max,r_min,r_max);

    scale   = (float)N_max/(r_max - r_min); 
    potential_scale = scale;

  }
  
  dx = p->x - v->x;
  dy = p->y - v->y;

  if (dx>SYS_size2_x)  dx -= SX;
  if (dx<-SYS_size2_x) dx += SX;

  if (dy>SYS_size2_y)  dy -= SY;
  if (dy<-SYS_size2_y) dy += SY;

  r2 = dx*dx + dy*dy;

  if (r2 > r_max) {
    *fx = 0.0;
    *fy = 0.0;
    return;
  }
 
  if (r2 < r_min) {
    f = forces[0]; 
  }
  else {
    r2 -= r_min;
    i = (int)(r2*scale);
    f = forces[i];
  }
 
  *fx = dx*f;
  *fy = dy*f; 

}

/*=============================== drawIt=====================================*/

/* Output the graphics. */

void drawIt(vortex,nV,time)
struct vortex *vortex;
int nV;
int time;
{
  int i;

  fwrite(&nV,sizeof (int),1,kmovie);
  fwrite(&time,sizeof (int),1,kmovie); /*Added 3.10.95*/
  /* fprintf(kmovie,"%d %d\n",nV,time); */

  for (i = 0; i < nV; i++) {
    fwrite(&(vortex[i].id),sizeof (int),1,kmovie);
    fwrite(&(vortex[i].x),sizeof (float),1,kmovie);
    fwrite(&(vortex[i].y),sizeof (float),1,kmovie);
    /* fwrite(&(vortex[i].cum_disp),sizeof (float),1,kmovie);
    fwrite(&(vortex[i].cum_x),sizeof (float),1,kmovie);*/
    /* fprintf(kmovie,"%d %f %f\n",vortex[i].id,vortex[i].x,vortex[i].y); */
  }
}

/*================================ nrerror===================================*/
void nrerror(error_text)
char error_text[];
{
  char string[200],temp[30];

  strcpy(string,error_text);
  strcat(string," ");
  sprintf(temp,"%d",myid);
  strcat(string,temp);
  fprintf(stderr,"Numerical Recipes run-time error...\n");
  fprintf(stderr,"%s\n",string);
  fprintf(stderr,"...now exiting to system...\n");
  exit(1);
}

/*========================== Rand()=================================*/
/* Return a random floating point number between [0.0,1) */
#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define NTAB 32
#define NDIV (1+(IM-1)/NTAB)
#define EPS 1.2e-7
#define RNMX (1.0-EPS)

float Rand()
/* From Numerical Recipies p 280 */
/* "Minimal" random nomber generator of Park and Miller with Bays-Durham */
/* shuffle and added safeguards.  Returns a uniform random deviate between */
/* 0.0 and 1.0 (exclusive of the endpoint values). idum is set to a */
/* negative integer to initialize; thereafter, idum should not be altered */
/* between successive deviates in a sequence.  RNMX should approximate */
/* the largest floating value that is less than 1. */
{
  int j;
  long k;
  static long iy=0;
  static long iv[NTAB];
  float temp;
  static long idum = -SEED;
  static int very_first_time = 1;

  if (idum <=0 || !iy){  /* Initialize */
    if (-(idum)<1) idum = 1;   /* Be sure to prevent idum = 0 */
    else{
      if(very_first_time){
#ifdef TODOSIG
	idum=SEED+PARATODOS*13771;
#else
	idum=SEED+myid*13771;
#endif
	very_first_time=0;
      }
      else idum = -idum;
    }
    for (j=NTAB+7;j>=0;j--){  /* Load the shuffle table after 8 warm-ups */
      k=idum/IQ;
      idum=IA*(idum-k*IQ)-IR*k;
      if (idum<0) idum += IM;
      if (j < NTAB) iv[j] = idum;
    }
    iy = iv[0];
    idum = SEED;
  }
  k=idum/IQ;  /* Start here when not initializing */
  idum=IA*(idum-k*IQ)-IR*k;  /*Compute idum=(IA*idum)%IM without overflows */
  if (idum<0) idum +=IM;     /*by Schrage's method.                        */
  j=iy/NDIV;    /* Will be in the range 0..NTAB-1.*/
  iy = iv[j];  /*Output previously stored value and refill the shuffle table */
  iv[j] = idum;
  if ((temp=AM*iy) > RNMX) return RNMX; /* Because users don't expect */
  else return temp;                     /* endpoint values.           */
}
/*=======================  gasdev ==============================*/
/* Returns a normally distributed deviate with zero mean and unit */
/* variance, using Rand() as the source of uniform deviates.*/
/* From Numerical Recipies, p. 289 */
float gasdev()
{
  float Rand();
  static int iset=0;
  static float gset;
  float fac,rsq,v1,v2;
 
  if(iset==0){      /*We don't have an extra deviate handy, so */
    do{
      v1=2.0*Rand()-1.0;    /* Pick 2 uniform numbers in the square extending*/
      v2=2.0*Rand()-1.0;    /* from -1 to +1 in each direction, */
      rsq=v1*v1+v2*v2;      /* see if they are in the unit circle, */
    } while (rsq >= 1.0 || rsq == 0.0);  /* and if they aren't, try again.*/
    fac=sqrt(-2.0*log(rsq)/rsq);
    /* Now make the Box-Muller transformation to get two normal */
    /* deviates.  Return one and save the other for next time.*/
    gset=v1*fac;
    iset=1;  /*Set flag*/
    return v2*fac;
  }
  else{  /*We have an extra deviate handy, */
    iset=0;   /*so unset the flag,*/
    return gset;  /*and return it.*/
  }
}

/*===========================calc_parabolic_well==================*/
void calc_parabolic_well(x_pin,y_pin,x,y,Ap,r_pin,Energy,F_x,F_y)
     float x_pin,y_pin,x,y;
     float Ap,r_pin;
     float *Energy,*F_x,*F_y;
{
  float dist2,dist_x,dist_y;
  float V_factor,pot_depth;
  pot_depth = (Ap/2.0)*r_pin;
  V_factor = Ap/(2.0*r_pin);
  
  dist_x = x - x_pin;
  dist_y = y - y_pin;

  if (dist_x>SYS_size2_x) dist_x -= SYS_size_x;
  if (dist_x<-SYS_size2_x) dist_x += SYS_size_x;

  if (dist_y>SYS_size2_y) dist_y -= SYS_size_y;
  if (dist_y<-SYS_size2_y) dist_y += SYS_size_y;

  dist2 = (dist_x*dist_x + dist_y*dist_y);

  if (dist2 > r_pin*r_pin) {
    *Energy = 0.0;
    *F_x = 0.0;
    *F_y = 0.0;
  }
  else{
    *Energy = V_factor*dist2 - pot_depth;
    *F_x = -2*V_factor*dist_x;
    *F_y = -2*V_factor*dist_y;
  }
}

/* ============================do_overlap===========================*/

int do_overlap(x,y,range,first)
     float x,y,range;
     struct pinning_site **first;
{
  int i,j,x_i,y_j;
  int cells_x,cells_y;
  int cell_x_num,cell_y_num,cell_num;
  float cell_x_scale,cell_y_scale;
  float dx,dy,dist2;
  struct pinning_site *cp;
  
  static int curr_pin_num = 1;

  cells_x = (int)(SYS_size_x/range)+1;
  cells_y = (int)(SYS_size_y/range)+1;
  
  cell_x_scale = cells_x/SYS_size_x;
  cell_y_scale = cells_y/SYS_size_y;

  cell_x_num = (int)(x*cell_x_scale);
  cell_y_num = (int)(y*cell_y_scale);

  for(j=cell_y_num-1; j<=cell_y_num+1; j++){
    for(i=cell_x_num-1; i<=cell_x_num+1; i++){
      
      x_i = i;
      y_j = j;
      
      if (x_i >= cells_x) x_i -= cells_x;
      if (x_i < 0)  x_i += cells_x;
      
      if (y_j >= cells_y) y_j -= cells_y;
      if (y_j < 0) y_j += cells_y;

      cp = *(first+y_j*cells_y+x_i);

      while (cp != NULL){
	dx = cp->x - x;
	dy = cp->y - y;

	if (dx>SYS_size2_x) dx -= SYS_size_x;
	if (dx<-SYS_size2_x) dx += SYS_size_x;

	if (dy>SYS_size2_y) dy -= SYS_size_y;
	if (dy <-SYS_size2_y) dy += SYS_size_y;

	dist2 = (dx*dx + dy*dy);

	if (dist2 < range*range) {
	  return 1;
	}

	cp = cp->next;
      }
      
    }
  }

  cell_num = cell_x_num + cell_y_num*cells_y;
  
  if (first[cell_num] == NULL){
    first[cell_num] = &(pin_sites[curr_pin_num]);
  }
  else {
    pin_sites[curr_pin_num].next = first[cell_num];
    first[cell_num] = &(pin_sites[curr_pin_num]);
  }
  
  pin_sites[curr_pin_num].x = x;
  pin_sites[curr_pin_num].y = y;
  pin_sites[curr_pin_num].id = curr_pin_num;
  curr_pin_num++;

  if (!(curr_pin_num%1000)) printf("Number of pins so far:  %d\n",curr_pin_num);
  
  return 0;
}

int check_overlap(x2,y2,range,num_pin)
float x2,y2,range;
int num_pin;
{
  int l,overlap;
  float dist2,dist_x,dist_y;
  static struct vector *pin_pos;
  static int current_pin_num = 0;

  if (!current_pin_num) {
    pin_pos = (struct vector *)malloc(sizeof(struct vector)*num_pin);
  }

  overlap = 0;

  for(l=0; l<current_pin_num; l++) {
    dist_x = pin_pos[l].x - x2;
    dist_y = pin_pos[l].y - y2;

    if (dist_x>SYS_size2_x)  dist_x -= SYS_size_x;
    if (dist_x<-SYS_size2_x) dist_x += SYS_size_x;

    if (dist_y>SYS_size2_y)  dist_y -= SYS_size_y;
    if (dist_y<-SYS_size2_y) dist_y += SYS_size_y;

    dist2  = (dist_x*dist_x + dist_y*dist_y);

    if (dist2 < range) {
      overlap = 1;
      break;
    }
  }

  if (!overlap) {
    pin_pos[current_pin_num].x = x2;
    pin_pos[current_pin_num].y = y2;
    current_pin_num++;
  }

  if (current_pin_num == num_pin) {
    free(pin_pos);
  }

  return overlap;
}

/*==============================make_pinning_site======================*/

void make_pinning_site(num_pin,r_pin)
     int num_pin;
     float r_pin;
{
  void nrerror();

  int i,k;
  int cells_x,cells_y,cells_total;
  int overlap1;
  float range;
  float x,y,size;
  struct pinning_site **first;
  struct pinning_site **last;

  size = MIN(SYS_size_x,SYS_size_y);
  
  range = 2.0*r_pin + size/N_POT;

  cells_x = (int)(SYS_size_x/range);
  cells_y = (int)(SYS_size_y/range);
  
  cells_total = cells_x * cells_y;
  
  first = (struct pinning_site **)(malloc((sizeof(struct pinning_site *))*cells_total));
  last = (struct pinning_site **)(malloc((sizeof(struct pinning_site *))*cells_total));
  if (first == NULL) nrerror("Not enough memory\n");
  if (last == NULL) nrerror("Not enough memory\n");

  for(i=0; i<cells_total; i++){
    first[i] = NULL;
    last[i] = NULL;
  }
  
  for(k = 0; k<num_pin; k++){
    pin_sites[k].next = NULL;
  }
  
  for(k=0; k<num_pin; k++){
    do{
      x = (SYS_left_edge+r_pin+.01) + (SYS_right_edge-SYS_left_edge -2*r_pin-.01) * Rand();
      y = (SYS_size_y - 4*r_pin)*Rand() +2*r_pin;
      overlap1 = do_overlap(x,y,range,first);
      
    }while (overlap1);
  }
}

/*============================== add_pinning (old) ==========================*/
/* Routine to add pinning potentials.  Altered to add potentials of a */
/* particular size range, distributed randomly */

int add_pinning(num_add,Ap,r_pin_in,left,right,num_so_far,id_string)
int num_add;        /* Number of pinning sites to add */
float r_pin_in;        /* Maximum radius of the added pinning sites */
float Ap;           /* Strength of the pinning sites */
float left;         /* The leftmost a site can be placed. */
float right;        /* The rightmost a site can be places */
int num_so_far;     /* The number of pinning sites that have been. */
char id_string[];
{
  void write_special_contour();
  float gasdev();

  int i,j,k,l,r_pin_cell_x,r_pin_cell_y,num_pin;
  int i_start,j_start,x_i,y_j,overlap;  
  float r_pin2,r_pin_check,V_factor,factor,r_pin;
  float x2,y2,pot_depth;
  float size_x,size_y;
  float dist2,dist_x,dist_y;
  float x,y,size2_x,size2_y;

  num_pin =  num_so_far;
    
  size_x = (float)SX;
  size_y = (float)SY;

  size2_x = size_x/2.0;
  size2_y = size_y/2.0;

  r_pin = r_pin_in;
  
  pot_depth = (Ap/2.0)*r_pin;
  V_factor  = Ap/(2.0*r_pin);
  
  r_pin_cell_x = (int)((r_pin/size_x)*N_POT);
  r_pin_cell_y = (int)((r_pin/size_y)*N_POT);
  
  r_pin2 = r_pin*r_pin;
  
  r_pin_check  = 4.01*r_pin2;
  r_pin_check += 5.0/N_POT;

  for(k=num_so_far+1; k<(num_so_far+num_add); k++) {

    /* DISTRIBUTIONS - Alter these lines. */
    /* This line determines pinning strength distribution.*/
    /* For uniformly distributed strengths in the range (0.2 fp, 1.0 fp): */
    /*factor = 1-(.80)*Rand(); */
    /* To set all pins to strength fp: */
    factor = 1;
    /* For either of the above, use these lines: */
    pin_sites[k].Ap     = -2.0*factor*V_factor;
    pin_sites[k].depth  = factor*pot_depth;

    /* For normally distributed strengths.  Mean = fp, std dev = 0.1, */
    /* use all of these lines: */
    /* Note, to change the std dev, change the multiplier of gasdev.*/
    /*factor = 0.1*gasdev();*/
    /*pin_sites[k].Ap = -2.0*V_factor + factor;*/
    /*pin_sites[k].depth = pot_depth + factor;*/

    pin_sites[k].radius = r_pin;

    j = 0;

    do {
      j++;
      overlap = 0;
      x2 =  left + ((right-left-r_pin)*Rand()); 
      y2 =  (size_y )*Rand();
      
      for(l=1; l<k; l++) {
	dist_x = pin_sites[l].x - x2;
	dist_y = pin_sites[l].y - y2;

	if (dist_x>size2_x)  dist_x -= size_x;
	if (dist_x<-size2_x) dist_x += size_x;
  
	if (dist_y>size2_y)  dist_y -= size_y;
	if (dist_y<-size2_y) dist_y += size_y;
    
	dist2  = (dist_x*dist_x + dist_y*dist_y);

	r_pin_check  = (pin_sites[l].radius + r_pin);
	r_pin_check *= r_pin_check;
	r_pin_check += 3.0/N_POT;

	if (dist2 < r_pin_check) {
	  overlap = 1;
	  break;
	}

      }

    } while (overlap&&(j<(num_so_far+num_add)));


    if (j== (num_so_far+num_add) ) {
      num_so_far = k;
      break; 
    }

    num_pin++;

    pin_sites[k].x = x2;
    pin_sites[k].y = y2;

    i_start = (int)((x2/size_x)*N_POT);
    j_start = (int)((y2/size_y)*N_POT);

    for(j=j_start-r_pin_cell_y; j<j_start+r_pin_cell_y; j++) {
      for(i=i_start-r_pin_cell_x; i<i_start+r_pin_cell_x; i++) {

	x_i = i;
	y_j = j;

	if (x_i < 0) 
	  x_i += N_POT;
	if (x_i >= N_POT)
	  x_i -= N_POT;

	if (y_j < 0) 
	  y_j += N_POT;
	if (y_j >= N_POT)
	  y_j -= N_POT;


	x = x_i*(size_x/N_POT);
	y = y_j*(size_y/N_POT);

	dist_x = x - x2;
	dist_y = y - y2;
    
	if (dist_x>size2_x)  dist_x -= size_x;
	if (dist_x<-size2_x) dist_x += size_x;
  
  
	if (dist_y>size2_y)  dist_y -= size_y;
	if (dist_y<-size2_y) dist_y += size_y;
    
	dist2 = (dist_x*dist_x + dist_y*dist_y);

    
	if (dist2 > (r_pin2) ) continue;
    
	lookup_par[y_j][x_i] = k;
    
      }   
    }

  }
  /*Special modification 7.6.95 CIJOL NOTE*/
  write_special_contour(num_pin,id_string);
  return(num_pin);
}

/*================================ add_pinning_channel ======================*/
/* Routine to add pinning potentials.  Altered to add potentials of a */
/* particular size range, distributed randomly */
/* Now adds pinning randomly everywhere except in a channel of specified*/
/* width.  Places pinning site 0 at the head of this channel, right in the */
/* middle.*/

int add_pinning_channel(num_add,Ap,r_pin_in,left,right,num_so_far,chan_width,
			id_string)
int num_add;        /* Number of pinning sites to add */
float r_pin_in;        /* Maximum radius of the added pinning sites */
float Ap;           /* Strength of the pinning sites */
float left;         /* The leftmost a site can be placed. */
float right;        /* The rightmost a site can be placed */
int num_so_far;     /* The number of pinning sites that have been. */
float chan_width;  /*Width of channel*/
char id_string[];
{
  void write_special_contour();

  int i,j,k,l,r_pin_cell_x,r_pin_cell_y,num_pin;
  int i_start,j_start,x_i,y_j,overlap;  
  float r_pin2,r_pin_check,V_factor,factor,r_pin;
  float x2,y2,pot_depth;
  float size_x,size_y;
  float dist2,dist_x,dist_y;
  float x,y,size2_x,size2_y;
  float top_chan_edge,bot_chan_edge;

  num_pin =  num_so_far;
    
  size_x = (float)SX;
  size_y = (float)SY;

  size2_x = size_x/2.0;
  size2_y = size_y/2.0;

  r_pin = r_pin_in;
  
  pot_depth = (Ap/2.0)*r_pin;
  V_factor  = Ap/(2.0*r_pin);
  
  r_pin_cell_x = (int)((r_pin/size_x)*N_POT);
  r_pin_cell_y = (int)((r_pin/size_y)*N_POT);
  
  r_pin2 = r_pin*r_pin;
  
  r_pin_check  = 4.01*r_pin2;
  r_pin_check += 5.0/N_POT;
  
  top_chan_edge = size2_y + chan_width/2.0;
  bot_chan_edge = size2_y - chan_width/2.0;

  for(k=num_so_far+1; k<(num_so_far+num_add); k++) {

    /*This line causes pinning sites to not all have same strength*/
    /*factor = 1-(.80)*Rand(); */
    factor = 1;

    pin_sites[k].Ap     = -2.0*factor*V_factor;
    pin_sites[k].depth  = factor*pot_depth;
    pin_sites[k].radius = r_pin;

    j = 0;

    do {
      j++;
      overlap = 0;
      if((k>0) && (k<4)){
	/*Add trio of pinning sites at channel mouth*/
	x2 = left+r_pin;
	if(k==1)
	  y2 = size2_y;
	else if(k==2)
	  y2 = size2_y - 3*r_pin;
	else 
	  y2 = size2_y + 3*r_pin;
      }
      else{
	x2 =  left + ((right-left-r_pin)*Rand()); 
	y2 =  (size_y )*Rand();
      }
      
      for(l=1; l<k; l++) {

	if((y2<top_chan_edge)&&(y2>bot_chan_edge)&&(k>3)){
	  overlap = 1;
	  break;
	}

	dist_x = pin_sites[l].x - x2;
	dist_y = pin_sites[l].y - y2;

	if (dist_x>size2_x)  dist_x -= size_x;
	if (dist_x<-size2_x) dist_x += size_x;
  
	if (dist_y>size2_y)  dist_y -= size_y;
	if (dist_y<-size2_y) dist_y += size_y;
    
	dist2  = (dist_x*dist_x + dist_y*dist_y);

	r_pin_check  = (pin_sites[l].radius + r_pin);
	r_pin_check *= r_pin_check;
	r_pin_check += 3.0/N_POT;

	if (dist2 < r_pin_check) {
	  overlap = 1;
	  break;
	}

      }

    } while (overlap&&(j<(num_so_far+num_add)));


    if (j== (num_so_far+num_add) ) {
      num_so_far = k;
      break; 
    }

    num_pin++;

    pin_sites[k].x = x2;
    pin_sites[k].y = y2;

    i_start = (int)((x2/size_x)*N_POT);
    j_start = (int)((y2/size_y)*N_POT);

    for(j=j_start-r_pin_cell_y; j<j_start+r_pin_cell_y; j++) {
      for(i=i_start-r_pin_cell_x; i<i_start+r_pin_cell_x; i++) {

	x_i = i;
	y_j = j;

	if (x_i < 0) 
	  x_i += N_POT;
	if (x_i >= N_POT)
	  x_i -= N_POT;

	if (y_j < 0) 
	  y_j += N_POT;
	if (y_j >= N_POT)
	  y_j -= N_POT;


	x = x_i*(size_x/N_POT);
	y = y_j*(size_y/N_POT);

	dist_x = x - x2;
	dist_y = y - y2;
    
	if (dist_x>size2_x)  dist_x -= size_x;
	if (dist_x<-size2_x) dist_x += size_x;
  
  
	if (dist_y>size2_y)  dist_y -= size_y;
	if (dist_y<-size2_y) dist_y += size_y;
    
	dist2 = (dist_x*dist_x + dist_y*dist_y);

    
	if (dist2 > (r_pin2) ) continue;
    
	lookup_par[y_j][x_i] = k;
    
      }   
    }

  }
  /*Special modification 7.6.95 CIJOL NOTE*/
  write_special_contour(num_pin,id_string);
  return(num_pin);
}


/*===================== add_pinning_hexagonal =====================*/
int add_pinning_hexagonal(num_pin,Ap,r_pin,order_param,id_string)
int num_pin;
float Ap;
float r_pin;
float order_param;
char id_string[];
{
  void write_special_contour();

  int i,j,k,l,r_pin_cell_x,r_pin_cell_y;
  int i_start,j_start,x_i,y_j,overlap;
  float x,y,a;
  float V,F_x,F_y;
  float r_pin2,x2,y2,r_pin_check,size;
  int row,col,curr_num_pin;
  int total_num_pin;

  size = MIN(SYS_size_x,SYS_size_y);

  r_pin_cell_x = (int)((r_pin/SYS_size_x)*N_POT) + 1;
  r_pin_cell_y = (int)((r_pin/SYS_size_y)*N_POT) + 1;
  r_pin2       = r_pin*r_pin;
  r_pin_check  = 4.0*r_pin2 + 2.0*(size/N_POT);

  curr_num_pin = 0;

  /* Here we need to lay down a triangular lattice that MUST */
  /* NOT leave any gaps between the bottom and top of the    */
  /* sample. Our strategy is to guess a lattice constant     */
  /* that is close to what would fit in the requested number */
  /* of wells. From that we get an integer # of rows.        */
  /* Now, we get a lattice constant from the rows such that  */
  /* the entire sample is covered. Then we find the # of     */
  /* columns for this new lattice constant.                  */

  a = ((RIGHTSX - LEFTSX ) * (SY) )/(num_pin * SQRT3_2);

  a = (float)sqrt((double)(a));

  if (num_pin == 750)
    row = (int)( (SY)/(SQRT3_2*a))+1;
  else
    row = (int)( (SY)/(SQRT3_2*a));

  /* Now we make sure that we have an even # of rows   */
  /* If there are an odd number of rows, then it and   */
  /* the bottom row will form a square lattice.        */

  if ((row%2)) row--;

  a = SY/(SQRT3_2*row);

  if (num_pin == 750)
    col = (int)((RIGHTSX - LEFTSX)/a);
  else
    col = (int)((RIGHTSX - LEFTSX)/a)+1;
  order_param *= .5*a;

  /*
  printf("ideal row number:  %f\n",(SYS_right_edge - LEFTSX - r_pin)/a);
  printf("ideal colum: %f\n", (SY-2*r_pin)/(SQRT3_2*a));
  printf(" how close to interger row is to 36:  %f\n",((float)row)/36);
  */

  total_num_pin = row*col;

  printf("\n\n\nNUMBER OF SITES PUT DOWN:  %d\n\n\n\n",(row*col));

  for(k=1; k<=row; k++) {
    for(l=1; l<=col; l++) {
      do {
        x2 = LEFTSX + (l-1)*a + 2*order_param*(.5 - Rand());
        y2 = (k-1)*SQRT3_2*a + 2*order_param*(.5 - Rand());
        if (!(k%2)) x2 += .5*a;
        overlap = check_overlap(x2,y2,r_pin_check,num_pin);
      } while (overlap);

      curr_num_pin++;

      pin_sites[curr_num_pin].x  = x2;
      pin_sites[curr_num_pin].y  = y2;
      pin_sites[curr_num_pin].Ap = -Ap/r_pin;
      pin_sites[curr_num_pin].depth = (Ap/2.0)*r_pin;
      pin_sites[curr_num_pin].radius = r_pin;

      i_start = (int)((x2/SYS_size_x)*N_POT);
      j_start = (int)((y2/SYS_size_y)*N_POT);

     for(j=j_start-r_pin_cell_y; j<j_start+r_pin_cell_y; j++) {
        for(i=i_start-r_pin_cell_x; i<i_start+r_pin_cell_x; i++) {
          x_i = i;
          y_j = j;

          if (x_i < 0)
            x_i += N_POT;
          if (x_i >= N_POT)
	    x_i -= N_POT;

          if (y_j < 0)
            y_j += N_POT;
          if (y_j >= N_POT)
            y_j -= N_POT;

          x = x_i*(SYS_size_x/N_POT);
          y = y_j*(SYS_size_y/N_POT);

	  calc_parabolic_well(x2,y2,x,y,Ap,r_pin,&V,&F_x,&F_y);

          lookup_par[y_j][x_i] = curr_num_pin;
        }
      }
    }
  }
  write_special_contour(total_num_pin,id_string);
  return(total_num_pin);
}

/*=================== add_pinning_cluster ===========================*/
/* Code adapted from add_pinning places non-overlapping pins in */
/* randomly spaced clusters.  Clusters are established by choosing*/
/* a random location for a seed vortex, then choosing radii and */
/* angles from this site and checking for overlap, placing a */
/* certain number (such as 10) of vortices around each seed.*/

int add_pinning_cluster(num_pin,Ap,r_pin,id_string)
int num_pin;
float Ap;
float r_pin;
char id_string[];
{
  void write_special_contour();
 
  int i,j,k,r_pin_cell_x,r_pin_cell_y;
  int i_start,j_start,x_i,y_j;
  int overlap;
  float x,y,r_pin2,r_pin_check,size;
  float V,F_x,F_y;
  float x2,y2;
  float xseed,yseed;
  float theta,radius;
  int pick_seed, cluster_sites, tries;

  /*Set up some constants we'll need:*/
  r_pin_cell_x = (int)((r_pin/SYS_size_x)*N_POT) + 1;
  r_pin_cell_y = (int)((r_pin/SYS_size_y)*N_POT) + 1;

  if (r_pin_cell_x < 1) r_pin_cell_x = 1;
  if (r_pin_cell_y < 1) r_pin_cell_y = 1;

  r_pin2 = r_pin*r_pin;

  size = MIN(SYS_size_x,SYS_size_y);

  r_pin_check  = 4.0*r_pin2 + 2.0*(size/N_POT);

  /*Start putting down pinning sites*/
  k = 0;
  pick_seed = 1;
  cluster_sites = 0;
  while(k<num_pin){

    /*Select a seed pinning site*/
    if(pick_seed){
      do {
        x2 = LEFTSX + (RIGHTSX-LEFTSX)*Rand();
        y2 = SY*Rand();
        overlap = check_overlap(x2,y2,r_pin_check,num_pin);
      } while (overlap);
      pick_seed = 0;
      xseed = x2;
      yseed = y2;
    }
    else{
      /*Select twenty clustered sites from this seed*/
      tries = 0;
      do{
        tries++;
        /*ALTER THE FOLLOWING LINE TO CHANGE DISTRIBUTION:*/
        radius = ( Rand()*Rand())*(2.80);
        theta = Rand()*360.0;
        x2 = xseed + radius*cos(theta);
        y2 = yseed + radius*sin(theta);
        overlap = check_overlap(x2,y2,r_pin_check,num_pin);
      } while(overlap && (tries < 250));
      if(overlap){
	pick_seed = 1;
	continue;
      }
      else
        cluster_sites++;
      if(cluster_sites >= 20){
        pick_seed = 1;
	cluster_sites = 0;   
      }
    }

    /*Place selected pinning site into data structure*/
    pin_sites[k+1].Ap = -Ap/r_pin;
    pin_sites[k+1].x  = x2 ;
    pin_sites[k+1].y  = y2;
    pin_sites[k+1].depth = (Ap/2.0)*r_pin;
    pin_sites[k+1].radius = r_pin;

    i_start = (int)((x2/SYS_size_x)*N_POT);
    j_start = (int)((y2/SYS_size_y)*N_POT);
 
    for(j=j_start-r_pin_cell_y; j<j_start+r_pin_cell_y; j++) {
      for(i=i_start-r_pin_cell_x; i<i_start+r_pin_cell_x; i++) {
        x_i = i;
        y_j = j;

        if (x_i < 0)
          x_i += N_POT;
        if (x_i >= N_POT)
          x_i -= N_POT;

        if (y_j < 0)
          y_j += N_POT;
        if (y_j >= N_POT)
          y_j -= N_POT;

        x = x_i*(SYS_size_x/N_POT);
	y = y_j*(SYS_size_y/N_POT);

	calc_parabolic_well(x2,y2,x,y,Ap,r_pin,&V,&F_x,&F_y);

        lookup_par[y_j][x_i] = k+1;
      }
    }
    k++;
  }
  write_special_contour(k,id_string);
  return(k);
}

/*=================== add_pinning_square ===============================*/
/* Puts pinning sites down in square array.  System size not altered */
/* in this implementation. */
void add_pinning_square(num_pin,Ap,r_pin,order_param,id_string)
int *num_pin;
float Ap;
float r_pin;
float order_param;
char id_string[];
{
  void write_special_contour();
  
  int i,j,k,l,r_pin_cell_x,r_pin_cell_y;
  int i_start,j_start,x_i,y_j,overlap;
  float x,y,a;
  float V,F_x,F_y;
  float r_pin2,x2,y2,r_pin_check,size;
  int row,col,curr_num_pin;

  size = MIN(SYS_size_x,SYS_size_y);

  r_pin_cell_x = (int)((r_pin/SYS_size_x)*N_POT) +1;
  r_pin_cell_y = (int)((r_pin/SYS_size_y)*N_POT) +1;
  r_pin2 = r_pin*r_pin;
  r_pin_check = 4.0*r_pin2 + 2.0*(size/N_POT);

  curr_num_pin = 0;
  
  a = ((RIGHTSX - LEFTSX) * (SY))/(*num_pin);

  a = (float)sqrt((double)(a));

  row = (int)(SY/a) + 1;
  a = SY/row;

  if (row*a<(SY-0.001)){
    printf("Problem with rows not fitting\n");
    exit(-1);
  }
  col = (int)((RIGHTSX-LEFTSX)/a);
  if(col*a>(SX+0.001)) col--;
  else if (col*a<(SX-0.001)) col++;
  if(col*a<(SX-0.001)){
    printf("Problem with cols not fitting\n");
    exit(-1);
  }

  order_param *= .5*a;

  *num_pin = row*col;
  printf("\nNUMBER OF SITES PUT DOWN: %d\n\n",(row*col));
  printf("The lattice constant is: %f\n",a);

  for(k=1;k<=row;k++){
    for(l=1;l<=col;l++){
      x2 = LEFTSX + (l-1)*a + 2*order_param*(.5-Rand()) + 0.5*a;
      y2 = (k-1)*a + 2*order_param*(.5-Rand()) + 0.5*a;
      overlap = check_overlap(x2,y2,r_pin_check,*num_pin);
      if(overlap){
	printf("Pin overlap; compensating\n");
	if(order_param < 0.001){
	  if (k==row) row--;
	  if (l==col) col--;
	  continue;
	}
      }

      curr_num_pin++;

      pin_sites[curr_num_pin].Ap = -Ap/r_pin;
      pin_sites[curr_num_pin].x = x2;
      pin_sites[curr_num_pin].y = y2;
      pin_sites[curr_num_pin].depth = (Ap/2.0)*r_pin;
      pin_sites[curr_num_pin].radius = r_pin;

      i_start = (int)((x2/SYS_size_x)*N_POT);
      j_start = (int)((y2/SYS_size_y)*N_POT);

      for(j=j_start-r_pin_cell_y; j<j_start+r_pin_cell_y; j++){
	for(i=i_start-r_pin_cell_x; i<i_start+r_pin_cell_x; i++){
	  x_i = i;
	  y_j = j;

	  if(x_i<0)
	    x_i += N_POT;
	  if(x_i >=N_POT)
	    x_i -= N_POT;
	  
	  if(y_j<0)
	    y_j += N_POT;
	  if(y_j>=N_POT)
	    y_j -= N_POT;

	  x = x_i*(SYS_size_x/N_POT);
	  y = y_j*(SYS_size_y/N_POT);
	  
	  calc_parabolic_well(x2,y2,x,y,Ap,r_pin,&V,&F_x,&F_y);

	  lookup_par[y_j][x_i] = curr_num_pin;
	}
      }
    }
  }
  write_special_contour(*num_pin,id_string);
}

/* ================== add_pinning (fast,broken)=============================*/
/* Routine to add pinning potentials.  Altered to add potentials of a */
/* particular size range, distributed randomly */
/* This version runs much more quickly than add_pinning_old.*/

void add_pinning_broken(num_pin,Ap,r_pin,pin_type)
     int num_pin;
     float Ap;
     float r_pin;
     int pin_type;
{
  int i,j,k,r_pin_cell_x,r_pin_cell_y;
  int i_start,j_start,x_i,y_j;
  float x,y;
  float V,F_x,F_y;
  float x2,y2;

  r_pin_cell_x = (int)((r_pin/SYS_size_x)*N_POT) + 1;
  r_pin_cell_y = (int)((r_pin/SYS_size_y)*N_POT) + 1;

  if (r_pin_cell_x < 1) r_pin_cell_x = 1;
  if (r_pin_cell_y < 1) r_pin_cell_y = 1;

  make_pinning_site(num_pin,r_pin);

  for (k=0;k<num_pin;k++){
  
    pin_sites[k+1].Ap = Ap;
    x2 = pin_sites[k+1].x;
    y2 = pin_sites[k+1].y; /*Note:pin_sites x,y are updated in check_overlap*/

    i_start = (int)((x2/SYS_size_x)*N_POT);
    j_start = (int)((y2/SYS_size_y)*N_POT);
    
    for(j=j_start-r_pin_cell_y; j<j_start+r_pin_cell_y; j++){
      for(i=i_start-r_pin_cell_x; i<i_start+r_pin_cell_x; i++){
	x_i = i;
	y_j = j;

	if (x_i < 0)
	  x_i += N_POT;
	if (x_i >= N_POT)
	  x_i -= N_POT;
	
	if (y_j < 0)
	  y_j += N_POT;
	if (y_j >= N_POT)
	  y_j -= N_POT;
	
	x = x_i*(SYS_size_x/N_POT);
	y = y_j*(SYS_size_y/N_POT);
	
	switch(pin_type){
	  
	case PIN_PARABOLIC:
	  calc_parabolic_well(x2,y2,x,y,Ap,r_pin,&V,&F_x,&F_y);
	  break;

	}

	lookup_par[y_j][x_i] = k+1;
      }
    }
  }
}

/*==================== add_vortex_in_pins ===========================*/
/* Puts down vortices IN pinning sites.  Then allows addition of a */
/* specified number of vortices above the number of pinning sites, or */
/* allows removal of some of the vortices.  Intended for use with */
/* periodic pinning, but other uses are not excluded. */
void add_vortex_in_pins(vortex,nV,num_pins)
struct vortex *vortex;
int *nV;
int num_pins;
{
  void insert_vortex();

  int k,count;
  float x,y;

  count = 0;
  for(k=0;k<num_pins;k++){
    vortex[count].x = pin_sites[count+1].x;
    vortex[count].y = pin_sites[count+1].y;
    vortex[count].color = 1;
    vortex[count].id = k;
    vortex[count].fx = 0.0;
    vortex[count].fy = 0.0;
    vortex[count].cum_disp = 0;
    vortex[count].cum_x = 0;
    vortex[count].avgfx = 0;
    vortex[count].avgfy = 0;
    count++;
  }
  *nV = count;
  
  /* CIJOL: Routines for adding additional vortices are here: */
  
  /* Add four vortices to the middle of the sample */
  x = SX*17.0/13.0;
  y = SY*0.52;
  insert_vortex(x,y,nV,vortex);
  x = x - SX/4.0;
  insert_vortex(x,y,nV,vortex);
  x = x - SX/4.0;
  insert_vortex(x,y,nV,vortex);
  x = x - SX/4.0;
  insert_vortex(x,y,nV,vortex);

  printf("Have %d vortices\n",*nV);
}

/*==================== insert_vortex =========================*/
/* Adds vortex to structure at specified location. */
void insert_vortex(x,y,nV,vortex)
float x;
float y;
int *nV;
struct vortex *vortex;
{
  int count;
  
  count = *nV;
  vortex[count].x = x;
  vortex[count].y = y;
  vortex[count].color = 1;
  vortex[count].id = count;
  vortex[count].fx = 0.0;
  vortex[count].fy = 0.0;
  vortex[count].cum_disp = 0;
  vortex[count].cum_x = 0;
  vortex[count].avgfx = 0;
  vortex[count].avgfy = 0;
  count++;
  *nV = count;
}

/*==================== add_vortex_hexagonal ======================= */
/* This routine places the vortices inside the sample in a triangular*/
/* array, with a matching field in the vortex-addition region.  */
/* It is intended to simulate field-cooling, and is adapted from*/
/* add_pinning_hexagonal.*/
/* Here we need to lay down a triangular lattice that MUST */
/* NOT leave any gaps between the bottom and top of the    */
/* sample. Our strategy is to guess a lattice constant     */
/* that is close to what would fit in the requested number */
/* of wells. From that we get an integer # of rows.        */
/* Now, we get a lattice constant from the rows such that  */
/* the entire sample is covered. Then we find the # of     */
/* columns for this new lattice constant.                  */
  
void add_vortex_hexagonal(vortex,nV,max_vortex_num)
     struct vortex *vortex;
     int *nV;
     int *max_vortex_num;
{
  int k,l;
  float a;
  int row,col,curr_num_pin;

  curr_num_pin = 0;

  a = ((RIGHTSX ) * (SY))/(*max_vortex_num * SQRT3_2);
  a = (float)sqrt((double)(a));

  if (*max_vortex_num == 750)
    row = (int)((SY)/(SQRT3_2*a)) + 1;
  else
    row = (int)((SY)/(SQRT3_2*a));

  /* Now we make sure that we have an even # of rows   */
  /* If there are an odd number of rows, then it and   */
  /* the bottom row will form a square lattice.        */
  
  if ((row%2)) row--;
 
  a = SY/(SQRT3_2*row);

  printf("The vortex lattice constant is:  %f\n",a);

  col = (int)((RIGHTSX)/a);

  while((SX-col*a)>=a)
    col++;

  /* Here we alter the size of the system */
  /* Note that scale_x MUST be adjusted to conform to */
  /* the sys_size_x used to put down pinning array.*/
  SX = col*a;
  RIGHTSX = SX;
  SYS_size_x = SX;
  SYS_size2_x = SX/2.0;
  SYS_right_edge = RIGHTSX;
  SYS_scale_x = ((float)N_POT)/((float)SX);

  *max_vortex_num = row*col;

  fprintf(diagnosefile,"SX = %f, lattice const = %f\n",SX,a);
  fflush(diagnosefile);

  printf("\n\n\nNUMBER OF SITES PUT DOWN:  %d\n\n\n\n",(row*col));
  
  for(k=1; k<=row; k++) {
    for(l=1; l<=col; l++) {

      vortex[curr_num_pin].x = (l-1)*a;
      vortex[curr_num_pin].y = (k-1)*SQRT3_2*a;
      if (!(k%2)) vortex[curr_num_pin].x += .5*a;
      vortex[curr_num_pin].oldx = vortex[curr_num_pin].x;
      vortex[curr_num_pin].oldy = vortex[curr_num_pin].y; 
      vortex[curr_num_pin].color = 1;
      vortex[curr_num_pin].id = curr_num_pin;
      vortex[curr_num_pin].fx = 0.0;
      vortex[curr_num_pin].fy = 0.0;
      vortex[curr_num_pin].cum_disp = 0;
      vortex[curr_num_pin].cum_x = 0;
      vortex[curr_num_pin].avgfx = 0;
      vortex[curr_num_pin].avgfy = 0;
      curr_num_pin++;
    }
  }
  *nV = curr_num_pin;
  printf("Field cooling placed %d vortices\n",*nV);
}

#ifdef NOTUSING
/* =================== add_fake_vortices ========================== */

/* This routine calculates the force that would be created by a */
/* group of fixed vortices in a triangular lattice, and stores  */
/* them into the table lookup (also used for parabolic wells;   */
/* not exact).                                                  */

void add_fake_vortices()
{
  void read_array();
  void nrerror();

  int i,j,r_pin_cell_x,r_pin_cell_y,fake_i,fake_j;
  int x_i,y_j,i_start,j_start,N_max,forcei;
  float cut_off,fake_x,fake_y,F;
  float x,y,r_min,r_max;
  float size_x,size_y,size2_x,size2_y;
  float dist2,dist_x,dist_y;
  float fake_density;
  int fake_row;
  read_array(&N_max,&r_min,&r_max);

  /*When system width in y was doubled, fake_row was doubled*/
  /*fake_row was 24 when y was 18.  It was 48 when y was 36*/
  /*It was 32 when y was 24.*/
  /*This setting is now automatic.*/
  if(SY==18)
    fake_row = 24;
  else if(SY==24)
    fake_row = 32;
  else if(SY==36)
    fake_row = 48;
  else
    nrerror("Error: please set fake_row properly");

  fake_density = 0.75;

  size_x = (float)SX;
  size_y = (float)SY;

  size2_x = size_x/2.0;
  size2_y = size_y/2.0;

  r_pin_cell_x = (int)((6.0/size_x)*N_POT);
  r_pin_cell_y = (int)((6.0/size_y)*N_POT);

  cut_off = 36.0;

  for(fake_j = 0; fake_j < fake_row; fake_j++) {
    for(fake_i=0; fake_i<5; fake_i++) {

      fake_x = (SX - .5*fake_density) - fake_density*fake_i;
      fake_y = fake_density*fake_j;

      if (fake_j % 2) fake_x -= .5*fake_density;

      i_start = (int)((fake_x/size_x)*N_POT);
      j_start = (int)((fake_y/size_y)*N_POT); 

      for(j=j_start-r_pin_cell_y; j<j_start+r_pin_cell_y;j++) {
	for(i=i_start-r_pin_cell_x;i<i_start+r_pin_cell_x; i++) {

	  x_i = i; 
	  y_j = j;
	  
	  if(x_i <0)
	    x_i += N_POT;
	  if (x_i >= N_POT)
	    x_i -= N_POT;
      
	  if (y_j < 0)
	    y_j += N_POT;
	  if (y_j >= N_POT)
	    y_j -= N_POT;
	  
	  x = x_i*(size_x/N_POT);
	  y = y_j*(size_y/N_POT);

	  dist_x = x - fake_x;
	  dist_y = y - fake_y;
	  
	  if (dist_x>size2_x) dist_x -= size_x;
	  if (dist_x<-size2_x) dist_x += size_x;

	  if ( dist_y>size2_y) dist_y -= size_y;
	  if (dist_y<-size2_y) dist_y += size_y; 
	  dist2 = (dist_x*dist_x + dist_y*dist_y);
	  
	  if (dist2 > cut_off ) continue ; 

	  if (dist2 < r_min) {
	    F = forces[0]; 
	  }
	  else {
	    dist2 -= r_min;
	    forcei = (int)(dist2*potential_scale);
	    F = forces[forcei];
	    F *= 5.0;
	  }
	  
	  (((struct vector*)lookup)+y_j*N_POT + x_i)->x += F*dist_x;
	  (((struct vector*)lookup)+y_j*N_POT + x_i)->y += F*dist_y;
	  
	}
	
      }
    }
  }

}
#endif

/* =================== make_lookup_tableII =========================*/
/* Puts down non-overlapping pinning sites in three stages:  first */
/* puts down wells of radius .4 until unable to find non-overlapping */
/* site;  next puts down wells of radius .2 until unable to find */
/* non-overlapping site;  finishes by putting down wells of radius */
/* .1 until all available sites filled.  */

void make_lookup_tableII(Ap,r_pin,num_pin,pin_arrange,order,chan_width,
			 use_contour,id_string,min_rhop,max_rhop,
			 rhop_width,ratchetperiods)
float Ap;
float r_pin;
int *num_pin;
int pin_arrange;
float order;
float chan_width;
int use_contour;
char id_string[];
float min_rhop;
float max_rhop;
float rhop_width;
int ratchetperiods;
{
  void add_fake_vortices();
  int read_contour();
  int read_contour_ascii();
  void make_ratchet_pinning();
  void nrerror();

  int i,j;
  int num_so_far;

  /* Needed only for fake vortices */
#ifdef NOTUSING
  lookup     = (void *)malloc(sizeof(struct vector)*N_POT*N_POT);
  if (lookup == NULL) nrerror("Out of memory for lookup in make_lookup..\n");
#endif

  for(j=0; j<N_POT; j++) {
    for(i=0; i<N_POT; i++) {
#ifdef NOTUSING
      (((struct vector*)lookup)+j*N_POT + i)->x = 0.0;
      (((struct vector*)lookup)+j*N_POT + i)->y = 0.0;
#endif
      lookup_par[j][i] = 0;
    }
  }

  num_so_far = 0;

  switch (pin_arrange) {
  case PIN_PLACE_RANDOM:
    /*7.6.95 Following two lines used interchangably.  Call*/
    /*       read_contour when starting from an existing contour file;*/
    /*       call add_pinning when creating a new contour.*/
    /*7.6.95 CIJOL NOTE: modification for contour is here*/
    if(use_contour){
      num_so_far = read_contour_ascii(r_pin);
      /*num_so_far = read_contour(r_pin);*/
    }
    else{
      num_so_far = add_pinning(*num_pin,Ap,r_pin,LEFTSX,RIGHTSX,num_so_far,
			       id_string);
      /*num_so_far = add_pinning_channel(*num_pin,Ap,r_pin,LEFTSX,RIGHTSX,
				     num_so_far,chan_width,id_string);*/
    }
    break;
  case PIN_PLACE_HEXAGONAL:
    num_so_far = read_contour_ascii(r_pin);
/*    num_so_far = add_pinning_hexagonal(*num_pin,Ap,r_pin,order,id_string);*/
    break;
  case PIN_PLACE_CLUSTER:
    num_so_far = read_contour_ascii(r_pin);
/*    num_so_far = add_pinning_cluster(*num_pin,Ap,r_pin,id_string);*/
    break;
  case PIN_PLACE_SQUARE:
    add_pinning_square(num_pin,Ap,r_pin,order,id_string);
    break;
  case PIN_PLACE_RATCHET:
    /* New 6.22.00 Puts down stripes of pinning.  Farming it out a bit.*/
    make_ratchet_pinning(Ap,r_pin,id_string,min_rhop,max_rhop,
			 rhop_width,ratchetperiods);
    break;
  }

  fprintf(diagnosefile,"Pinning in place; NOT adding fake vortices\n");
  fflush(diagnosefile);

  /* Now put down the barrier region */ 
  /*CIJOL NOTE: FAKE VORTICES DISABLED 8.26.96*/
  /*Fake vortices re-enabled 11.13.96*/
  /* Fake vortices disabled again 6.23.97 */
  /*add_fake_vortices();*/
  
  printf("contour calcs are done!\n");
}

/*======================= make_ratchet_pinning =============================*/
/* Put down ratchet composed of strips of varying pinning density. */
/* It will be aligned with the X direction. */
void make_ratchet_pinning(Ap,r_pin,id_string,min_rhop,max_rhop,
			  rhop_width_in,ratchetperiods)
float Ap;
float r_pin;
char id_string[];
float min_rhop;
float max_rhop;
float rhop_width_in;
int ratchetperiods;
{
  float periodwidth;
  int numbands;
  float rhop_width;
  int i,j;
  float rhop_incr;
  int num_to_add;
  int num_so_far;
  float leftedge;
  float rightedge;
  float rhop;
  
  /* First, figure out the scales on the ratchet density gradations. */
  periodwidth = SX/(float)ratchetperiods;
  numbands = (int)(periodwidth/rhop_width_in);
  /* adjust width */
  rhop_width = SX/(float)(numbands*ratchetperiods);
  fprintf(diagnosefile,"Ratchet numbands %d width %f\n",numbands,rhop_width);
  fflush(diagnosefile);

  /* Now start adding the pinning one stripe at a time */
  rhop_incr = (max_rhop-min_rhop)/(float)numbands;
  num_so_far = 0;
  leftedge=0;
  for(i=0;i<ratchetperiods;i++){
    rhop = min_rhop;
    for(j=0;j<numbands;j++){
      num_to_add = (int)(rhop*rhop_width*SY);
      rightedge = leftedge+rhop_width;
      num_so_far = add_pinning(num_to_add,Ap,r_pin,leftedge,rightedge,
			       num_so_far,id_string);
      rhop += rhop_incr;
      leftedge=rightedge;
    }
  }
  
}

/*============================== start_here=================================*/

/* This routine allows you to start the calculations from the condition of */
/* the system as recorded in a frame from a movie file.                    */
/* NOTE: ALTERED to use NEW start_file format  8.23.95*/
/* Altered to use kstart files, 12.12.96.  These files represent a departure */
/* from the fortran format into a c format. */

void start_here(vortex,nV,filename)
struct vortex *vortex;
int *nV;
char filename[];
{
  void nrerror();

  FILE *in;
  char filein[80];
  int num_part;
  int i;
  
  strcpy(filein,filename);
  in = fopen(filein,"r");
  
  if (in == NULL) {
    nrerror("Initial starting file not found; program exiting\n");
  }

  fread(&num_part,sizeof(int),1,in);

  for(i=0;i<num_part;i++){
    if(feof(in))
      nrerror("Program requires kstart format");
    fread(&(vortex[i].x),sizeof(float),1,in);
    fread(&(vortex[i].y),sizeof(float),1,in);
    fread(&(vortex[i].id),sizeof(int),1,in);
    if(vortex[i].id > next_id_no)
      next_id_no = vortex[i].id;
    fread(&(vortex[i].cum_disp),sizeof(float),1,in);
    fread(&(vortex[i].cum_x),sizeof(float),1,in);
    vortex[i].avgfx = 0;
    vortex[i].avgfy = 0;
    vortex[i].oldx = vortex[i].x;
    vortex[i].oldy = vortex[i].y;
    vortex[i].color = 1;
    vortex[i].fx = 0.0;
    vortex[i].fy = 0.0;
  }

  if(feof(in))
    nrerror("Program requires kstart format.");

  *nV = num_part;
  next_id_no++;

  fclose(in);
}

/*============================= read_array() ==============================*/

/* reads a file containing the modified bessel function lookup table. */

void read_array(N_max,r_min,r_max)
int   *N_max;
float *r_min;
float *r_max;
{
  void nrerror();

  FILE *in_file;
  float r,r2,force,rmin,rmax;
  float F,F_max,F_min;
  int i;
  char filename[120];

  strcpy(filename,INPUTPREPEND);
  strcat(filename,"C2.txt");

  if((in_file  = fopen(filename,"r"))==NULL)
    nrerror("File C2.txt not found.");

  rmin =  1000000.0;
  rmax = -1000000.0;

  F_min =  1000000.0;
  F_max = -1000000.0;

  i=0;

  while( (fscanf(in_file,"%f %f",&r2,&force)) != EOF) {
    r    = (float)sqrt((double)(r2));
    rmin = MIN(rmin,r2);
    rmax = MAX(rmax,r2);
    F    = force/r;
    forces[i++] = Av*F;
    F_max = MAX(F_max,F);
    F_min = MIN(F_min,F);
  }

  potential_scale   = (float)(i)/(rmax - rmin); 
  potential_F_max   = F_max;

  *N_max = i;
  *r_min = rmin;
  *r_max = rmax;

fclose(in_file);

}

/*======================== write_special_contour ==================*/
/* This routine writes a special contour that will be read in by */
/* the program not unlike the start_file  6.10.95*/
void write_special_contour(num_pin,id_string)
     int num_pin;
     char id_string[];
{
  void nrerror();

  int i;
  char temp[20];
  char filename[100];
  FILE *contour2;

  sprintf(temp,"%d",myid);
  strcpy(filename,"cn");
  strcat(filename,id_string);

  if((contour2 = fopen(filename,"w"))==NULL){
    nrerror("Error opening contour2 file");
  }

  /*Write number of pinning sites*/
  fprintf(contour2,"%d\n",num_pin);
  
  /*Loop over all pinning sites and write the contents of each*/
  for(i=1; i<=num_pin; i++){
    fprintf(contour2,"%f\n",(pin_sites[i].x));
    fprintf(contour2,"%f\n",(pin_sites[i].y));
    fprintf(contour2,"%f\n",(pin_sites[i].Ap));
    fprintf(contour2,"%f\n",(pin_sites[i].depth));
    fprintf(contour2,"%f\n",(pin_sites[i].radius));
  }

  /*Close file and return*/
  fclose(contour2);
}

/*================== read_contour_ascii ================================*/
/* This routine reads in a previously created contour file and*/
/* uses it for all calculations.  Replaces function add_pinning().*/

int read_contour_ascii(r_pin)
     float r_pin;
{
  void nrerror();

  int i,j,k;
  float x2,y2,Ap,depth,radius;
  float size_x, size_y;
  float r_pin_cell_x, r_pin_cell_y;
  int num_pin;
  int x_i, y_j, i_start, j_start;
  char temp[20];
  char filename[100];
  FILE *contourin;
  /* Unused: */
  int temp2;

  size_x = (float) SX;
  size_y = (float) SY;
  r_pin_cell_x = (int)((r_pin/size_x)*N_POT);
  r_pin_cell_y = (int)((r_pin/size_y)*N_POT);

  /*Open appropriate contour file*/
#ifdef TODOSIG
  temp2 = PARATODOS;
  sprintf(temp,"%d",temp2);
#else
  sprintf(temp,"%d",myid);
#endif
  strcpy(filename,"contour");
  strcat(filename,temp);
  strcat(filename,".sta");

  if((contourin = fopen(filename,"r"))==NULL){
    nrerror("Error opening contour2 file");
  }
  
  /*Get number of pinning sites*/
  fscanf(contourin,"%d",&num_pin);
  
  /*Loop over pinning sites and fill pin_sites[k]*/
  for(k=1; k<=num_pin; k++){
    fscanf(contourin,"%f",&x2);
    fscanf(contourin,"%f",&y2);
    fscanf(contourin,"%f",&Ap);
    fscanf(contourin,"%f",&depth);
    fscanf(contourin,"%f",&radius);
    pin_sites[k].x = x2;
    pin_sites[k].y = y2;
    pin_sites[k].Ap = Ap;
    pin_sites[k].depth = depth;
    pin_sites[k].radius = radius;

    /*Now fill the structure lookup_par[][]*/

    i_start = (int)((x2/size_x)*N_POT);
    j_start = (int)((y2/size_y)*N_POT);
    
    for(j=j_start-r_pin_cell_y; j<j_start+r_pin_cell_y; j++){
      for(i=i_start-r_pin_cell_x; i<i_start+r_pin_cell_x; i++){
	
	x_i = i;
	y_j = j;

	if (x_i < 0)
	  x_i += N_POT;
	if (x_i >= N_POT)
	  x_i -= N_POT;

	if (y_j < 0)
	  y_j += N_POT;
	if (y_j >= N_POT)
	  y_j -= N_POT;

	lookup_par[y_j][x_i] = k;

      }
    }
  }

  /*Return the number of pinning sites*/
  printf("Contour read completed!\n");
  fclose(contourin);
  return(num_pin);
}

/*================== read_contour ================================*/
/* This routine reads in a previously created contour file and*/
/* uses it for all calculations.  Replaces function add_pinning().*/
/* Uses a binary contour file.*/

int read_contour(r_pin)
     float r_pin;
{
  void nrerror();

  int i,j,k;
  float x2,y2,Ap,depth,radius;
  float size_x, size_y;
  float r_pin_cell_x, r_pin_cell_y;
  int num_pin;
  int x_i, y_j, i_start, j_start;
  char temp[20];
  char filename[100];
  FILE *contourin;
  /* Unused: */
  int temp2;

  size_x = (float) SX;
  size_y = (float) SY;
  r_pin_cell_x = (int)((r_pin/size_x)*N_POT);
  r_pin_cell_y = (int)((r_pin/size_y)*N_POT);

  /*Open appropriate contour file*/
#ifdef TODOSIG
  temp2 = PARATODOS;
  sprintf(temp,"%d",temp2);
#else
  sprintf(temp,"%d",myid);
#endif
  strcpy(filename,"contour_node");
  strcat(filename,temp);
  strcat(filename,".bin");

  if((contourin = fopen(filename,"r"))==NULL){
    nrerror("Error opening contour2 file");
  }

  /*Get number of pinning sites*/
  fread(&num_pin,sizeof(int),1,contourin);
  
  /*Loop over pinning sites and fill pin_sites[k]*/
  for(k=1; k<=num_pin; k++){
    fread(&x2,sizeof(float),1,contourin);
    fread(&y2,sizeof(float),1,contourin);
    fread(&Ap,sizeof(float),1,contourin);
    fread(&depth,sizeof(float),1,contourin);
    fread(&radius,sizeof(float),1,contourin);

    pin_sites[k].x = x2;
    pin_sites[k].y = y2;
    pin_sites[k].Ap = Ap;
    pin_sites[k].depth = depth;
    pin_sites[k].radius = radius;

    /*Now fill the structure lookup_par[][]*/

    i_start = (int)((x2/size_x)*N_POT);
    j_start = (int)((y2/size_y)*N_POT);
    
    for(j=j_start-r_pin_cell_y; j<j_start+r_pin_cell_y; j++){
      for(i=i_start-r_pin_cell_x; i<i_start+r_pin_cell_x; i++){
	
	x_i = i;
	y_j = j;

	if (x_i < 0)
	  x_i += N_POT;
	if (x_i >= N_POT)
	  x_i -= N_POT;

	if (y_j < 0)
	  y_j += N_POT;
	if (y_j >= N_POT)
	  y_j -= N_POT;

	lookup_par[y_j][x_i] = k;

      }
    }
  }

  /*Return the number of pinning sites*/
  printf("Contour read completed!\n");
  fclose(contourin);
  return(num_pin);
}

#ifdef NOTUSING
/*========================= updateRiver ==========================*/
/* This routine uses the new and old positions of the vortices to mark*/
/* vortex trails on the river structure. */
/* Next addition: considers only vortices that fall within the sample*/
/* Bug fix: periodic B.C. only in y, not in x.*/
/* Bug fix for new sample config: periodic B.C. on x and y */
void updateRiver(vortex,nV)
struct vortex *vortex;
int nV;
{
  int i,x,y,k;
  float x1,y1,dx,dy,x2,y2,x_inc,y_inc;
  float xscale;
  int dx2,dy2,steps,temp1,temp2;

  static float yscale=((float)R_POT)/((float)(SY));
  static int firsttime = 1;
  xscale=((float)R_POT)/(RIGHTSX-LEFTSX);

  if(firsttime){
    for(i=0;i<nV;i++){
      vortex[i].rivx = vortex[i].x-LEFTSX;
      vortex[i].rivy = vortex[i].y;
    }
    firsttime = 0;
    return;
  }

  for(i=0;i<nV;i++){
    /*Find pinning sites intersected by this vortex as it moved. */
    /*This code is adapted from check_pinning in twincurrent.c*/
    
    x1 = (int)((vortex[i].rivx)*xscale);
    y1 = (int)((vortex[i].rivy)*yscale);
    x = (int)((vortex[i].x-LEFTSX)*xscale);
    y = (int)((vortex[i].y)*yscale);

    /* USE THIS FOR GRADIENT DRIVING: */
    /*No periodic BC on x: the vortex may fall outside sample.  If so,*/
    /*skip this vortex.*/
    /*if((x1<0)||(x<0)||(x1>=R_POT)||(x>=R_POT)){*/
    /*  vortex[i].rivx = vortex[i].x-LEFTSX;*/
    /*  vortex[i].rivy = vortex[i].y;*/
    /*  continue;*/
    /*}*/
    
    dx = x - x1;
    dy = y - y1;

    /*Periodic BC in y only.*/
    if(dy>R_POT2) dy -= R_POT;
    if(dy<-R_POT2) dy += R_POT;

    /* USE THIS FOR CURRENT DRIVING: */
    if(dx>R_POT2) dx -= R_POT;
    if(dx<-R_POT2) dx += R_POT;

    dx2 = ABS(dx);
    dy2 = ABS(dy);

    /*Screen out particles that moved less than a minimum distance*/
    if(!dx && !dy) continue;
    /*CIJOL tinkering with this 11.14.96*/
    if((dx*dx+dy*dy)<3) continue;

    /* Increment the appropriate elements of the data structure */
    
    if(dx2>dy2)
      steps=dx2;
    else
      steps=dy2;

    x_inc = (float)(dx)/steps;
    y_inc = (float)(dy)/steps;

    x2 = (float)x1;
    y2 = (float)y1;

    for(k=1;k<=steps;k++){
      temp1=(int)(y2+.5);
      temp2=(int)(x2+.5);

      /*Bug fix: Periodic b.c. in y direction only*/
      if(temp1>=R_POT) temp1 -= R_POT;
      if(temp1<0) temp1 += R_POT;

      /* USE THIS FOR CURRENT DRIVING */
      if(temp2>=R_POT) temp2 -= R_POT;
      if(temp2<0) temp2 += R_POT;

      /* USE THIS FOR GRADIENT DRIVING */
      /*CIJOL ADDED 1.29.97*/
      /*If vortex now falls outside sample, skip to next vortex.*/
      /*if((temp2<0)||(temp2<0)||(temp2>=R_POT)||(temp2>=R_POT))*/
	/*break;*/

      river[temp1][temp2]++;
      x2 += x_inc;
      y2 += y_inc;

      /*Bug fix: adding periodic b.c. in y direction only*/
      /*BUG FIX: THIS WAS CHANGED FROM SY TO R_POT*/
      if(y2>=R_POT) y2 -= R_POT;
      if(y2<0) y2+= R_POT;

      /*USE THIS FOR CURRENT DRIVING */
      if(x2>=R_POT) x2 -= R_POT;
      if(x2<0) x2 += R_POT;
    }
    vortex[i].rivx = vortex[i].x-LEFTSX;
    vortex[i].rivy = vortex[i].y;
  }
}
#endif

#ifdef NOTUSING
/*=========================== dumpRiver =================================*/
/* This code writes out the contents of the river structure */
/* Code now requires id_string pass.*/
/* Write made binary 12.12.96*/
void dumpRiver(time,id_string)
int time;
char id_string[];
{
  int i,j;
  FILE *out;
  char filename[120],temp[30];
  
  strcpy(filename,"river");
  strcat(filename,id_string);
  strcat(filename,".");
  sprintf(temp,"%d",time);
  strcat(filename,temp);
  out = fopen(filename,"w");
  
  for(i=0;i<R_POT;i++){
    for(j=0;j<R_POT;j++){
      /*fwrite(&i,sizeof(int),1,out);*/
      /*fwrite(&j,sizeof(int),1,out);*/
      fwrite(&(river[j][i]),sizeof(int),1,out);
    }
  }
  fclose(out);
}
#endif
#ifdef NOTUSING
/*--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==--==*/
/*  Defined constants and global variables for Voronoi routines */
#ifndef NULL
#define NULL 0
#endif
#define DELETED -2

struct Site *nextone();

int debug;

struct	Freenode	{
struct	Freenode	*nextfree;
};
struct	Freelist	{
struct	Freenode	*head;
int			nodesize;
};
char *getfree();
/*char *malloc();*/
char *myalloc();

float xmin, xmax, ymin, ymax, deltax, deltay;
float pxmin, pxmax, pymin, pymax;
int ntry, totalsearch;

struct Point	{
float x,y;
};

/* structure used both for sites and for vertices */
struct Site	{
struct	Point	coord;
int		sitenbr;
int		refcnt;
};

/* CIJOL: sites needs to be alloc'ed, then freed */
struct	Site	*sites;
int		nsites;
int		siteidx;
int		sqrt_nsites;
int		nvertices;
struct 	Freelist sfl;
struct	Site	*bottomsite;

struct Edge	{
float		a,b,c;
struct	Site 	*ep[2];
struct	Site	*reg[2];
int		edgenbr;
};
#define le 0
#define re 1
int nedges;
struct	Freelist efl;

int has_endpoint(),right_of();
struct Site *intersect();
float dist();
struct Point PQ_min();
struct Halfedge *PQextractmin();
struct Edge *bisect();

struct Halfedge {
struct Halfedge	*ELleft, *ELright;
struct Edge	*ELedge;
int		ELrefcnt;
char		ELpm;
struct	Site	*vertex;
float		ystar;
struct	Halfedge *PQnext;
};

struct   Freelist	hfl;
struct	Halfedge *ELleftend, *ELrightend;
int 	ELhashsize;
/* This structure needs to be freed when done: ELhash */
struct	Halfedge **ELhash;
struct	Halfedge *HEcreate(), *ELleft(), *ELright(), *ELleftbnd();
struct	Site *leftreg(), *rightreg();

int PQhashsize;
/* This structure needs to be freed when done: PQhash */
struct	Halfedge *PQhash;
struct	Halfedge *PQfind();
int PQcount;
int PQmin;
int PQempty();

/*============================= voronoi_construct ================*/
/*
 * The author of this software is Steven Fortune.  Copyright (c) 1994 by AT&T
 * Bell Laboratories.
 * Permission to use, copy, modify, and distribute this software for any
 * purpose without fee is hereby granted, provided that this entire notice
 * is included in all copies of any software which is or includes a copy
 * or modification of this software and in all copies of the supporting
 * documentation for such software.
 * THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED
 * WARRANTY.  IN PARTICULAR, NEITHER THE AUTHORS NOR AT&T MAKE ANY
 * REPRESENTATION OR WARRANTY OF ANY KIND CONCERNING THE MERCHANTABILITY
 * OF THIS SOFTWARE OR ITS FITNESS FOR ANY PARTICULAR PURPOSE.
 */
/* Data structures in program:
    rx, ry hold x and y positions of vortex.
    Site structure:  Used for both sites and vertices.
    site[i].sitenbr = Number of vertices that site i has.
    site[i].refcnt 
    site[i].Point.x, site[i].Point.y = Location of site
*/
void voronoi_construct(nV,vortex,time)
int nV;
struct vortex *vortex;
int time;
{
  void calculate_voronoi();
  void plot_postscript();
  void get_side_statistics();

  float xdat[5000],ydat[5000];
  float rx[4000],ry[4000];
  float vertx[20000],verty[20000];
  int cjoleft[20000],cjoright[20000];
  int tripA[20000],tripB[20000],tripC[20000];
  int sidenum[5000];
  int site_counter;
  int num_vert;
  int num_ep;
  int num_trip;
  int i;
  int numdefects;
  float frac_p5,frac_p6,frac_p7;
  int defects_written;

  /*CIJOL: For holding lines data */
  float linea[10000],lineb[10000],linec[10000];
  int numlines;

  /* CIJOL test: zero vertex structures */
  for(i=0;i<20000;i++){
    vertx[i] = 0;
    verty[i] = 0;
  }

  site_counter = 0;
  num_vert = 0;
  num_ep = 0;
  num_trip = 0;

  /* Pull required information out of vortex structure */
  for(i=0;i<nV;i++){
    xdat[i] = vortex[i].x;
    ydat[i] = vortex[i].y;
  }

  /* Now pass this info to the att voronoi program.*/

  calculate_voronoi(nV,xdat,ydat,linea,lineb,linec,&numlines,
		    rx,ry,&site_counter,vertx,verty,&num_vert,
		    cjoleft,cjoright,&num_ep,
		    tripA,tripB,tripC,&num_trip);

  /* Calculate the number of vortices with N sides */
  get_side_statistics(nV,tripA,tripB,tripC,num_trip,sidenum,rx,ry,
		      &frac_p5,&frac_p6,&frac_p7,&numdefects);

  /* Print these numbers to the ongoing files */
  /* CIJOL making ascii */
  /*fwrite(&frac_p5,sizeof(float),1,psub5file);
  fwrite(&frac_p6,sizeof(float),1,psub6file);
  fwrite(&frac_p7,sizeof(float),1,psub7file);*/
  fprintf(psub5file,"%f\n",frac_p5);
  fprintf(psub6file,"%f\n",frac_p6);
  fprintf(psub7file,"%f\n",frac_p7);

  /* CIJOL: We need to free the memory used for the sites structure*/
  /*        and for the ELhash, PQhash structures.*/
  free(sites);
  free(ELhash);
  free(PQhash);

}
/*=================== get_side_statistics ==============*/
/* Use Delaunay triangulation data to find # of vortices with N */
/* sides. */
void get_side_statistics(nV,tripA,tripB,tripC,num_trip,sidenum,rx,ry,
			 frac_p5,frac_p6,frac_p7,numdefects)
int nV;
int tripA[],tripB[],tripC[];
int num_trip;
int sidenum[];
float rx[],ry[];
float *frac_p5,*frac_p6,*frac_p7;
int *numdefects;
{
  int i;
  float a;
  float lowerx,lowery,upperx,uppery;
  int P5,P6,P7,numP;

  /* For each triple in which a vortex appears, count one */
  /* side for that vortex's voronoi polygon.  Go through */
  /* all triples and count.  Realize that, due to the lack */
  /* of periodic boundary conditions in this ATT code, */
  /* we need to throw away data from vortices on the edges of */
  /* the system. */

  /* First, zero our data structure.*/
  for(i=0;i<nV;i++){
    sidenum[i]=0;
  }

  /* Now, do the counting */
  for(i=0;i<num_trip;i++){
    sidenum[tripA[i]]++;
    sidenum[tripB[i]]++;
    sidenum[tripC[i]]++;
  }

  /* Now accumulate statistics for P5, P6, and P7. */
  
  P5 = 0;
  P6 = 0;
  P7 = 0;
  for(i=0;i<nV;i++){
    switch(sidenum[i]){
    case 5:
      P5++;
      break;
    case 6:
      P6++;
      break;
    case 7:
      P7++;
      break;
    }
  }

  *frac_p5 = (float) P5/ (float) nV;
  *frac_p6 = (float) P6/ (float) nV;
  *frac_p7 = (float) P7/ (float) nV;

  /* Find out the total number of non-six-sided cells and pass */
  /* back to calling program */
  *numdefects = nV - P6;
    
}
/*======================= calculate_voronoi ============*/
/* CIJOL making this a routine (was main()): */

void calculate_voronoi(nVin,xdatin,ydatin,linea,lineb,linec,numlines,
		       rx,ry,site_counter,vertx,verty,num_vert,
		       cjoleft,cjoright,num_ep,
		       tripA,tripB,tripC,num_trip)
int nVin;  /* Number of particles */
float xdatin[]; /*X positions of particles */
float ydatin[]; /*Y positions of particles */
float linea[]; /* Equations of lines */
float lineb[];
float linec[];
int *numlines; /*Number of lines */
float rx[4000],ry[4000];
int *site_counter;
float vertx[];
float verty[];
int *num_vert;
int cjoleft[];
int cjoright[];
int *num_ep;
int tripA[];
int tripB[];
int tripC[];
int *num_trip;
{	
  void voronoi();
  void geominit();

  struct Site *(*next)();

  void read_program_sites();

  /*CIJOL added:*/
  int num_part;

  *numlines = 0;
  
  /* Here are the different switches the program accepts. */
  debug = 0; /* Keep this one off! */

  freeinit(&sfl, sizeof *sites);

  /* CIJOL: Now receiving data directly from calling program.*/
  read_program_sites(&num_part,nVin,xdatin,ydatin);
  next = nextone;

  /* We have now read in all of the data on particle positions. */
  siteidx = 0;
  geominit();

  voronoi(next,rx,ry,site_counter,vertx,verty,num_vert,
	  linea,lineb,linec,numlines,cjoleft,cjoright,num_ep,
	  tripA,tripB,tripC,num_trip); 
  
  /* Go back to the calling program, eventually with data... */
  return;
}

/*=============== scomp ===================*/
/* sort sites on y, then x, coord */
int scomp(s1,s2)
struct Point *s1,*s2;
{
  if(s1 -> y < s2 -> y) return(-1);
  if(s1 -> y > s2 -> y) return(1);
  if(s1 -> x < s2 -> x) return(-1);
  if(s1 -> x > s2 -> x) return(1);
  return(0);
}

/*============= nextone =============*/
/* return a single in-storage site */
struct Site *nextone()
{
  struct Site *s;
  if(siteidx < nsites){	
    s = &sites[siteidx];
    siteidx += 1;
    return(s);
  }
  else
    return( (struct Site *)NULL);
}

/*================== read_program_sites ===================*/
/* read all sites, sort, and compute xmin, xmax, ymin, ymax */
/* Assumes that it has already been passed the needed data.*/
/* It just funnels this data into the correct structures.*/
void read_program_sites(num_part,nvin,xdatin,ydatin)
int *num_part;
int nvin;
float xdatin[];
float ydatin[];
{
  int i;
  
  nsites=0;
  sites = (struct Site *) myalloc(4000*sizeof *sites);

  *num_part = nvin;
  nsites = nvin;
  for(i=0;i<*num_part;i++){
    sites[i].coord.x = xdatin[i];
    sites[i].coord.y = ydatin[i];
    sites[i].sitenbr = i;
    sites[i].refcnt = 0;
    if ((i)&&(i % 4000 == 0)) 
      sites = (struct Site *) realloc(sites,(nsites+4000)*sizeof*sites);
  };
  
  qsort(sites, nsites, sizeof *sites, scomp);
  xmin=sites[0].coord.x; 
  xmax=sites[0].coord.x;
  for(i=1; i<nsites; i+=1){	
    if(sites[i].coord.x < xmin) xmin = sites[i].coord.x;
    if(sites[i].coord.x > xmax) xmax = sites[i].coord.x;
  }
  ymin = sites[0].coord.y;
  ymax = sites[nsites-1].coord.y;
}

/*======================= voronoi ===============*/
/* implicit parameters: nsites, sqrt_nsites, xmin, xmax, ymin, ymax,
   deltax, deltay (can all be estimates).
   Performance suffers if they are wrong; better to make nsites,
   deltax, and deltay too big than too small.  (?) */

void voronoi(nextsite,rx,ry,site_counter,vertx,verty,num_vert,
	linea,lineb,linec,numlines,cjoleft,cjoright,num_ep,
	tripA,tripB,tripC,num_trip)
struct Site *(*nextsite)();
float rx[],ry[];
int *site_counter;
float vertx[];
float verty[];
int *num_vert;
float linea[];
float lineb[];
float linec[];
int *numlines;
int cjoleft[];
int cjoright[];
int *num_ep;
int tripA[];
int tripB[];
int tripC[];
int *num_trip;
{
  struct Site *newsite, *bot, *top, *temp, *p;
  struct Site *v;
  struct Point newintstar;
  int pm;
  struct Halfedge *lbnd, *rbnd, *llbnd, *rrbnd, *bisector;
  struct Edge *e;
  
  PQinitialize();
  bottomsite = (*nextsite)();

  /* Write out the coordinates of this "bottom" site:*/
  out_site(bottomsite,rx,ry,site_counter);

  ELinitialize();
  
  newsite = (*nextsite)();
  while(1)
    {
      if(!PQempty()) newintstar = PQ_min();
      
      if (newsite != (struct Site *)NULL 
	  && (PQempty() 
	      || newsite -> coord.y < newintstar.y
	      || (newsite->coord.y == newintstar.y 
		  && newsite->coord.x < newintstar.x)))
	{/* new site is smallest */
	  
	  /* Write out the coordinates of this "new" site: */
	  out_site(newsite,rx,ry,site_counter);

	  lbnd = ELleftbnd(&(newsite->coord));
	  rbnd = ELright(lbnd);
	  bot = rightreg(lbnd);
	  e = bisect(bot, newsite,linea,lineb,linec,numlines);
	  bisector = HEcreate(e, le);
	  ELinsert(lbnd, bisector);
	  if ((p = intersect(lbnd, bisector)) != (struct Site *) NULL) 
	    {	PQdelete(lbnd);
		PQinsert(lbnd, p, dist(p,newsite));
	      };
	  lbnd = bisector;
	  bisector = HEcreate(e, re);
	  ELinsert(lbnd, bisector);
	  if ((p = intersect(bisector, rbnd)) != (struct Site *) NULL)
	    {	PQinsert(bisector, p, dist(p,newsite));	
	      };
	  newsite = (*nextsite)();	
	}
      else if (!PQempty()) 	/* intersection is smallest */
	{
	  lbnd = PQextractmin();
	  llbnd = ELleft(lbnd);
	  rbnd = ELright(lbnd);
	  rrbnd = ELright(rbnd);
	  bot = leftreg(lbnd);
	  top = rightreg(rbnd);
	  
	  /* Write out this triple of numbers: bot, top, and rightreg.*/
	  out_triple(bot, top, rightreg(lbnd),tripA,tripB,tripC,num_trip);

	  v = lbnd->vertex;

	  /* Create a vertex; also write it out.*/
	  makevertex(v,vertx,verty,num_vert);

	  /* Write out the two endpoints of this segment? */
	  endpoint(lbnd->ELedge,lbnd->ELpm,v,cjoleft,cjoright,num_ep);
	  endpoint(rbnd->ELedge,rbnd->ELpm,v,cjoleft,cjoright,num_ep);

	  ELdelete(lbnd); 
	  PQdelete(rbnd);
	  ELdelete(rbnd); 
	  pm = le;
	  if (bot->coord.y > top->coord.y)
	    {	temp = bot; bot = top; top = temp; pm = re;}

	  /* This will write out bisector location */
	  e = bisect(bot, top,linea,lineb,linec,numlines);

	  bisector = HEcreate(e, pm);
	  ELinsert(llbnd, bisector);
	  
	  /*This will write out endpoints*/
	  endpoint(e, re-pm, v,cjoleft,cjoright,num_ep);

	  deref(v);
	  if((p = intersect(llbnd, bisector)) != (struct Site *) NULL)
	    {	PQdelete(llbnd);
		PQinsert(llbnd, p, dist(p,bot));
	      };
	  if ((p = intersect(bisector, rrbnd)) != (struct Site *) NULL)
	    {	PQinsert(bisector, p, dist(p,bot));
	      };
	}
      else break;
    };
  
  for(lbnd=ELright(ELleftend); lbnd != ELrightend; lbnd=ELright(lbnd))
    {	e = lbnd -> ELedge;
	/* Write out endpoints */
	out_ep(e,cjoleft,cjoright,num_ep);
      };
}
/*================= out_bisector ==========*/
/* CIJOL Adding a pass of the lines data back to calling program.*/
out_bisector(e,linea,lineb,linec,numlines)
struct Edge *e;
float linea[];
float lineb[];
float linec[];
int *numlines;
{
  int num;

  num = *numlines;

  /*fprintf(out,"l %f %f %f", e->a, e->b, e->c);*/
  /*Put this data into structure.*/
  linea[num] = e->a;
  lineb[num] = e->b;
  linec[num] = e->c;
  num++;
  if(num>10000){
    printf("Not enough room for lines\n");
    exit(-1);
  }
  *numlines = num;

  if(debug)
    printf("line(%d) %gx+%gy=%g, bisecting %d %d\n", e->edgenbr,
	   e->a, e->b, e->c, e->reg[le]->sitenbr, e->reg[re]->sitenbr);
}

/*================ out_ep =============*/
/* Writes out an end point.*/
/* Each edge is numbered, and the other two numbers give the */
/* vertices at either end of this edge.*/
out_ep(e,cjoleft,cjoright,num_ep)
struct Edge *e;
int cjoleft[];
int cjoright[];
int *num_ep;
{
  int tmp;

  clip_line(e);

  /*fprintf(out,"e %d", e->edgenbr);
  fprintf(out," %d ", e->ep[le] != (struct Site *)NULL ? e->ep[le]->sitenbr : -1);
  fprintf(out,"%d\n", e->ep[re] != (struct Site *)NULL ? e->ep[re]->sitenbr : -1);*/

  tmp = *num_ep;
  cjoleft[e->edgenbr]= 
    (e->ep[le] != (struct Site *)NULL ? e->ep[le]->sitenbr : -1);
  cjoright[e->edgenbr]= 
    (e->ep[re] != (struct Site *)NULL ? e->ep[re]->sitenbr : -1);
  tmp++;
  *num_ep = tmp;
}

/*======================== out_vertex ===================*/
/*This routine outputs the x and y coordinates of a vertex*/
/* Passes back to program vertices indexed by vertex number.*/
out_vertex(v,vertx,verty,num_vert)
struct Site *v;
float vertx[];
float verty[];
int *num_vert;
{
  int tmp;
  /*fprintf (out,"v %f %f\n", v->coord.x, v->coord.y);*/
  vertx[v->sitenbr] = v->coord.x;
  verty[v->sitenbr] = v->coord.y;
  tmp = *num_vert;
  tmp++;
  *num_vert = tmp;

  if(debug)
    printf("vertex(%d) at %f %f\n", v->sitenbr, v->coord.x, v->coord.y);
}

/*======================= out_site ==============================*/
/*This routine outputs the x and y coordinates of an input particle*/
/* CIJOL changing a bit... should now pass back coords. indexed*/
/* by site number.*/
out_site(s,rx,ry,site_counter)
struct Site *s;
float rx[],ry[];
int *site_counter;
{
  int tmp;

  /*fprintf(out,"s %f %f\n", s->coord.x, s->coord.y);*/
  rx[s->sitenbr] = s->coord.x;
  ry[s->sitenbr] = s->coord.y;
  tmp = *site_counter;
  tmp++;
  *site_counter = tmp;

  if(debug)
    printf("site (%d) at %f %f\n", s->sitenbr, s->coord.x, s->coord.y);
}

/*============== out_triple ===========*/
/* For delaunay triangulation */
out_triple(s1, s2, s3,tripA,tripB,tripC,num_trip)
struct Site *s1, *s2, *s3;
int tripA[];
int tripB[];
int tripC[];
int *num_trip;
{
  int tmp;
  /*fprintf(out,"%d %d %d\n", s1->sitenbr, s2->sitenbr, s3->sitenbr);*/
  tmp = *num_trip;
  tripA[tmp] = s1->sitenbr;
  tripB[tmp] = s2->sitenbr;
  tripC[tmp] = s3->sitenbr;
  tmp++;
  *num_trip = tmp;
    
  if(debug)
    printf("circle through left=%d right=%d bottom=%d\n", 
	   s1->sitenbr, s2->sitenbr, s3->sitenbr);
}


/*============== clip_line =============*/
int clip_line(e)
struct Edge *e;
{
  struct Site *s1, *s2;
  float x1,x2,y1,y2;
  
  if(e -> a == 1.0 && e ->b >= 0.0){
    s1 = e -> ep[1];
    s2 = e -> ep[0];
  }
  else{	
    s1 = e -> ep[0];
    s2 = e -> ep[1];
  };
  
  if(e -> a == 1.0){
    y1 = pymin;
    if (s1!=(struct Site *)NULL && s1->coord.y > pymin)
      y1 = s1->coord.y;
    if(y1>pymax) return;
    x1 = e -> c - e -> b * y1;
    y2 = pymax;
    if (s2!=(struct Site *)NULL && s2->coord.y < pymax) 
      y2 = s2->coord.y;
    if(y2<pymin) return(0);
    x2 = e -> c - e -> b * y2;
    if ((x1> pxmax & x2>pxmax) | (x1<pxmin&x2<pxmin)) return;
    if(x1> pxmax){
      x1 = pxmax; y1 = (e -> c - x1)/e -> b;};
    if(x1<pxmin){
      x1 = pxmin; y1 = (e -> c - x1)/e -> b;};
    if(x2>pxmax){
      x2 = pxmax; y2 = (e -> c - x2)/e -> b;};
    if(x2<pxmin){
      x2 = pxmin; y2 = (e -> c - x2)/e -> b;};
  }
  else{
    x1 = pxmin;
    if (s1!=(struct Site *)NULL && s1->coord.x > pxmin) 
      x1 = s1->coord.x;
    if(x1>pxmax) return(0);
    y1 = e -> c - e -> a * x1;
    x2 = pxmax;
    if (s2!=(struct Site *)NULL && s2->coord.x < pxmax) 
      x2 = s2->coord.x;
    if(x2<pxmin) return(0);
    y2 = e -> c - e -> a * x2;
    if ((y1> pymax & y2>pymax) | (y1<pymin&y2<pymin)) return(0);
    if(y1> pymax){
      y1 = pymax; x1 = (e -> c - y1)/e -> a;
    };
    if(y1<pymin){
      y1 = pymin; x1 = (e -> c - y1)/e -> a;
    };
    if(y2>pymax){
      y2 = pymax; x2 = (e -> c - y2)/e -> a;
    };
    if(y2<pymin){
      y2 = pymin; x2 = (e -> c - y2)/e -> a;
    };
  };
	
}

/*============== ELinitialize ================*/
ELinitialize()
{
  int i;

  freeinit(&hfl, sizeof **ELhash);
  ELhashsize = 2 * sqrt_nsites;
  ELhash = (struct Halfedge **) myalloc ( sizeof *ELhash * ELhashsize);
  for(i=0; i<ELhashsize; i +=1) ELhash[i] = (struct Halfedge *)NULL;
  ELleftend = HEcreate( (struct Edge *)NULL, 0);
  ELrightend = HEcreate( (struct Edge *)NULL, 0);
  ELleftend -> ELleft = (struct Halfedge *)NULL;
  ELleftend -> ELright = ELrightend;
  ELrightend -> ELleft = ELleftend;
  ELrightend -> ELright = (struct Halfedge *)NULL;
  ELhash[0] = ELleftend;
  ELhash[ELhashsize-1] = ELrightend;
}

/*============ HEcreate ==============*/
struct Halfedge *HEcreate(e, pm)
struct Edge *e;
int pm;
{
  struct Halfedge *answer;
  answer = (struct Halfedge *) getfree(&hfl);
  answer -> ELedge = e;
  answer -> ELpm = pm;
  answer -> PQnext = (struct Halfedge *) NULL;
  answer -> vertex = (struct Site *) NULL;
  answer -> ELrefcnt = 0;
  return(answer);
}

/*========== ELinsert ==============*/
ELinsert(lb, new)
struct	Halfedge *lb, *new;
{
  new -> ELleft = lb;
  new -> ELright = lb -> ELright;
  (lb -> ELright) -> ELleft = new;
  lb -> ELright = new;
}

/*=============== ELgethash ===============*/
/* Get entry from hash table, pruning any deleted nodes */
struct Halfedge *ELgethash(b)
int b;
{
  struct Halfedge *he;
  
  if(b<0 || b>=ELhashsize) return((struct Halfedge *) NULL);
  he = ELhash[b]; 
  if (he == (struct Halfedge *) NULL || 
      he -> ELedge != (struct Edge *) DELETED ) return (he);
  
  /* Hash table points to deleted half edge.  Patch as necessary. */
  ELhash[b] = (struct Halfedge *) NULL;
  if ((he -> ELrefcnt -= 1) == 0) makefree(he, &hfl);
  return ((struct Halfedge *) NULL);
}	

/*============== ELleftbnd =================*/
struct Halfedge *ELleftbnd(p)
struct Point *p;
{
  int i, bucket;
  struct Halfedge *he;
  
  /* Use hash table to get close to desired halfedge */
  bucket = (p->x - xmin)/deltax * ELhashsize;
  if(bucket<0) bucket =0;
  if(bucket>=ELhashsize) bucket = ELhashsize - 1;
  he = ELgethash(bucket);
  if(he == (struct Halfedge *) NULL){   
    for(i=1; 1 ; i += 1){	
      if ((he=ELgethash(bucket-i)) != (struct Halfedge *) NULL) break;
      if ((he=ELgethash(bucket+i)) != (struct Halfedge *) NULL) break;
    };
    totalsearch += i;
  };
  ntry += 1;
  /* Now search linear list of halfedges for the corect one */
  if (he==ELleftend  || (he != ELrightend && right_of(he,p))){
    do{
      he = he -> ELright;
    } while (he!=ELrightend && right_of(he,p));
    he = he -> ELleft;
  }
  else 
    do {
      he = he -> ELleft;
    } while (he!=ELleftend && !right_of(he,p));

  /* Update hash table and reference counts */
  if(bucket > 0 && bucket <ELhashsize-1){
    if(ELhash[bucket] != (struct Halfedge *) NULL) 
      ELhash[bucket] -> ELrefcnt -= 1;
    ELhash[bucket] = he;
    ELhash[bucket] -> ELrefcnt += 1;
  };
  return (he);
}

/*=============== ELdelete =================*/
/* This delete routine can't reclaim node, since pointers from hash
   table may be present.   */
ELdelete(he)
struct Halfedge *he;
{
  (he -> ELleft) -> ELright = he -> ELright;
  (he -> ELright) -> ELleft = he -> ELleft;
  he -> ELedge = (struct Edge *)DELETED;
}


/*============= ELright =============*/
struct Halfedge *ELright(he)
struct Halfedge *he;
{
  return (he -> ELright);
}

/*=========== ELleft =================*/
struct Halfedge *ELleft(he)
struct Halfedge *he;
{
  return (he -> ELleft);
}

/*=============== leftreg ===============*/
struct Site *leftreg(he)
struct Halfedge *he;
{
  if(he -> ELedge == (struct Edge *)NULL) return(bottomsite);
  return( he -> ELpm == le ? 
	 he -> ELedge -> reg[le] : he -> ELedge -> reg[re]);
}

/*=============== rightreg ===================*/
struct Site *rightreg(he)
struct Halfedge *he;
{
  if(he -> ELedge == (struct Edge *)NULL) return(bottomsite);
  return( he -> ELpm == le ? 
	 he -> ELedge -> reg[re] : he -> ELedge -> reg[le]);
}

/*===================== geominit ================*/
void geominit()
{
  struct Edge e;
  float sn;
  
  freeinit(&efl, sizeof e);
  nvertices = 0;
  nedges = 0;
  sn = nsites+4;
  sqrt_nsites = sqrt(sn);
  deltay = ymax - ymin;
  deltax = xmax - xmin;
}

/*===================== bisect ==================*/
struct Edge *bisect(s1,s2,linea,lineb,linec,numlines)
struct	Site *s1,*s2;
float linea[];
float lineb[];
float linec[];
int *numlines;
{
  float dx,dy,adx,ady;
  struct Edge *newedge;
  
  newedge = (struct Edge *) getfree(&efl);
  
  newedge -> reg[0] = s1;
  newedge -> reg[1] = s2;
  ref(s1); 
  ref(s2);
  newedge -> ep[0] = (struct Site *) NULL;
  newedge -> ep[1] = (struct Site *) NULL;
  
  dx = s2->coord.x - s1->coord.x;
  dy = s2->coord.y - s1->coord.y;
  adx = dx>0 ? dx : -dx;
  ady = dy>0 ? dy : -dy;
  newedge -> c = s1->coord.x * dx + s1->coord.y * dy + (dx*dx + dy*dy)*0.5;
  if (adx>ady){	
    newedge -> a = 1.0; newedge -> b = dy/dx; newedge -> c /= dx;
  }
  else{	
    newedge -> b = 1.0; newedge -> a = dx/dy; newedge -> c /= dy;
  }

  newedge -> edgenbr = nedges;

  /* Write out location of the bisecting line, part of Voronoi construct.*/
  out_bisector(newedge,linea,lineb,linec,numlines);

  nedges += 1;
  return(newedge);
}

/*======================== intersect ==============*/
struct Site *intersect(el1, el2)
struct Halfedge *el1, *el2;
{
  struct	Edge *e1,*e2, *e;
  struct  Halfedge *el;
  float d, xint, yint;
  int right_of_site;
  struct Site *v;
  
  e1 = el1 -> ELedge;
  e2 = el2 -> ELedge;
  if(e1 == (struct Edge*)NULL || e2 == (struct Edge*)NULL) 
    return ((struct Site *) NULL);
  if (e1->reg[1] == e2->reg[1]) return ((struct Site *) NULL);
  
  d = e1->a * e2->b - e1->b * e2->a;
  if (-1.0e-10<d && d<1.0e-10) return ((struct Site *) NULL);
  
  xint = (e1->c*e2->b - e2->c*e1->b)/d;
  yint = (e2->c*e1->a - e1->c*e2->a)/d;
  
  if( (e1->reg[1]->coord.y < e2->reg[1]->coord.y) ||
     (e1->reg[1]->coord.y == e2->reg[1]->coord.y &&
      e1->reg[1]->coord.x < e2->reg[1]->coord.x) ){	
    el = el1; e = e1;
  }
  else{	
    el = el2; e = e2;
  }
  right_of_site = xint >= e -> reg[1] -> coord.x;
  if ((right_of_site && el -> ELpm == le) ||
      (!right_of_site && el -> ELpm == re)) return ((struct Site *) NULL);
  
  v = (struct Site *) getfree(&sfl);
  v -> refcnt = 0;
  v -> coord.x = xint;
  v -> coord.y = yint;
  return(v);
}

/*====================== right_of ================*/
/* returns 1 if p is to right of halfedge e */
int right_of(el, p)
struct Halfedge *el;
struct Point *p;
{
  struct Edge *e;
  struct Site *topsite;
  int right_of_site, above, fast;
  float dxp, dyp, dxs, t1, t2, t3, yl;
  
  e = el -> ELedge;
  topsite = e -> reg[1];
  right_of_site = p -> x > topsite -> coord.x;
  if(right_of_site && el -> ELpm == le) return(1);
  if(!right_of_site && el -> ELpm == re) return (0);
  
  if (e->a == 1.0){
    dyp = p->y - topsite->coord.y;
    dxp = p->x - topsite->coord.x;
    fast = 0;
    if ((!right_of_site &e->b<0.0) | (right_of_site&e->b>=0.0) ){
      above = dyp>= e->b*dxp;	
      fast = above;
    }
    else{
      above = p->x + p->y*e->b > e-> c;
      if(e->b<0.0) above = !above;
      if (!above) fast = 1;
    };
    if (!fast){
      dxs = topsite->coord.x - (e->reg[0])->coord.x;
      above = e->b * (dxp*dxp - dyp*dyp) <
	dxs*dyp*(1.0+2.0*dxp/dxs + e->b*e->b);
      if(e->b<0.0) above = !above;
    };
  }
  else{  /*e->b==1.0 */
    yl = e->c - e->a*p->x;
    t1 = p->y - yl;
    t2 = p->x - topsite->coord.x;
    t3 = yl - topsite->coord.y;
    above = t1*t1 > t2*t2 + t3*t3;
  };
  return (el->ELpm==le ? above : !above);
}

/*=================== endpoint ==================*/
endpoint(e, lr, s,cjoleft,cjoright,num_ep)
struct Edge *e;
int	lr;
struct Site *s;
int cjoleft[];
int cjoright[];
int *num_ep;
{
  e -> ep[lr] = s;
  ref(s);
  if(e -> ep[re-lr]== (struct Site *) NULL) return;
  out_ep(e,cjoleft,cjoright,num_ep);
  deref(e->reg[le]);
  deref(e->reg[re]);
  makefree(e, &efl);
}

/*==================== dist =======================*/
float dist(s,t)
struct Site *s,*t;
{
  float dx,dy;
  dx = s->coord.x - t->coord.x;
  dy = s->coord.y - t->coord.y;
  return(sqrt(dx*dx + dy*dy));
}

/*===================== makevertex ===================*/
int makevertex(v,vertx,verty,num_vert)
struct Site *v;
float vertx[];
float verty[];
int *num_vert;
{
  v -> sitenbr = nvertices;
  nvertices += 1;
  out_vertex(v,vertx,verty,num_vert);
}

/*================== deref ===============*/
deref(v)
struct	Site *v;
{
  v -> refcnt -= 1;
  if (v -> refcnt == 0 ) makefree(v, &sfl);
}

/*=================== ref ==============*/
ref(v)
struct Site *v;
{
  v -> refcnt += 1;
}

/*================ PQinsert ===============*/
PQinsert(he, v, offset)
struct Halfedge *he;
struct Site *v;
float 	offset;
{
  struct Halfedge *last, *next;
  
  he -> vertex = v;
  ref(v);
  he -> ystar = v -> coord.y + offset;
  last = &PQhash[PQbucket(he)];
  while ((next = last -> PQnext) != (struct Halfedge *) NULL &&
	 (he -> ystar  > next -> ystar  ||
	  (he -> ystar == next -> ystar && v -> coord.x > next->vertex->coord.x))){
    last = next;
  }
  he -> PQnext = last -> PQnext; 
  last -> PQnext = he;
  PQcount += 1;
}

/*============== PQdelete ===========*/
PQdelete(he)
struct Halfedge *he;
{
  struct Halfedge *last;
  
  if(he ->  vertex != (struct Site *) NULL){
    last = &PQhash[PQbucket(he)];
    while (last -> PQnext != he) last = last -> PQnext;
    last -> PQnext = he -> PQnext;
    PQcount -= 1;
    deref(he -> vertex);
    he -> vertex = (struct Site *) NULL;
  };
}

/*=============== PQbucket =============*/
int PQbucket(he)
struct Halfedge *he;
{
  int bucket;
  
  bucket = (he->ystar - ymin)/deltay * PQhashsize;
  if (bucket<0) bucket = 0;
  if (bucket>=PQhashsize) bucket = PQhashsize-1 ;
  if (bucket < PQmin) PQmin = bucket;
  return(bucket);
}


/*============== PQempty ==============*/
int PQempty()
{
  return(PQcount==0);
}

/*============== PQ_min ===============*/
struct Point PQ_min()
{
  struct Point answer;
  
  while(PQhash[PQmin].PQnext == (struct Halfedge *)NULL) {PQmin += 1;};
  answer.x = PQhash[PQmin].PQnext -> vertex -> coord.x;
  answer.y = PQhash[PQmin].PQnext -> ystar;
  return (answer);
}

/*================ PQextractmin ============*/
struct Halfedge *PQextractmin()
{
  struct Halfedge *curr;
  
  curr = PQhash[PQmin].PQnext;
  PQhash[PQmin].PQnext = curr -> PQnext;
  PQcount -= 1;
  return(curr);
}

/*===================== PQinitialize ===========*/
PQinitialize()
{
  int i; 
  
  PQcount = 0;
  PQmin = 0;
  PQhashsize = 4 * sqrt_nsites;
  PQhash = (struct Halfedge *) myalloc(PQhashsize * sizeof *PQhash);
  for(i=0; i<PQhashsize; i+=1) PQhash[i].PQnext = (struct Halfedge *)NULL;
}

/*================ freeinit ===========*/
freeinit(fl, size)
struct	Freelist *fl;
int	size;
{
  fl -> head = (struct Freenode *) NULL;
  fl -> nodesize = size;
}

/*========== getfree =============*/
char *getfree(fl)
struct	Freelist *fl;
{
  int i; struct Freenode *t;
  if(fl->head == (struct Freenode *) NULL){	
    t =  (struct Freenode *) myalloc(sqrt_nsites * fl->nodesize);
    for(i=0; i<sqrt_nsites; i+=1) 	
      makefree((struct Freenode *)((char *)t+i*fl->nodesize), fl);
  };
  t = fl -> head;
  fl -> head = (fl -> head) -> nextfree;
  return((char *)t);
}

/*=========== makefree ============*/
makefree(curr,fl)
struct Freenode *curr;
struct Freelist *fl;
{
  curr -> nextfree = fl -> head;
  fl -> head = curr;
}

/*============ total_alloc =============*/
int total_alloc;
char *myalloc(n)
unsigned n;
{
  char *t;
  if ((t=malloc(n)) == (char *) 0){    
    fprintf(stderr,"Insufficient memory processing site %d (%d bytes in use)\n",
	    siteidx, total_alloc);
    exit(1);
  };
  total_alloc += n;
  return(t);
}
#endif

/*-----------------------------fvector---------------------------------------*/
float *fvector(int nl, int nh)
/* allocate a float vector with subscript range v[nl..nh] */
{
float *v;
v=(float *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(float)));
if (!v) nrerror("allocation failure in fvector()");
return v-nl+NR_END;
}

void free_fvector(float *v, int nl, int nh)
/* free a float vector allocated with fvector() */
{
free((FREE_ARG) (v+nl-NR_END));
}
/*---------------------------------------------------------------------------*/

/*-----------------------------dvector---------------------------------------*/
double *dvector(int nl, int nh)
/* allocate a double vector with subscript range v[nl..nh] */
{
double *v;
v=(double *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(double)));
if (!v) nrerror("allocation failure in fvector()");
return v-nl+NR_END;
}

void free_dvector(double *v, int nl, int nh)
/* free a double vector allocated with fvector() */
{
free((FREE_ARG) (v+nl-NR_END));
}
/*---------------------------------------------------------------------------*/

/*---------------------------fmatrix---------------------------------------*/
float **fmatrix(int nrl, int nrh, int ncl, int nch)
/* allocate a float matrix with subscript range m[nrl..nrh][ncl..nch] */
{
int i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
float **m;
/* allocate pointers to rows */
m=(float **) malloc((size_t)((nrow+NR_END)*sizeof(float*)));
if (!m) nrerror("allocation failure 1 in matrix()");
m += NR_END;
m -= nrl;
/* allocate rows and set pointers to them */
m[nrl]=(float *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(float)));
if (!m[nrl]) nrerror("allocation failure 2 in matrix()");
m[nrl] += NR_END;
m[nrl] -= ncl;
for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;
/* return pointer to array of pointers to rows */
return m;
}

/*---------------------------------------------------------------------------*/

/*-----------------------------free_fmatrix----------------------------------*/
void free_fmatrix(float **m, int nrl, int nrh, int ncl, int nch)
/* free a ULL matrix allocated by llmatrix() */
{
free((FREE_ARG) (m[nrl]+ncl-NR_END));
free((FREE_ARG) (m+nrl-NR_END));
}

/*----------------------------------------------------------------------------*/


/*-----------------------------gftolineplas----------------------------------*/
void gftolineplas(nV,current,jeffrho,realrho,myid,num_procs,fxmat,fymat,linenow,id_string)
int nV;
float current;
float jeffrho;
float realrho;
int myid;
int num_procs;
float **fxmat;
float **fymat;
int linenow;
char id_string[];
{ 
	FILE *infile;
	FILE *inyfile;
	FILE *outfile;
	FILE *diagfile;
	FILE *difffile;
	char filename[20],addendum[6];
	double *sumtraj;
	double *sumtrajy;
	double meantraj;
	int poscount[HALFNUMBINS];
	int negcount[HALFNUMBINS];
	char trash[25];
	float check;
	double sumnegAnot[HALFNUMBINS];
	double lognprat[HALFNUMBINS],meannAnot[HALFNUMBINS];
	double lognprstol[HALFNUMBINS*TOTALTAU],meannAnstol[HALFNUMBINS*TOTALTAU];
	double pcstol[HALFNUMBINS*TOTALTAU];
	int tottime;
	double taureal;
	int i,j,tau,k,l;
	int storedavgers,bindx;
	double totd;
	double meand;
	double binsize,binloc,maxdist;
	int stol;
	double slope,covariance,chisq;
	double percentmotion;
	int curhun,rhohun;
	double diffcount,xdispcmsq,ydispcmsq,meanxsqcm,meanysqcm;
	double runxsqcm,runysqcm,stdxsqcm,stdysqcm,deltaxsq,deltaysq;
	int idnow;
	double resultout[RESULTFIELDS];
	MPI_Status status;
	int totsize,numoutliers;
	float *maxguys;
	float *minguys;
	int breaker;

	sumtraj=dvector(0,nV);
	sumtrajy=dvector(0,nV);

	totsize=linenow*nV;
	// numoutliers=(int)(totsize*OUTLIERFRAC);
	// maxguys=fvector(0,numoutliers);
	// minguys=fvector(0,numoutliers);

/* open output file */
	curhun=(int)((current-0.005)*100);
	rhohun=(int)((jeffrho-0.005)*100);
	sprintf(addendum,"v%dc%d",rhohun,curhun);
	strcpy(filename,RESULTPREPEND);
	strcat(filename,"gfftl");
	strcat(filename,id_string);
	strcat(filename,addendum);
	strcat(filename,".dat");
	outfile=fopen(filename,"wb");
	strcpy(filename,RESULTPREPEND);
	strcat(filename,"gfftldiag");
	strcat(filename,id_string);
	strcat(filename,addendum);
	diagfile=fopen(filename,"wb");
	strcpy(filename,RESULTPREPEND);
	strcat(filename,"diff");
	strcat(filename,id_string);
	strcat(filename,addendum);
	strcat(filename,"sq");
	difffile=fopen(filename,"wb");

	/* fprintf(diagfile,"current=%f\n",current); */
	totd=0.0;
	storedavgers=0;
	maxdist=0.0;

for(l=0;l<linenow;l++){
	for(j=0;j<nV;j++){
		totd+=(double)fxmat[l][j]*DECIFACXTIMESTEP;
		if(fabsf(fxmat[l][j]*DECIFACXTIMESTEP)>(float)maxdist) maxdist=(double)fabsf(fxmat[l][j]*DECIFACXTIMESTEP); /* BRAIN */
		storedavgers++;
	}
}
	meand=totd/(double)storedavgers;
	binsize=maxdist/(double)HALFNUMBINS;
	percentmotion=meand/(current*DECIFACXTIMESTEP);
	/* fprintf(diagfile,"Mean absolute displacement = %f\n",meand);  */
	/* fprintf(diagfile,"Bin size = %f\n",binsize); */

	/* gsl_sort_float_largest(maxguys,(size_t)numoutliers,*fxmat,(size_t)1,(size_t)totsize); */

	stol=0;
	for(j=0;j<(HALFNUMBINS*TOTALTAU);j++){
		lognprstol[j]=0.0;
		meannAnstol[j]=0.0;
		pcstol[j]=0.0;
	}

for(i=0;i<TOTALTAU;i++){

        tau=(int)TAUSTEPS*i+1;

	diffcount=0;
	meanxsqcm=0.0;
	meanysqcm=0.0;
	runxsqcm=0.0;
	runysqcm=0.0;

        for(j=0;j<nV;j++){
                sumtraj[j]=0.0;
		sumtrajy[j]=0.0;
        }

	for(k=0;k<HALFNUMBINS;k++){
		poscount[k]=0;
		negcount[k]=0;
		sumnegAnot[k]=0;
		meannAnot[k]=0;
		lognprat[k]=0;
	}

        taureal=(double)tau*DECIFACXTIMESTEP;
        /* fprintf(diagfile,"taureal is %f\n",taureal); */

	tottime=1;
        for(l=0;l<linenow;l++){
                for(j=0;j<nV;j++){
                        sumtraj[j]+=fxmat[l][j]*DECIFACXTIMESTEP;
			sumtrajy[j]+=fymat[l][j]*DECIFACXTIMESTEP;
                        if(!(tottime%tau)&&tottime){
                        /*        if(!j){
                                        meantraj=0.0;
                                        for(k=0;k<nV;k++) meantraj+=sumtraj[k];
                                        meantraj/=(float)nV;
                                }    	*/
                                xdispcmsq=(sumtraj[j]-(meand*tau))*(sumtraj[j]-(meand*tau));
				ydispcmsq=(sumtrajy[j])*(sumtrajy[j]);
				diffcount++;
				deltaxsq=xdispcmsq-meanxsqcm;
				deltaysq=ydispcmsq-meanysqcm;
				meanxsqcm+=deltaxsq/diffcount;
				meanysqcm+=deltaysq/diffcount;
				runxsqcm+=deltaxsq*(xdispcmsq-meanxsqcm);
                                runysqcm+=deltaysq*(ydispcmsq-meanysqcm);
			/* 	sumtraj[j]-=current*tau*DECIFACXTIMESTEP; */  /* BRAIN */
				bindx=(int)fabs((double)sumtraj[j]/((double)tau*binsize)); /* Ok dog binsize is in dist tho the real bins are in power */
				if(bindx>=HALFNUMBINS){
					printf("Error JeffI");
					bindx=HALFNUMBINS-1;
				}
                                if(sumtraj[j]>0.0){   /* BRAIN */
                                        poscount[bindx]+=1;
                                        sumnegAnot[bindx]+=sumtraj[j]*current/taureal;
                                }
                                if(sumtraj[j]<0.0) negcount[bindx]+=1;   /* BRAIN */
                                sumtraj[j]=0.0;
				sumtrajy[j]=0.0;
                        }
                }
                tottime++;
        }

	breaker=nV*(int)(((int)(totsize/nV))/tau);
	/*	numoutliers=0;
	for(j=0;j<HALFNUMBINS;j++) numoutliers+=poscount[j]+negcount[j];
	if(!(numoutliers==breaker)) printf("Error JeffII");  Checked,seems to work. */

        for(k=0;k<HALFNUMBINS;k++){
                binloc=(((double)k)+0.5)*binsize*current/DECIFACXTIMESTEP;
		fprintf(diagfile,"%f %f %d %d\n",taureal,binloc,negcount[k],poscount[k]);
		breaker-=negcount[k]+poscount[k];
		if(breaker<1) break;
		if(poscount[k]==0||negcount[k]==0) continue;
                lognprat[k]=log((double)negcount[k]/(double)poscount[k]);
                meannAnot[k]=sumnegAnot[k]/(double)poscount[k];
		fprintf(outfile,"%f %f %f %f\n",binloc,lognprat[k],meannAnot[k],taureal);
		lognprstol[stol]=lognprat[k];
		meannAnstol[stol]=meannAnot[k];
		pcstol[stol]=(double)poscount[k];
		stol++;
        }

	stdxsqcm=sqrt(runxsqcm/diffcount);
	stdysqcm=sqrt(runysqcm/diffcount);
	fprintf(difffile,"%f %'.8f %'.8f %'.8f %'.8f\n",taureal,meanxsqcm,stdxsqcm,meanysqcm,stdysqcm);

	fflush(difffile);
        fflush(outfile);

}

	gsl_fit_wmul(lognprstol,(size_t)1,pcstol,(size_t)1,meannAnstol,(size_t)1,(size_t)stol,&slope,&covariance,&chisq);

  if(!(MASTER_RANK==myid)){
        resultout[0]=(double)realrho;
        resultout[1]=(double)current;
        resultout[2]=percentmotion;
        resultout[3]=slope;
	resultout[4]=covariance;
	resultout[5]=chisq;
	resultout[6]=chisq/((double)stol);
        MPI_Send(&resultout, RESULTFIELDS, MPI_DOUBLE, MASTER_RANK, RESULTTAG,MPI_COMM_WORLD);
  }

  if(MASTER_RANK==myid){
        fprintf(resultfile,"%f %f %f %f %f %f %f\n",realrho,current,percentmotion,slope,covariance,chisq,chisq/stol);
	for(idnow=1;idnow<num_procs;idnow++){
		MPI_Recv(&resultout,RESULTFIELDS,MPI_DOUBLE,MPI_ANY_SOURCE,RESULTTAG,MPI_COMM_WORLD,&status);
                fprintf(resultfile,"%f %f %f %f %f %f %f \n",resultout[0],resultout[1],resultout[2],resultout[3],resultout[4],resultout[5],resultout[6]);
        }
        fflush(resultfile);
  }


	free_dvector(sumtraj,0,nV);
	free_dvector(sumtrajy,0,nV);
	// free_fvector(maxguys,0,numoutliers);
	// free_fvector(minguys,0,numoutliers);

	fclose(outfile);
	fclose(diagfile);
	fclose(difffile);

}
