#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define HITPTS 180  /* must equal number of columns in infile */
#define TOTALTAU 150
#define TAUSTEPS 2
#define DECIFACXTIMESTEP 0.1
#define HALFNUMBINS 50

main(void)
{ 
	FILE *infile;
	FILE *outfile;
	FILE *paramfile;
	FILE *diagfile;
	char filename[20],id_string[3];
	float intake[HITPTS];
	double sumtraj[HITPTS],meantraj;
	int poscount[HALFNUMBINS];
	int negcount[HALFNUMBINS];
	float current;
	char trash[25];
	float check;
	double sumnegAnot[HALFNUMBINS];
	double lognprat[HALFNUMBINS],meannAnot[HALFNUMBINS];
	int tottime;
	double taureal;
	int i,j,tau,k;
	int storedavgers,bindx;
	float totd;
	double meand;
	double binsize,maxdist;

/* open param file */
	if ((paramfile=fopen("Pco0","r"))==NULL){
		printf("Param file missing \n");
		exit(1);
	}

	for(k=0;k<11;k++) fscanf(paramfile,"%s %s",trash,trash);
	fscanf(paramfile,"%s %f",trash,&current);
	fprintf(diagfile,"current=%f\n",current);
	fscanf(paramfile,"%s %f",trash,&check);
	if(check>0.0){
		printf("Current not stationary\n");
		exit(1);
	}

/* open output file */
	strcpy(filename,"gfntf.dat");
	outfile=fopen(filename,"wb");
	strcpy(filename,"gfntfdiag");
	diagfile=fopen(filename,"wb");

	totd=0.0;
	storedavgers=0;
	maxdist=0.0;

	/* open input file */
	if((infile=fopen("fxtest.n0","r"))==NULL){
	printf("File missing\n");
	exit(1);
	}

while(!feof(infile)){
	for(j=0;j<HITPTS;j++){
		fscanf(infile,"%f",&intake[j]);
		totd+=intake[j]*DECIFACXTIMESTEP;
		if(fabsf(intake[j]*DECIFACXTIMESTEP-current*DECIFACXTIMESTEP)>(float)maxdist) maxdist=(double)fabsf(intake[j]*DECIFACXTIMESTEP-current*DECIFACXTIMESTEP);
		storedavgers++;
	}
}
	meand=(double)totd/(double)storedavgers;
	binsize=maxdist/(double)HALFNUMBINS;
	fprintf(diagfile,"Mean displacement = %f\n",meand);
	fprintf(diagfile,"Bin size = %f\n",binsize); 
	fclose(infile);

for(i=0;i<TOTALTAU;i++){

        tau=(int)TAUSTEPS*i+1;

        for(j=0;j<HITPTS;j++){
                sumtraj[j]=0.0;
        }

	for(k=0;k<HALFNUMBINS;k++){
		poscount[k]=0;
		negcount[k]=0;
		sumnegAnot[k]=0;
		meannAnot[k]=0;
		lognprat[k]=0;
	}

        /* open input file*/
        if ((infile=fopen("fxtest.n0","r"))==NULL) {
        printf("File missing\n");
        exit(1);
        }

	tottime=1;
        while(!feof(infile)){
                for(j=0;j<HITPTS;j++){
                        fscanf(infile,"%f",&intake[j]);
                        sumtraj[j]+=intake[j]*DECIFACXTIMESTEP;
                        if(!(tottime%tau)&&tottime){
                        /*        if(!j){
                                        meantraj=0.0;
                                        for(k=0;k<HITPTS;k++) meantraj+=sumtraj[k];
                                        meantraj/=(float)HITPTS;
                                }
                                sumtraj[j]-=meantraj;  */
				sumtraj[j]-=current*tau*DECIFACXTIMESTEP;
				bindx=(int)fabs((double)sumtraj[j]/((double)tau*binsize));
				if(bindx>=HALFNUMBINS) bindx=HALFNUMBINS-1;
                                if(sumtraj[j]<0.0){
                                        poscount[bindx]+=1;
                                        sumnegAnot[bindx]+=sumtraj[j]*current;
                                }
                                if(sumtraj[j]>0.0) negcount[bindx]+=1;
                                sumtraj[j]=0.0;
                        }
                }
                tottime++;
        }

        taureal=(double)tau*DECIFACXTIMESTEP;
	fprintf(diagfile,"taureal is %f\n",taureal);

        for(k=0;k<HALFNUMBINS;k++){
		fprintf(diagfile,"bin %d negcount is %d poscount is %d\n",k,negcount[k],poscount[k]);
		if(poscount[k]==0||negcount[k]==0) break;
                lognprat[k]=log((double)negcount[k]/(double)poscount[k]);
                meannAnot[k]=sumnegAnot[k]/(double)poscount[k];
		fprintf(outfile,"%f %f\n",lognprat[k],meannAnot[k]);
        }
        fclose(infile);

        fflush(outfile);

}

	fclose(outfile);

}
