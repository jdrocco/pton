#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_fit.h>

#define HITPTS 180  /* must equal number of columns in infile */
#define TOTALTAU 250
#define TAUSTEPS 1
#define DECIFACXTIMESTEP 0.1
#define HALFNUMBINS 50

main(void)
{ 
	FILE *infile;
	FILE *outfile;
	FILE *paramfile;
	FILE *diagfile;
	FILE *resultfile;
	char filename[20],id_string[3];
	float intake[HITPTS];
	double sumjay[HITPTS],meantraj;
	double jaystor[TOTALTAU];
	int storjs[TOTALTAU];
	int poscount[HALFNUMBINS];
	int negcount[HALFNUMBINS];
	float current;
	char trash[25];
	float check;
	double sumnegAnot[HALFNUMBINS];
	double lognprat[HALFNUMBINS],meannAnot[HALFNUMBINS];
	double lognprstol[HALFNUMBINS*TOTALTAU],meannAnstol[HALFNUMBINS*TOTALTAU];
	double pcstol[HALFNUMBINS*TOTALTAU];
	int tottime;
	double taureal;
	int i,j,tau,k;
	int storedavgers,bindx;
	float totd;
	double meand;
	double binsize,maxdist;
	int stol;
	double slope,covariance,chisq;
	double percentmotion;

/* open param file */
	if ((paramfile=fopen("Pco0","r"))==NULL){
		printf("Param file missing \n");
		exit(1);
	}

	for(k=0;k<11;k++) fscanf(paramfile,"%s %s",trash,trash);
	fscanf(paramfile,"%s %f",trash,&current);
	fscanf(paramfile,"%s %f",trash,&check);
	if(check>0.0){
		printf("Current not stationary\n");
		exit(1);
	}

/* open output file */
	strcpy(filename,"gfftl.dat");
	outfile=fopen(filename,"wb");
	strcpy(filename,"gfftldiag");
	diagfile=fopen(filename,"wb");
	strcpy(filename,"gfftlres.dat");
	resultfile=fopen(filename,"wb");

	fprintf(diagfile,"current=%f\n",current);
	totd=0.0;
	storedavgers=0;
	maxdist=0.0;

	/* open input file */
	if((infile=fopen("fxtest.n0","r"))==NULL){
	printf("File missing\n");
	exit(1);
	}

while(!feof(infile)){
	for(j=0;j<HITPTS;j++){
		fscanf(infile,"%f",&intake[j]);
		totd+=intake[j]*DECIFACXTIMESTEP;
		if(fabsf(intake[j]*DECIFACXTIMESTEP-current*DECIFACXTIMESTEP)>(float)maxdist) maxdist=(double)fabsf(intake[j]*DECIFACXTIMESTEP-current*DECIFACXTIMESTEP);
		storedavgers++;
	}
}
	meand=(double)totd/(double)storedavgers;
	binsize=maxdist/(double)HALFNUMBINS;
	percentmotion=meand/(current*DECIFACXTIMESTEP);
	fprintf(diagfile,"Mean displacement = %f\n",meand);
	fprintf(diagfile,"Bin size = %f\n",binsize); 
	fclose(infile);

stol=0;
for(j=0;j<(HALFNUMBINS*TOTALTAU);j++){
	lognprstol[j]=0.0;
	meannAnstol[j]=0.0;
	pcstol[j]=0.0;
}

for(i=0;i<TOTALTAU;i++){

        tau=(int)TAUSTEPS*i+1;
	jaystor[i]=0.0;
	storjs[i]=0;
	
        taureal=(double)tau*DECIFACXTIMESTEP;
        fprintf(diagfile,"taureal is %f\n",taureal);

        for(j=0;j<HITPTS;j++){
                sumjay[j]=0.0;
        }

	for(k=0;k<HALFNUMBINS;k++){
		poscount[k]=0;
		negcount[k]=0;
		sumnegAnot[k]=0;
		meannAnot[k]=0;
		lognprat[k]=0;
	}

        /* open input file*/
        if ((infile=fopen("fxtest.n0","r"))==NULL) {
        printf("File missing\n");
        exit(1);
        }

	tottime=1;
        while(!feof(infile)){
                for(j=0;j<HITPTS;j++){
                        fscanf(infile,"%f",&intake[j]);
                        sumjay[j]+=intake[j]*current*DECIFACXTIMESTEP;  /* So intake is the vel, current is the F, DFTS is the dt */
                        if(!(tottime%tau)&&tottime){
                        /*        if(!j){
                                        meantraj=0.0;
                                        for(k=0;k<HITPTS;k++) meantraj+=sumjay[k];
                                        meantraj/=(float)HITPTS;
                                }
                                sumjay[j]-=meantraj;  */
		/*		sumjay[j]-=current*tau*DECIFACXTIMESTEP; */ /* Eliminando this because it doesn't fit maite */
				jaystor[i]+=sumjay[j]/taureal;
				storjs[i]+=1;
				bindx=(int)fabs((double)sumjay[j]/((double)tau*binsize));
				if(bindx>=HALFNUMBINS) bindx=HALFNUMBINS-1;
                                if(sumjay[j]<0.0){
                                        poscount[bindx]+=1;
                                        sumnegAnot[bindx]+=sumjay[j]*current;
                                }
                                if(sumjay[j]>0.0) negcount[bindx]+=1;
                                sumjay[j]=0.0;
                        }
                }
                tottime++;
        }

        for(k=0;k<HALFNUMBINS;k++){
		fprintf(diagfile,"bin %d negcount is %d poscount is %d\n",k,negcount[k],poscount[k]);
		if(poscount[k]==0||negcount[k]==0) break;
                lognprat[k]=log((double)negcount[k]/(double)poscount[k]);
                meannAnot[k]=sumnegAnot[k]/(double)poscount[k];
		fprintf(outfile,"%f %f\n",lognprat[k],meannAnot[k]);
		lognprstol[stol]=lognprat[k];
		meannAnstol[stol]=meannAnot[k];
		pcstol[stol]=(double)poscount[k];
		stol++;
        }
        fclose(infile);

        fflush(outfile);

}

	gsl_fit_wmul(lognprstol,(size_t)1,pcstol,(size_t)1,meannAnstol,(size_t)1,(size_t)stol,&slope,&covariance,&chisq);
	fprintf(resultfile,"%f %f %f %f %f %f\n",percentmotion,slope,covariance,chisq,chisq/stol,current);

	fclose(resultfile);
	fclose(outfile);

}
