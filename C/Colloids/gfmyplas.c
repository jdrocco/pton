#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define HITPTS 180  /* must equal number of columns in infile */
#define TOTALTAU 400
#define TAUSTEPS 1
#define DECIFACXTIMESTEP 0.1
#define INVKBTGUESS 10.0
#define INVKBTMIN 0.00001
#define INVKBTMAX 500000.0
#define PRECISION 0.00001
#define MAXITER 20
#define TAUSTOAVG 5

main(void)
{ 
	FILE *infile;
	FILE *outfile;
	FILE *paramfile;
	char filename[20],id_string[3];
	float intake[HITPTS];
	float sumtraj[HITPTS],meantraj;
	int poscount[HITPTS];
	int negcount[HITPTS];
	int negtot,postot;
	int startavgflag,finalfitter,uselast;
	float current;
	char trash[25];
	float check;
	float spenatot,spenatotplus,spenaoverposplus;
	float sumposexpnegA[HITPTS],sumposexpnegAplus[HITPTS];
	float difference,derivative,invkbtsum,useikbt;
	int tottime=0;
	float negposratio,spenaoverpos,taureal,inversekbt;
	int i,j,tau,doneflag,optcount,k,ikbtsdivisor;

/* open param file */
	if ((paramfile=fopen("Pco0","r"))==NULL){
		printf("Param file missing \n");
		exit(1);
	}

	for(k=0;k<11;k++) fscanf(paramfile,"%s %s",trash,trash);
	fscanf(paramfile,"%s %f",trash,&current);
	printf("current=%f\n",current);
	fscanf(paramfile,"%s %f",trash,&check);
	if(check>0.0){
		printf("Current not stationary\n");
		exit(1);
	}

/* open output file */
	strcpy(filename,"gfoff.dat");
	outfile=fopen(filename,"wb");

        inversekbt=INVKBTGUESS;
	invkbtsum=0.0;
	ikbtsdivisor=TAUSTOAVG;
	startavgflag=0;

for(i=TOTALTAU;i>0;i--){
	
	if(TAUSTOAVG==startavgflag){
		finalfitter=i;
		break;
	}
	optcount=0;
	tau=(int)TAUSTEPS*i+1;
	
	do{
	for(j=0;j<HITPTS;j++){
		poscount[j]=0;
		negcount[j]=0;
		sumtraj[j]=0.0;
		sumposexpnegA[j]=0.0;
		sumposexpnegAplus[j]=0.0;
	}

	/* open input file*/
        if ((infile=fopen("fxtest.n0","r"))==NULL) {
        printf("File missing\n");
        exit(1);
        }

	while(!feof(infile)){
		for(j=0;j<HITPTS;j++){
			fscanf(infile,"%f",&intake[j]);
			sumtraj[j]+=intake[j]*DECIFACXTIMESTEP;
			if(!(tottime%tau)){
		/*		if(!j){
					meantraj=0.0;
					for(k=0;k<HITPTS;k++) meantraj+=sumtraj[k];
					meantraj/=(float)HITPTS;
				}
				sumtraj[j]-=meantraj;   */
				if(sumtraj[j]>0.0){ 
					poscount[j]+=1;
					sumposexpnegA[j]+=exp(-sumtraj[j]*DECIFACXTIMESTEP*current*inversekbt);
					sumposexpnegAplus[j]+=exp(-sumtraj[j]*DECIFACXTIMESTEP*current*(inversekbt+0.005));
				}
				if(sumtraj[j]<0.0) negcount[j]+=1;
				sumtraj[j]=0.0;
			}
		}
		tottime++;
	}

	taureal=(float)tau*DECIFACXTIMESTEP;

	negtot=0;
	postot=0;
	spenatot=0.0;
	spenatotplus=0.0;
	for(j=0;j<HITPTS;j++){
		negtot+=negcount[j];
		postot+=poscount[j];
		spenatot+=sumposexpnegA[j];
		spenatotplus+=sumposexpnegAplus[j];
        }
	fclose(infile);
        negposratio=(float)negtot/(float)postot;
	if(!optcount){
		if(((negposratio>(4*PRECISION))&&(!startavgflag))||(startavgflag&&(startavgflag<TAUSTOAVG)))
			startavgflag++;
		else continue;
	}
        spenaoverpos=spenatot/(float)postot;
	spenaoverposplus=spenatotplus/(float)postot;
	difference=negposratio-spenaoverpos;
	doneflag=!(fabs(difference)>PRECISION);
	if(!doneflag){
		derivative=200.0*(spenaoverposplus-spenaoverpos);
		inversekbt+=(difference/derivative);
		}
	if(inversekbt<INVKBTMIN) inversekbt=INVKBTMIN;
	if(inversekbt>INVKBTMAX) inversekbt=INVKBTMAX;
	optcount++;
	}while((!doneflag)&&(optcount<MAXITER));

	if(!(startavgflag)) continue;

	if(!(optcount<MAXITER)){
		ikbtsdivisor--;
		printf("Optimization incomplete for tau = %f",taureal);
	}
	else	invkbtsum+=inversekbt;

}
	
	useikbt=invkbtsum/ikbtsdivisor;
	if(finalfitter<(TOTALTAU-4*TAUSTOAVG))
		uselast=finalfitter+4*TAUSTOAVG;
	else uselast=TOTALTAU;

for(i=0;i<uselast;i++){

        tau=(int)TAUSTEPS*i+1;

        for(j=0;j<HITPTS;j++){
                poscount[j]=0;
                negcount[j]=0;
                sumtraj[j]=0.0;
                sumposexpnegA[j]=0.0;
        }

        /* open input file*/
        if ((infile=fopen("fxtest.n0","r"))==NULL) {
        printf("File missing\n");
        exit(1);
        }

        while(!feof(infile)){
                for(j=0;j<HITPTS;j++){
                        fscanf(infile,"%f",&intake[j]);
                        sumtraj[j]+=intake[j]*DECIFACXTIMESTEP;
                        if(!(tottime%tau)){
                        /*        if(!j){
                                        meantraj=0.0;
                                        for(k=0;k<HITPTS;k++) meantraj+=sumtraj[k];
                                        meantraj/=(float)HITPTS;
                                }
                                sumtraj[j]-=meantraj;  */
                                if(sumtraj[j]>0.0){
                                        poscount[j]+=1;
                                        sumposexpnegA[j]+=exp(-sumtraj[j]*DECIFACXTIMESTEP*current*useikbt);
                                }
                                if(sumtraj[j]<0.0) negcount[j]+=1;
                                sumtraj[j]=0.0;
                        }
                }
                tottime++;
        }

        taureal=(float)tau*DECIFACXTIMESTEP;

        negtot=0;
        postot=0;
        spenatot=0.0;
        for(j=0;j<HITPTS;j++){
                negtot+=negcount[j];
                postot+=poscount[j];
                spenatot+=sumposexpnegA[j];
        }
        negposratio=(float)negtot/(float)postot;
        spenaoverpos=spenatot/(float)postot;
        fclose(infile);

        fprintf(outfile,"%f %f %f %f\n",taureal,negposratio,spenaoverpos,useikbt);
        fflush(outfile);

}

	fclose(outfile);

}
