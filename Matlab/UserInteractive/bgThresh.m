% Jeff changelog -- modifying bin number to be hardwired

function thresh = bgThresh(im)

% we seek min i s.t. n(i) > n(i-1) and n(i-1) <= n(i-2)

im = double(im(im>0));
numbins=101;
edges = linspace(0,max(im), numbins);
n = histc(im, edges);
nPos = n(n>0);
nPosIdx = find(n>0);
curIncreasing = [0; diff(nPos)>0];
prevNotIncreasing = [0; 0; diff(nPos(1:end-1))<=0];
i = min(find(curIncreasing & prevNotIncreasing));
thresh = edges(nPosIdx(i));
% 
% nPrimeSgn = diff(n) ./ max(1, abs(diff(n)));
% nPrimeSgn(nPrimeSgn == 0) = -1;
% nPrimeSgnChange = diff(nPrimeSgn) / 2;
% firstDecrease = min(find(nPrimeSgn == -1));
% peakRHS = firstDecrease - 1 + min(find(nPrimeSgnChange(firstDecrease:end) == 1));
% thresh = edges(peakRHS+2);
