function im = convhullimage(x, y, M, N)

k = convhull(x,y);
im = poly2mask(x(k), y(k), M, N);