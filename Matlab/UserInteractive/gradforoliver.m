function gradforoliver(numsections,skindepthfrac,skinbarrierfrac)

if(~exist('numsections'))
    numsections=200;
end
% cd('/Users/olivergrimm/Desktop');
[filename,pathname] = uigetfile('*.tif', 'chose a file');
im=imread(strcat(pathname,filename));
doneflag=logical(0);
while(~doneflag)
        if(~exist('cutoff'))
            [embryoMask,cutoff] = getEmbryoMask(im);
        else
            [embryoMask,cutoff] = getEmbryoMask(im,cutoff);
        end
    [AP_x, AP_y] = getAPAxis(embryoMask);
    APpixellength=sqrt((AP_x(1)-AP_x(2))^2+(AP_y(1)-AP_y(2))^2);
        if(~exist('skinbarrierfrac'))
            skinbarrierfrac=0.005;
        end
    outerpxdrop=round(skinbarrierfrac*APpixellength);
        if(~exist('skindepthfrac'))
            skindepthfrac=0.025;
        end
    pxdropwidth=round(skindepthfrac*APpixellength);
    outermask=shrink(embryoMask,outerpxdrop);
    innermask=shrink(outermask,pxdropwidth);
    surfacemask=logical(outermask-innermask);
    figure; imagesc(int16(im)+int16(cutoff*embryoMask)+int16(4*cutoff*surfacemask));
    title(filename);
    line(AP_x,AP_y);
    k=menu('Everything OK?','Yes','No');
        if(k==2)
            prompt={'Choose a factor to multiply cutoff by:','Choose a new skin depth fraction:','Choose a new skin barrier fraction:'};
            name='Fixer upper';
            skindepthfracstring=num2str(skindepthfrac);
            skinbarrierfracstring=num2str(skinbarrierfrac);
            defaultans={'1',skindepthfracstring,skinbarrierfracstring};
            response=inputdlg(prompt,name,1,defaultans);
            multcutbycell=response(1);
            multcutby=str2double(multcutbycell);
            cutoff=cutoff*multcutby;
            skindepthfraccell=response(2);
            skindepthfrac=str2double(skindepthfraccell);
            skinbarrierfraccell=response(3);
            skinbarrierfrac=str2double(skinbarrierfraccell);
        else
            doneflag=logical(1);
        end
end
[APconc,APaxis]=APproject(im,surfacemask,AP_x,AP_y,numsections);
plotfignum=figure; 
% plot(0,0,'w.'); hold on; 
plot((1:numsections)/numsections,APconc(:,2),'r.'); hold on; plot((1:numsections)/numsections,APconc(:,3),'b.'); title(filename); ylim([0 1.03*max(max(APconc))]);
l=menu('Needs flipping?','Yes','No');
if(l==1)
    for j=1:size(APconc,1)
        newAPconc(j,:)=APconc(size(APconc,1)-j+1,:);
    end
    figure(plotfignum); hold off;
    % plot(0,0,'w.'); hold on; 
    plot((1:numsections)/numsections,newAPconc(:,2),'r.'); hold on; plot((1:numsections)/numsections,newAPconc(:,3),'b.'); title(filename); ylim([0 1.03*max(max(APconc))]);
end
figure; imagesc(im); title(filename);
save dumpfile;
