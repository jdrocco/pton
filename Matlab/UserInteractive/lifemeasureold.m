function [lifetimemeas,coeffvals,putindark,remainindark]=lifemeasure

if(~exist('numsections'))
    numsections=100;
end
if(~exist('totinseries'))
    numseries=3;
    totinseries=60;
    prompt={'Choose number of sections:','Choose number of series:','Choose number of images in series:'};
    name='User input';
    numseriesstring=num2str(numseries);
    numsectionsstring=num2str(numsections);
    totinseriesstring=num2str(totinseries);
    defaultans={numsectionsstring,numseriesstring,totinseriesstring};
    response=inputdlg(prompt,name,1,defaultans);
    numsectionscell=response(1);
    numsections=str2double(numsectionscell);
    numseriescell=response(2);
    numseries=str2double(numseriescell);
    totinseriescell=response(3);
    totinseries=str2double(totinseriescell);
end
% cd('/Users/olivergrimm/Desktop');
for n=1:numseries
    [firstfilename(n,:),pathname(n,:)] = uigetfile('*.tif', 'choose first image in series');
    cd(pathname(n,:));
    files=dir;
    for i=1:length(files)
        if length(files(i).name)==length(firstfilename(n,:))
            if files(i).name==firstfilename(n,:)
                onepicked(n)=i;
            end
        end
    end
    if(~(n==numseries))
        secelapsed(n)=1460;
        prompt={'How much time will elapse (seconds)?'};
        name='User input';
        secelapsedstring=num2str(secelapsed(n));
        defaultans={secelapsedstring};
        response=inputdlg(prompt,name,1,defaultans);
        secelapsedcell=response(1);
        secelapsed(n)=str2double(secelapsedcell);
        [prepulsefilename(n,:),prepulsepathname(n,:)] = uigetfile('*.tif', 'choose prepulse recovered image');
    end
end

[surfacemask,AP_x,AP_y,idxstore,idx2len,idxdorlen,innerdiam,outerdiam]=embryosetup(pathname(1,:),firstfilename(1,:),numsections);

save(strcat(firstfilename(n,1:17),'dump.mat'));
for n=1:numseries
    for k=1:totinseries
        cd(pathname(n,:));
        files=dir;
        im=imread(strcat(pathname(n,:),files(onepicked(n)+k-1).name));
        [APconc,APaxis,quanthere]=APprojectwrad(im,surfacemask,AP_x,AP_y,numsections,idxstore,idx2len,idxdorlen,innerdiam,outerdiam);
        totamount(k)=nansum(quanthere);
    end
    curvoptins=fitoptions('exp2','Lower',[0 -2 0 -0.00001],'Upper',[Inf 0 Inf 0.00001]);
    curvout=fit((1:length(totamount))',totamount','exp2',curvoptins);
    coeffvals(n,:)=coeffvalues(curvout);
    if((coeffvals(n,4)/coeffvals(n,2))>0.02)
        disp('Warning: reflection greater than 2% of transmission');
    end
    initialamount(n)=curvout(0);
    finalamount(n)=curvout(totinseries);
    figure; plot(totamount,'+'); hold on; plot(curvout); title(firstfilename(n,:));
    legend(strcat('Initial amount = ',num2str(initialamount(n))),strcat('Conversion lifetime (ims) = ',num2str(-1/coeffvals(n,2))));
    if(~(n==1))
        plot(0,prepulseamount(n-1),'g*');
        putindark(n-1)=initialamount(n-1)-finalamount(n-1);
        remainindark(n-1)=initialamount(n)-prepulseamount(n-1);
        lifetimemeas(n-1)=-1*secelapsed(n-1)/log(remainindark(n-1)/putindark(n-1));
        legend(strcat('Initial amount = ',num2str(initialamount(n))),strcat('Conversion lifetime (ims) = ',num2str(-1/coeffvals(n,2))),strcat('Protein lifetime (min) = ',num2str(lifetimemeas(n-1)/60)));
    end
    if(~(n==numseries))
        im=imread(strcat(prepulsepathname(n,:),prepulsefilename(n,:)));
        [APconc,APaxis,quanthere]=APprojectwrad(im,surfacemask,AP_x,AP_y,numsections,idxstore,idx2len,idxdorlen,innerdiam,outerdiam);
        prepulseamount(n)=nansum(quanthere);
    end
end