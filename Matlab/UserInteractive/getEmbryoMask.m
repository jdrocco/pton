function [embryoMask,cutoff] = getEmbryoMask(im, cutoff, concaveMask)

if ~exist('cutoff') || isempty(cutoff), cutoff = bgThresh(im); end
embryoMask = im >= cutoff;
L = bwlabeln(embryoMask, 4);
S = regionprops(L, 'Area');
embryoMask = ismember(L, find([S.Area] == max([S.Area])));
if exist('concaveMask'), closedMask = padBorder(imclose(padBorder(embryoMask, 100), strel('disk', 100)), -100); end
outline = bwperim(embryoMask);
[i,j] = find(outline);
embryoMask = convhullimage(j, i, size(im,1), size(im,2));
if exist('concaveMask'), embryoMask = (embryoMask & ~concaveMask) | (closedMask & concaveMask); end