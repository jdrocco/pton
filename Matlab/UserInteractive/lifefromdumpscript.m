
for n=1:numseries
    for k=1:totinseries
        cd(pathname(n,:));
        files=dir;
        im=imread(strcat(pathname(n,:),files(onepicked(n)+k-1).name),'tif');
        [APconc,APaxis,quanthere]=APprojectwrad(im,surfacemask,AP_x,AP_y,numsections,idxstore,idx2len,idxdorlen,innerdiam,outerdiam);
        totamount(k)=nansum(quanthere);
        forbigplot(n,k)=nansum(quanthere);
    end
    curvoptins=fitoptions('exp2','Lower',[0 -2 0 -0.00001],'Upper',[Inf 0 Inf 0.00001]);
    curvout=fit((1:length(totamount))',totamount','exp2',curvoptins);
    coeffvals(n,:)=coeffvalues(curvout);
    if((coeffvals(n,4)/coeffvals(n,2))>0.02)
        disp('Warning: reflection greater than 2% of transmission');
    end
    initialamount(n)=curvout(0);
    finalamount(n)=curvout(totinseries);
    figure; plot(totamount,'+'); hold on; plot(curvout); title(firstfilename(n,:));
    legend(strcat('Initial amount = ',num2str(initialamount(n))),strcat('Conversion lifetime (ims) = ',num2str(-1/coeffvals(n,2))));
    if(~(n==1))
        plot(0,prepulseamount(n-1),'g*');
        putindark(n-1)=initialamount(n-1)-finalamount(n-1);
        remainindark(n-1)=initialamount(n)-prepulseamount(n-1);
        lifetimemeas(n-1)=-1*secelapsed(n-1)/log(remainindark(n-1)/putindark(n-1));
        legend(strcat('Initial amount = ',num2str(initialamount(n))),strcat('Conversion lifetime (ims) = ',num2str(-1/coeffvals(n,2))),strcat('Protein lifetime (min) = ',num2str(lifetimemeas(n-1)/60)));
    end
    if(~(n==numseries))
        im=imread(strcat(prepulsepathname(n,:),prepulsefilename(n,:)),'tif');
        [APconc,APaxis,quanthere]=APprojectwrad(im,surfacemask,AP_x,AP_y,numsections,idxstore,idx2len,idxdorlen,innerdiam,outerdiam);
        prepulseamount(n)=nansum(quanthere);
    end
end