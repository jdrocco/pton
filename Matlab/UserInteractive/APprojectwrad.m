function [APconc,APaxis,quanthere,idxstore,idx2len,idxdorlen,innerdiam,outerdiam]=APprojectwrad(images,mask,x_AP,y_AP,sections,idxstore,idx2len,idxdorlen,innerdiam,outerdiam)

metersperpixel=0.001;
%
% geometrical setup... (perpendicular projection along AP-axis)
x0=x_AP(1);
y0=y_AP(1);
lenx=diff(x_AP);
leny=diff(y_AP);
leng=sqrt((lenx)^2+(leny)^2);
dl=leng/sections;
dlx=lenx/sections;dly=leny/sections;
slope=-lenx/leny;
dims=size(images);
[X,Y]=meshgrid(1:dims(2),1:dims(1));
% get indices for sections along embryo rim, projected onto AP-axis
%idxstore=zeros(sections,1000);
if(~exist('outerdiam'))
for i=1:sections
        if sign(leny)>0
            idx=find(Y>=slope*(X-(x0+dlx*(i-1)))+y0+dly*(i-1) & ...
                Y<=slope*(X-(x0+dlx*i))+(y0+dly*i));
        else
            idx=find(Y<=slope*(X-(x0+dlx*(i-1)))+y0+dly*(i-1) & ...
                Y>=slope*(X-(x0+dlx*i))+(y0+dly*i));
        end
%         if sign(leny)>0
%             idx=find(Y>=slope*(X-(x0+sign(lenx)*abs(dx)*(i-1)))+y0+abs(dy)*(i-1) & ...
%                 Y<=slope*(X-(x0+sign(lenx)*abs(dx)*i))+(y0+abs(dy)*i));
%         else
%             idx=find(Y<=slope*(X-(x0+sign(lenx)*abs(dx)*(i-1)))+y0+abs(dy)*(i-1) & ...
%                 Y>=slope*(X-(x0+sign(lenx)*abs(dx)*i))+(y0+abs(dy)*i));
%         end
        idx2=find(mask(idx)==1);
        idxdor=find(mask(idx)==1 & Y(idx)>=-(X(idx)-x0)/slope+y0);
        idxven=find(mask(idx)==1 & Y(idx)<=-(X(idx)-x0)/slope+y0);
        idxstore(i,1:length(idxdor))=idx(idxdor)';
        idxstore(i,length(idxdor)+1:length(idxdor)+length(idxven))=idx(idxven)';
        %store length in last two colums
        idx2len(i)=length(idx2);
        idxdorlen(i)=length(idxdor);
        [huey,lewis] = ind2sub(size(mask),idx(idxdor));
        [andthe,news] = ind2sub(size(mask),idx(idxven));
        [allx,ally] = ind2sub(size(mask),idx(idx2));
        if(length(huey)&&length(andthe))
%             distpairmat=dist([huey lewis],[andthe news]);
%             outerdiam(i)=max(max(distpairmat));
%             innerdiam(i)=min(min(distpairmat));
            centroidpt=[mean(allx) mean(ally)];
            distdormat=dist(centroidpt,[huey lewis]);
            distvenmat=dist(centroidpt,[andthe news]);
            outerdiam(i)=max(distdormat)+max(distvenmat);
            innerdiam(i)=min(distdormat)+min(distvenmat);
        else
            outerdiam(i)=0;
            innerdiam(i)=0;
        end
end
end
%define unit-AP-axis
APaxis=[dl:dl:leng]-0.5*dl;
%get concentrations along AP-axis
APconc=zeros(sections,3);
	for i=1:sections;
        %mean of both sides
        APconc(i,1)=nanmean(images(idxstore(i,1:idx2len(i))));
%         quanthere(i)=3.14159*(((outerdiam(i)/2)^2)-((innerdiam(i)/2)^2))*dl*APconc(i,1)*metersperpixel^3;
        quanthere(i)=3.14159*(((outerdiam(i)/2)^2))*dl*APconc(i,1)*metersperpixel^3;
%         dispims=images;
%         dispims(idxstore(i,1:idx2len(i)))=50000;
%         imagesc(dispims); pause(0.2);
        %APconc(isnan(APconc(:,1)),1)=0;%mean(APconc(find(diff(isnan(APconc(:,1)))==1)-1:find(diff(isnan(APconc(:,1)))==1),1));
        %dorsal side
        APconc(i,2)=nanmean(images(idxstore(i,1:idxdorlen(i))));
        %ventral side
        APconc(i,3)=nanmean(images(idxstore(i,idxdorlen(i)+1:idx2len(i))));
    end