function im2 = padBorder(im, pad, fillValue)

if ndims(im) > 2, error('padBorder only implemented for 2-dimensional images'); end
if ~exist('fillValue'), fillValue = 0; end
[mIn, nIn] = size(im);
if isscalar(pad)
    mOut = mIn + 2*pad;
    nOut = nIn + 2*pad;
else
    mOut = mIn + 2*pad(1);
    nOut = nIn + 2*pad(2);
end
if any([mOut nOut] > [mIn nIn])
    if islogical(im)
        im2 = logical(fillValue) & true(mOut, nOut);
    else
        im2 = fillValue * ones(mOut, nOut, class(im));
    end
end
if mOut > mIn
    mInMin = 1; mInMax = mIn;
    mPad = (mOut - mIn) / 2;
    mOutMin = 1 + mPad; mOutMax = mOut - mPad;
else
    mOutMin = 1; mOutMax = mOut;
    mPad = (mIn - mOut) / 2;
    mInMin = 1 + fix(mPad); mInMax = mIn - ceil(mPad);
end
if nOut > nIn
    nInMin = 1; nInMax = nIn;
    nPad = (nOut - nIn) / 2;
    nOutMin = 1 + nPad; nOutMax = nOut - nPad;
else
    nOutMin = 1; nOutMax = nOut;
    nPad = (nIn - nOut) / 2;
    nInMin = 1 + fix(nPad); nInMax = nIn - ceil(nPad);
end
im2(mOutMin:mOutMax, nOutMin:nOutMax) = im(mInMin:mInMax, nInMin:nInMax);