function shrunkmask=shrink(mask,times)

for k=1:times
uponex=circshift(mask,[1 0]);
uponey=circshift(mask,[0 1]);
downonex=circshift(mask,[-1 0]);
downoney=circshift(mask,[0 -1]);
diagupup=circshift(mask,[1 1]);
diagdownup=circshift(mask,[-1 1]);
diagdowndown=circshift(mask,[-1 -1]);
diagupdown=circshift(mask,[1 -1]);
for i=1:size(mask,1)
    for j=1:size(mask,2)
        sumweights=mask(i,j)+uponex(i,j)+uponey(i,j)+downonex(i,j)+downoney(i,j)+diagupup(i,j)+diagupdown(i,j)+diagdownup(i,j)+diagdowndown(i,j);
        if(sumweights>8)
            shrunkmask(i,j)=logical(1);
        else
            shrunkmask(i,j)=logical(0);
        end
    end
end
mask=shrunkmask;
end