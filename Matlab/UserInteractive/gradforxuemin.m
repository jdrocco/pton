function APconc=gradforxuemin(numsections,skindepthfrac,skinbarrierfrac)

if(~exist('numsections'))
    numsections=100;
end
cd('/');
[filename,pathname] = uigetfile('*.gif', 'chose a file');
im=imread(strcat(pathname,filename));
doneflag=logical(0);
APaxisfinalized=logical(0);
interactivefig=figure;
while(~doneflag)
        if(~exist('cutoff'))
            [embryoMask,cutoff] = getEmbryoMask(im);
        else
            [embryoMask,cutoff] = getEmbryoMask(im,cutoff);
        end
    if(~APaxisfinalized)   
        [AP_x, AP_y] = getAPAxis(embryoMask);
    end
    APpixellength=sqrt((AP_x(1)-AP_x(2))^2+(AP_y(1)-AP_y(2))^2);
        if(~exist('skinbarrierfrac'))
            skinbarrierfrac=0.01;
        end
    outerpxdrop=round(skinbarrierfrac*APpixellength);
        if(~exist('skindepthfrac'))
            skindepthfrac=0.03;
        end
    pxdropwidth=round(skindepthfrac*APpixellength);
    outermask=shrink(embryoMask,outerpxdrop);
    innermask=shrink(outermask,pxdropwidth);
    surfacemask=logical(outermask-innermask);
    figure(interactivefig); imagesc(int16(im)+int16(cutoff*embryoMask)+int16(4*cutoff*surfacemask));
    title(filename);
    line(AP_x,AP_y,'color',[1 1 1]);
    k=menu('Everything OK?','Yes','No');
        if(k==2)
            prompt={'Choose a factor to multiply cutoff by:','Choose a new skin depth fraction:','Choose a new skin barrier fraction:'};
            name='Fixer upper';
            skindepthfracstring=num2str(skindepthfrac);
            skinbarrierfracstring=num2str(skinbarrierfrac);
            defaultans={'1',skindepthfracstring,skinbarrierfracstring};
            response=inputdlg(prompt,name,1,defaultans);
            multcutbycell=response(1);
            multcutby=str2double(multcutbycell);
            cutoff=cutoff*multcutby;
            skindepthfraccell=response(2);
            skindepthfrac=str2double(skindepthfraccell);
            skinbarrierfraccell=response(3);
            skinbarrierfrac=str2double(skinbarrierfraccell);
        else
            doneflag=logical(1);
        end
end
if(~APaxisfinalized)
    q=menu('Manually choose AP axis?','Yes','No');
    if(q==1)
        APselfig=figure;
        [AP_x,AP_y]=getapaxisj(im);
    end
    APaxisfinalized=logical(1);
    close(APselfig);
end
figure(interactivefig); clf; imagesc(int16(im)+int16(cutoff*embryoMask)+int16(4*cutoff*surfacemask)); line(AP_x,AP_y,'color',[1 1 1]);
[APconc,APaxis]=APprojectophalf(im,surfacemask,AP_x,AP_y,numsections);
plotfignum=figure; 
% plot(0,0,'w.'); hold on; 
plot((1:numsections)/numsections,APconc(:,2),'r.'); hold on; plot((1:numsections)/numsections,APconc(:,3),'b.'); title(filename); ylim([0 1.03*max(max(APconc))]);
figure(plotfignum); l=menu('Needs flipping?','Yes','No');
if(l==1)
    for j=1:size(APconc,1)
        newAPconc(j,:)=APconc(size(APconc,1)-j+1,:);
    end
    figure(plotfignum); hold off;
    % plot(0,0,'w.'); hold on; 
    plot((1:numsections)/numsections,newAPconc(:,2),'r.'); hold on; plot((1:numsections)/numsections,newAPconc(:,3),'b.'); title(filename); ylim([0 1.03*max(max(APconc))]);
    clear APconc; APconc=newAPconc;
end
figure; imagesc(im); title(filename);
save dumpfile;