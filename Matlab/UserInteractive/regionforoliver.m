function areaused=regionforoliver(areatouse)

numbins=48;
cd('/Users/olivergrimm/Desktop');
doneflag=logical(0);
moreimages=1;
while(moreimages)
[filename,pathname] = uigetfile('*.tif', 'chose a file');
im=imread(strcat(pathname,filename));
regionfig=figure;
histofig=figure;
while(~doneflag)
    figure(regionfig); clf;
    imagesc(int16(im));
    if(~exist('areatouse'))
        areatouse=roipoly;
    end
    binspacer=linspace(0,256,numbins);
    [intensityfreq,binspout]=hist(double(im(areatouse)),binspacer);
    meanintens=mean(double(im(areatouse)));
    figure(regionfig); clf; imagesc(int16(im)+int16(areatouse*(meanintens+mean(mean(im)))));
    figure(histofig); clf;
    bar(binspout,intensityfreq);
    title(filename); xlabel('Intensity'); ylabel('Frequency');
    legend(strcat('<Intesity> = ',num2str(meanintens)));
    k=menu('Everything OK?','Yes','No');
    if(k==2)
        doneflag=logical(0);
        prompt={'Choose number of bins:'};
        name='Fixer upper';
        numbinstring=num2str(numbins);
        defaultans={numbinstring};
        response=inputdlg(prompt,name,1,defaultans);
        numbinput=response(1);
        numbins=str2double(numbinput);
        newregsel=menu('Choose region again?','Yes','No');
        if(newregsel==1)
            figure(regionfig); clf;
            imagesc(int16(im));
            areatouse=roipoly;
        end
    else
        doneflag=logical(1);
    end
end
responded=menu('Try this on another image?','Yes','No');
    if(responded==2)
        moreimages=logical(0);
    else
        moreimages=logical(1);
        doneflag=logical(0);
    end
end