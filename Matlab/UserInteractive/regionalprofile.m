function regionalprofile(numsections,micronsperpixel)

if(~exist('numsections'))
    numsections=100;
end
if(~exist('micronsperpixel'))
    micronsperpixel=2.0;
    prompt={'Choose number of sections:','How many microns per pixel:'};
    name='User input';
    numsectionsstring=num2str(numsections);
    micronsperpixelstring=num2str(micronsperpixel);
    defaultans={numsectionsstring,micronsperpixelstring};
    response=inputdlg(prompt,name,1,defaultans);
    numsectionscell=response(1);
    numsections=str2double(numsectionscell);
    micronsperpixelcell=response(2);
    micronsperpixel=str2double(micronsperpixelcell);
end
cd('/Users/olivergrimm/Desktop');
[filename,pathname] = uigetfile('*.tif', 'chose a file');
im=imread(strcat(pathname,filename));
doneflag=logical(0);
regionfig=figure;
figure(regionfig); clf;
imagesc(int16(im));
surfacemask=roipoly;
[AP_x, AP_y] = getapaxisj(int16(im)+int16(mean(im(:))*surfacemask));
lenx=diff(AP_x);
leny=diff(AP_y);
leng=sqrt((lenx)^2+(leny)^2);
dlreal=micronsperpixel*(leng/numsections);
[APconc,APaxis]=APproject(im,surfacemask,AP_x,AP_y,numsections);
plotfignum=figure; 
% plot(0,0,'w.'); hold on; 
plot((1:numsections)*dlreal,APconc(:,2),'r.'); hold on; plot((1:numsections)*dlreal,APconc(:,3),'b.'); title(filename); ylim([0 1.03*max(max(APconc))]);
l=menu('Needs flipping?','Yes','No');
if(l==1)
    for j=1:size(APconc,1)
        newAPconc(j,:)=APconc(size(APconc,1)-j+1,:);
    end
    figure(plotfignum); hold off;
    % plot(0,0,'w.'); hold on; 
    plot((1:numsections)*dlreal,newAPconc(:,2),'r.'); hold on; plot((1:numsections)/numsections,newAPconc(:,3),'b.'); title(filename); ylim([0 1.03*max(max(APconc))]);
end
figure; imagesc(im); title(filename);
save dumpfile;
